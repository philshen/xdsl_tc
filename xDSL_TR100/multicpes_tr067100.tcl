# Jun21-2006 Read csv file for WT100
# Jun23-2006 Test dls414E for WT100
# Jul07-2006 Add GUI to exctue it to request ohters to help do test.
# Jul11-2006 Final Verify
# Jul25-2006 Modify to test Haiuew ADSL IOT
# Aug04-2006 For WT100 AnnexB
# Aug30-2006 For Annex boht A and B by INI File LoopSimulator.
#            Read multi testcase csv files.
# Sep01-2006 Optimize test flow with same testprofile, loop length, noise file.
#            And Verify it again.
# Sep04-2006 Add both DSLAM and Noise parameter from setup.ini
# Sep06-2006 Verify both Straigh loop and Bridge tap
# Sep07-2006 Updatae items that follow ToDoList.
# Sep08-2006 Update both ANSI and CSA loop type using test_wt100_loop.tcl.
# Sep11-2006 Add Func_INI::_GetTargetTestCasesID to set list_target_testcasesid
#            Add "wan adsl reset" while modem can't link up.
# Sep15-2006 By dbg_tr067.tcl do 2layer PCB, waiting 60 seconds then check status
#            if "up"
# Sep20-2006 Verify TR067 test plan BRCM_AnnexA.
#            Add proc main_cycleone to reduce proc main.
# Sep21-2006 Logout failed list_testcaseID
# Sep26-2006 Adapt from Test_tr067.tcl
# Sep28-2006 Update test flow after meeting with CSI
# Oct11-2006 Modify each 10 sec to waiting modem up till 70 sec
#            Add proc Disp_NoiseFilePath
# Oct26-2006 Add stradsldiag_opt parameter in proc DUT_DumpInfo
#            Modify Loop flow just only one open noise source
# OCt31-2006 Modify both proc main and proc main_cycleone to run once
#            Verify this script for retest while failed test case.
# Nov03-2006 Add {fail} condition to list_fail_testcaseid
# Nov05-2006 Add proc ShowLoopTypeLength
#            Move proc Loop_GPIBSendCmd to proc LoopCommon::Loop_GPIBSendCmd
# Nov10-2006 Add proc to get info from BRCM DSLAM
#            Test it with both BRCM modem from Comtrend  and TC modem
# Nov13-2006 Adjust testing flow.  Before dump modem data, change loop setting.
# Nov16-2006 No "wan adsl rest" while waiting error second
# Nov30-2006 Add TC pure bridge modem procdure
# Dec01-2006 Add ZyNOS modem procdure
# Dec20-2006 Addd TR067_B test case
#            Remark proc proc DSLAM_BRCM_Init
# Dec25-2006 Add Loop Simualtor DLS400IAE for TR067_ADSL1
# Dec27-2006 Test by 400IAE and 410A
# Jan02-2007 Add DSLAM Alctel7300 AnnexA/B
# Jan11-2007 logout failed testcase ID after 2nd datarate dump
# Jan13-2007 Modify proc proc DUT_DumpInfo_Sel
# Jan15-2007 Adapt from rc4_tr067.tcl
# Jan17-2007 set adslrest_opt "disable" while first time
# Jan18-2007 Modify proc DUT_DumpInfo_Sel
# Jan19-2007 Add proc DSLAM_EnDisAnnexLM for TR100
# Feb26-2007 For BRCM_A and BRCM_B,divid COCfg.ini into both COCfg_profile.ini
#             and COCfg_port.ini.
#            Merge rc6_tr067.tcl to rc6_tr100.tcl.
# Mar01-2007 Modify Enable/Disable AnnexL/M in proc DSLAM_RunProcomm
# Mar15-2007 Modify proc Parse_TCPureBridge::DUT_DumpInfo_PureBridge and
#            proc Parse_TCModem::DUT_DumpInfo to meet TR100 US/DS datarate in differnet path
# Mar16-2007 Get value of waituptimes and value of errorsec_duration from ini file.
#            Add Std name to log csv file.
# Mar16-2007 Modify argument of porc DSLAM_EnDisAnnexLM to meet TR067 TR100 test cases
# Mar20-2007 Add adsldiag_iniopt from ini from inifile to en/disable "wan adsl diag" opt.
# Mar21-2007 Add target DSLAM Model name in log csv file.
#            Add Relay board function of Multi-CPEs.
# Mar28-2007 Update proc main_cycleone_testbed_setup
# Apr02-2007 Update proc Get_socketch_relaybdpt to return target relaybd port socket
# Apr17-2007 Solve to log out table name to each modem log file.
# Apr23-2007 To set session 500 miniutes to solve mutli CPEs function in relay board
# Apr25-2007 Add DLS5500 ID00184 noise generator option
# May02-2007 Show TCmodem model on log file name
# May06-2007 Slove Enable/Disable annexL/M option in multiCPES and multiDSLAMs code.
################################################################################
package require Tcl 8.4
package require Tk 8.4
package require tile
package require Tktable

set currpath [file dirname [info script]]
set lib_path [file join  ".." "xDSL_Lib"]
set log_path [file join  ".." "Log"]
source "$lib_path/API_GPIB.tcl"
source "$lib_path/API_Loop.tcl"
source "$lib_path/API_Noise.tcl"
source "$lib_path/API_Misc.tcl"
source "$lib_path/API_DSLAM.tcl"

proc Log {level filename list_msg} {
    foreach temp $list_msg {
        set strmsg "$temp"
        Log::tofile $level $filename $strmsg
    }
}
################################################################################
# GUI Procedure
################################################################################
set config(elide,time) 0
set config(elide,level) 0
foreach level [logger::levels] {
    set config(elide,$level) 0
}

#
# Create a simple logger with the servicename 'global'
#
#
proc createLogger {} {
    global mylogger
    set mylogger [logger::init global]
    
    # loggers logproc takes just one arg, so curry
    # our proc with the loglevel and use an alias
    foreach level [logger::levels] {
        interp alias {} insertLogLine_$level {} insertLogLine $level
        ${mylogger}::logproc $level insertLogLine_$level
    }
}

# Put the logmessage to the logger system
proc sendMessageToLog {level} {
    ${::mylogger}::$level $::logmessage
}

proc createGUI {str_title str_args list_csvrow list_multicpes_testconds_logfile_availflag_ptnum \
            list_414ELenCoeff list_410ALooplenCoeff list_410AGPIBCmdSet \
            list_noise_pathfname list_dslamprofile} {
    global mylogger
    global logwidget
    
    wm title . $str_title
    
    # The output window
    labelframe .log -text "Output Message"
    text .log.text -yscrollcommand [list .log.yscroll set] -wrap none
    set logwidget .log.text
    scrollbar .log.yscroll -orient vertical -command [list $logwidget yview]
    frame .log.buttons
    frame .log.buttons.elide
    
    grid .log.text .log.yscroll -sticky nsew
    grid configure .log.yscroll -sticky nws
    grid .log.buttons.elide -sticky ew
    grid .log.buttons -columnspan 200 -sticky ew
    grid .log -sticky ew;#news
    grid columnconfigure . 0 -weight 1
    grid rowconfigure . 0 -weight 0
    grid rowconfigure . 1 -weight 1
    grid columnconfigure .log 0 -weight 1
    grid columnconfigure .log 1 -weight 0
    grid rowconfigure .log 0 -weight 1
    
    ### a little compose window for entering messages
    labelframe .compose -text "Function Button"
    frame .compose.levels
    button .compose.levels.run -command [list main $str_args $list_csvrow \
            $list_multicpes_testconds_logfile_availflag_ptnum \
            $list_414ELenCoeff $list_410ALooplenCoeff $list_410AGPIBCmdSet \
            $list_noise_pathfname $list_dslamprofile] -text "Run"
    button .compose.levels.exit -command [list Noise_Disconnect] -text "Exit"
    lappend buttons .compose.levels.exit .compose.levels.run
    eval grid $buttons -sticky ew -padx 20 -pady 5
    # grid .compose.logmessage -sticky ew
    grid .compose.levels -sticky ew
    grid .compose -sticky ew
    
    # Now we create some fonts
    # a fixed font for the first two columns, so they stay nicely lined up
    # a proportional font for the message as it is probably better to read
    #
    font create logger::timefont -family {Courier} -size 8
    font create logger::levelfont -family {Courier} -size 8
    font create logger::msgfont -family {Arial} -size 10
    $logwidget tag configure logger::time -font logger::timefont
    $logwidget tag configure logger::level -font logger::levelfont
    $logwidget tag configure logger::message -font logger::msgfont
    
    # Now we create some colors for the levels, so our messages appear in different colors
    # color<->levels {debug,info,notice,warn,error,critical }
    foreach level [logger::levels] color {darkgrey lightgrey lightgreen lightblue red orange} {
        $logwidget tag configure logger::$level -background $color
    }
    
    # Disable the widget, so it is read only
    $logwidget configure -state disabled
        
}
proc insertLogLine {level txt} {
    global logwidget
    
    $logwidget configure -state normal
    $logwidget insert end "<[clock format [clock seconds] -format "%H:%M:%S"]> " \
            [list logger::time logger::$level] \
            $txt\n [list logger::message logger::$level]
    
    $logwidget yview moveto 1;update
    $logwidget configure -state disabled
}

proc GUIRun {str_title str_args list_csvrow list_multicpes_testconds_logfile_availflag_ptnum \
            list_414ELenCoeff list_410ALooplenCoeff list_410AGPIBCmdSet \
            list_noise_pathfname list_dslamprofile} {
    createLogger
    createGUI $str_title $str_args $list_csvrow $list_multicpes_testconds_logfile_availflag_ptnum \
            $list_414ELenCoeff $list_410ALooplenCoeff $list_410AGPIBCmdSet \
            $list_noise_pathfname $list_dslamprofile
}
proc  DUT_DSLOPmode {list_dutip maxretry timeout opmode} {
    global sock_desc_modem telnet_line
    
    set str_adslstatus [Parse_TCModem::_GetInfo_adsl-status $sock_desc_modem]
    set retrytimes 0
    
    set str_opmode [Parse_TCModem::_SetDUT_DSLOpMode $sock_desc_modem $opmode]
    
    return $str_opmode
}

proc DUT_InfoFile  {logfile csvfile loghead log} {
    Log::LogOut $loghead $logfile
    Log::LogOut $log $logfile
    Log::LogOut $log $csvfile
    return true
}
proc DUT_ErrorSecond {logfile maxtimeout} {
    set maxtimeout_duration [expr 1000 * $maxtimeout]
    
    set strmsg "Waiting $maxtimeout seconds that modem links up to dump data."
    # Func_INI::PntMsg "Waiting $timeout seconds that modem links up"
    insertLogLine info $strmsg
    set timeunit 10
    set timeout_counter 0
    set maxtimeout_counter [expr $maxtimeout / $timeunit]
    
    while {$timeout_counter < $maxtimeout_counter} {
        set strmsg "waiting $timeunit seconds"
        insertLogLine info $strmsg
        Log "info" $logfile [list $strmsg]
        
        set timeout_duration [expr 1000 * [expr $timeunit / 5]]
        after $timeout_duration; update;after $timeout_duration; update
        after $timeout_duration; update;after $timeout_duration; update
        after $timeout_duration; update
        
        incr timeout_counter
    }
    
    return true
}
proc DUT_DumpADSLDiag {logfname} {
    global sock_desc_modem
    
    set strmsg [Parse_TCModem::_GetInfo_ADSLDiag $sock_desc_modem]
    Log "info" $logfname [list $strmsg]
    
    set strmsg "Log out ADSL Diag Info"
    return $strmsg
}
proc DUT3rd_DumpInfo_ChkStatus {list_dslaminfo maxretry timeout} {
    set dslamdevicid [lindex $list_dslaminfo 0]
    set dslamportnum [lindex $list_dslaminfo 1]
    
    set inifile "dslam.ini"
    set inifile_co_dump "CODump.ini"
    set dslamdump "dslam_dump.log"
    
    set currpath [file normalize [file dirname [info script]]]
    set co_wasfile "test_dslam.wax"
    set co_was [file join $currpath $co_wasfile]
    
    set retrytimes 0
    
    switch -regexp $dslamdevicid {
        {^0007(1|6)} {
            ##Waiting to link down to up
            after [expr 1000 * 5]
            
            set str_adslstatus [DSLAM_BRCM::DumpInfo_ChkStatus_RunProcomm $inifile_co_dump \
                    $co_was $inifile $dslamdump]
            while {[string tolower $str_adslstatus] != "up"} {
                incr retrytimes
                ##Check retry times to break
                if {$retrytimes > $maxretry} {
                    set str_adslstatus "down"
                    return $str_adslstatus
                }
                
                set timeout_duration [expr $timeout * $retrytimes]
                set strmsg "Waiting $timeout_duration seconds that modem links up to dump info. \
                        Retry=$retrytimes times; Maxtimes=$maxretry"
                
                insertLogLine info $strmsg
                after [expr $timeout * 1000]; update
                set str_adslstatus [DSLAM_BRCM::DumpInfo_ChkStatus_RunProcomm $inifile_co_dump \
                        $co_was $inifile $dslamdump]
            };#while {$str_adslstatus != "up"}
        }
        default {
        }
    };#switch -regexp $dslamdevicid
    
    ## str_adslstatus is up
    return $str_adslstatus
}

proc DUT3rd_DumpInfo {testcaseid logfname maxretry timeout {csvfname ""} rowval list_dslaminfo} {
    set log_path [file join   ".." "Log"]
    if {$csvfname == ""} { set csvfname [file join $log_path "chandata_err.csv"]}
    
    set dslamdevicid [lindex $list_dslaminfo 0]
    set dslamportnum [lindex $list_dslaminfo 1]
    ##Base on DSLAM ID
    set str_adslstatus [DUT3rd_DumpInfo_ChkStatus $list_dslaminfo $maxretry $timeout]
    
    set looplen "$Func_CSV::LoopLen,$Func_CSV::BTLen"
    set time_tag [clock format [clock seconds] -format "%H:%M:%S %m%d%y"]
    append log_info $testcaseid "," $looplen "," $time_tag "," $Func_CSV::TestProfile ","
    
    if {[string tolower $str_adslstatus] == "up"} {
        ##Base on DSLAM ID
        switch -regexp $dslamdevicid {
            {^0007(1|6)} {
                set log_up [DSLAM_BRCM::DumpInfo_DUT3rd $testcaseid $looplen $time_tag \
                        $logfname $csvfname $rowval]
            }
        };#switch -regexp $dslamdevicid
        
        return $log_up
    } else  {
        append log_down $log_info "False"
        DUT_InfoFile  $logfname $csvfname "False" $log_down
        
        return $log_down
    };#if {[string tolower $str_adslstatus] == "up"}
}

proc Disp_NoiseFilePath {list_noisefilepath logfile} {
    ##Including Ch1~Ch4 noise file
    if {[llength $list_noisefilepath] == 4} {
        set chnum_counter 0
        foreach strtemp $list_noisefilepath {
            incr chnum_counter
            insertLogLine warn "CH$chnum_counter Noise File= $strtemp"
            Log "info" $logfile [list "CH$chnum_counter Noise File= $strtemp"]
        };#foreach strtemp $list_strmsg
    } else  {
        insertLogLine warn $list_noisefilepath
        Log "info" $logfile [list $list_noisefilepath]
    };#if {[llength $list_strmsg] == 4}
    
    return true
}

proc DSLAM_RunProcomm {rowvalue procom_inifile list_dslaminfo logfile list_dslamprofile} {
    set currprofile $Func_CSV::TestProfile
    set dslamdevicid [lindex $list_dslaminfo 0]
    set dslamportnum [lindex $list_dslaminfo 1]
    set COCfg_inifile "COCfg.ini"
    set COCfg_profile_inifile "COCfg_profile.ini"
    set COCfg_port_inifile     "COCfg_port.ini"
    set COCfg_profile_del_inifile "COCfg_profile_del.ini"
    #####################################
    # Testing purpose if INFX EVM
    #####################################
    if {($dslamdevicid == "00128") || ($dslamdevicid == "00067")} {
        return bypass
    }
    
    ##Check what kind of dslam to chose procoom script.
    ###################################################
    set co_wasfile [DSLAM_Common::Get_COWASFile $dslamdevicid]
    
    #Check prvious vs current DSLAM profile
    if {$DSLAM_Common::strTestProfile == $currprofile} {
        set strmsg "Current profile: $currprofile and previous one are the same."
        insertLogLine warn $strmsg
        Log "info" $logfile [list $strmsg]
        
    } else  {
        set strmsg "Previous profile: $DSLAM_Common::strTestProfile. \
                Current profile: $currprofile. DSLAMDevicID=$dslamdevicid. \
                DSLAMPortNum=$dslamportnum."
        insertLogLine warn $strmsg
        Log "info" $logfile [list $strmsg]
        
        set list_inifile [list $procom_inifile $COCfg_profile_inifile $COCfg_port_inifile]
        ###########################################
        #Generate Procmm ini file and Excute Procmm
        ###########################################
        DSLAM_Common::DSLAM_ProcomCfg_Run $list_dslaminfo $currprofile $list_inifile \
                $logfile $list_dslamprofile $co_wasfile
        ############################################
        #Enable/Disable annexL/M option
        #############################################
        DSLAM_Common::DSLAM_EnDisAnnexLM $DSLAM_Common::strTestProfile $currprofile \
                $dslamdevicid $procom_inifile $COCfg_inifile \
                $dslamportnum $co_wasfile $logfile
                
        ###########################################################
        #For BRCM_B
        #Delet previous DSLAM profile, and previous profile not null
        ###########################################################
        if {($dslamdevicid == "00071") && ($DSLAM_Common::strTestProfile != "")} {
            
            switch -regexp $dslamdevicid {
                {^B1_(Fix|RA)} {
                    set strmsg "Delete Previous profile: $DSLAM_Common::strTestProfile. \
                            DSLAMDevicID=$dslamdevicid. DSLAMPortNum=$dslamportnum."
                    insertLogLine warn $strmsg
                    Log "info" $logfile [list $strmsg]
                    
                    ##Generate ini file and excute by Procomm
                    DSLAM_BRCM::ProcomCfg_Delete_Init $DSLAM_Common::strTestProfile \
                            $COCfg_profile_del_inifile
                    DSLAM-Procomm::Run $procom_inifile $COCfg_profile_del_inifile $co_wasfile
                }
                
            };#switch -regexp $dslamdevicid
            
        };#if {($dslamdevicid == "00071") && ($DSLAM_Common::strTestProfile != "")}
                
    };#if {$DSLAM_Common::strTestProfile == $currprofile}
    
    return true
}

proc ShowLoopTypeLength {list_looptypeinfo list_loopinfo logfile csv_logfname list_414ELenCoeff list_410ALoopLen list_410ACmdSet} {
    ###################################################
    ## list_looptypeinfo
    ## [list $Func_CSV::LoopType $Func_CSV::LoopLen $Func_CSV::BTLen]
    ##list_loopinfo as below
    ##[list $Func_INI::str_LoopSimulator $Loop410A::GPIB411A $Loop410A::GPIB412A $Loop414E::GPIB414E]
    #####################################################
    set str_looptype [lindex $list_looptypeinfo 0]
    set str_looplen [lindex $list_looptypeinfo 1]
    set str_btlen [lindex $list_looptypeinfo 2]
    
    set str_loopsimulator [lindex $list_loopinfo 0]
    set str_gpib411A [lindex $list_loopinfo 1]
    set str_gpib412A [lindex $list_loopinfo 2]
    set str_gpib414E [lindex $list_loopinfo 3]
    set str_gpib400IAE1 [lindex $list_loopinfo 4]
    set str_gpib400IAE2 [lindex $list_loopinfo 5]
    
    #Check what kind of Loop Modle
    switch -regexp $str_loopsimulator {
        {^DLS414E} {
            ##unit meter
            set strmsg "In In LoopCommon::Loop_GPIBSendCmd: LoopLen meter=$str_looplen"
            Loop414E::LoopLength $str_looplen $list_414ELenCoeff
            
            Log "info" $logfile [list $strmsg]
            insertLogLine "warn" $strmsg
            MiscCommon::LogLoopBTLen $strmsg $logfile "$csv_logfname.csv"
            
        }
        
        {^DLS410A} {
            ################################
            #Check Loop Type CSA or ANSI
            ################################
            set list_410ALoopBTLenIdx [Loop410A::LoopBTLength $str_looplen \
                    $str_btlen $list_410ALoopLen]
            #Checking input loop length is correct.
            if {[llength $list_410ALoopBTLenIdx] > 0} {
                set strmsg "Length of Loop=$str_looplen; BT=$str_btlen"
                
                MiscCommon::LogLoopBTLen $strmsg $logfile "$csv_logfname.csv"
                
            } else  {
                ######################################################
                ## list_looptypeinfo
                ## [list $Func_CSV::LoopType $Func_CSV::LoopLen $Func_CSV::BTLen]
                #######################################################
                set strmsg "LoopType= $str_looptype; Length of Loop= $str_looplen; BT= $str_btlen"
                append strmsg "GPIB411A= $str_gpib411A; GPIB412A=$str_gpib412A"
                
                set list_410A_ANSI_CSA_LenIdx [Loop410A::ANSI_CSALength $str_looptype \
                        $list_410ALoopLen]
                MiscCommon::LogLoopBTLen $list_410A_ANSI_CSA_LenIdx $logfile "$csv_logfname.csv"
                
            };#if {[llength $list_410ALoopBTLenIdx] > 0}
            
            Log "info" $logfile [list $strmsg]
            insertLogLine "warn" $strmsg
        }
        
        {^A_DLS400IAE} {
            set len_str_looptype [string length $str_looptype]
            set len_str_looplen  [string length $str_looplen]
            set len_str_btlen    [string length $str_btlen]
            
            #################################
            #Check Loop Type CSA or ANSI
            ################################
            if {$len_str_looptype > 0} {
                set strmsg "LoopType= $str_looptype "
                append strmsg "GPIB400IAE1= $str_gpib400IAE1; GPIB400IAE2=$str_gpib400IAE2"
                
                ################################
                #Check if Straight or BT loop
                ################################
            } else {
                set strmsg "Length of Loop=$str_looplen; BT=$str_btlen; "
                append strmsg "GPIB400IAE1= $str_gpib400IAE1; GPIB400IAE2=$str_gpib400IAE2"
            };#if {$len_str_looptype > 0}
            
            Log "info" $logfile [list $strmsg]
            insertLogLine "warn" $strmsg
            MiscCommon::LogLoopBTLen $strmsg $logfile "$csv_logfname.csv"
        }
        
    };#switch -regexp $looptype
    
    return true
}
proc ShowCSVTestCases {list_csvfname} {
    
    foreach temp [lrange $list_csvfname 0 end] {
        append strmsg "Current test cases CSV file=" $temp ","
    }
    insertLogLine warn $strmsg
    
    return true
}

proc DUT_DumpInfo_Sel {strDUTID str_current_testcaseid logfile waituptimes timeout \
            csv_logfname rowval list_adsldiag_adslrest list_dslaminfo} {
    global sock_desc_modem
    set tcmodem_inifile  "tcmodem.ini"
    set tcmodem_cfgfile "tcmodemCfg.ini"
    set tcmodem_dump    "tcmodem_reset.log"
    set adsldiag_opt    [lindex $list_adsldiag_adslrest 0]
    set adslrest_opt    [lindex $list_adsldiag_adslrest 1]
    
    switch -regexp $strDUTID  {
        {[A-Z][A-Z][A-Z][A-Z]d$} {
            set strmsg [DUT3rd_DumpInfo $str_current_testcaseid $logfile \
                    $waituptimes $timeout "$csv_logfname.csv" $rowval $list_dslaminfo]
            insertLogLine notice $strmsg
        }
        {[0-9][0-9][0-9][0-9][0-9]b$} {
            ##First time to dump TC modem
            ##############################
            if {[string tolower $adslrest_opt] == "enable"} {
                set strmsg "wan adsl reset"
                insertLogLine info $strmsg
                Log "info" $logfile [list $strmsg]
                
                Parse_TCModem::WanRest $tcmodem_inifile $tcmodem_cfgfile $tcmodem_dump
            }
            
            set strmsg [DUT3rd_DumpInfo $str_current_testcaseid $logfile \
                    $waituptimes $timeout "$csv_logfname.csv" $rowval $list_dslaminfo]
            insertLogLine notice $strmsg
        }
        {([0-9][0-9][0-9][0-9][0-9]pb|zy[a-z][a-z][a-z])$} {
            
            set strmsg [Parse_TCPureBridge::DUT_DumpInfo_PureBridge $str_current_testcaseid \
                    $logfile $waituptimes $timeout "$csv_logfname.csv" \
                    $adsldiag_opt $adslrest_opt $rowval]
            insertLogLine notice $strmsg
        }
        {[0-9][0-9][0-9][0-9][0-9]} {
            
            set strmsg [Parse_TCModem::DUT_DumpInfo $str_current_testcaseid $logfile \
                    $waituptimes $timeout "$csv_logfname.csv" \
                    $adsldiag_opt $adslrest_opt $rowval]
            insertLogLine notice $strmsg
        }
    };#switch -regexp $Func_INI::list_DUTID
    
    return $strmsg
}
proc main_cycleone_testbed_setup {rowval procom_inifile testbedlogfile testbedcsv_logfname \
            list_414ELenCoeff list_410ALoopLen list_410ACmdSet \
            list_noise_pathfname list_dslamprofile adslreset_iniopt} {
    
    
    ##############################
    ####Get DSLAM test profile
    #############################
    Func_CSV::Profile $rowval
    
    #################################
    #Run Procomm to setup DSLAM
    ################################
    ##Kill previous Procomm utility
    DSLAM-Procomm::KillProcomm
    
    set retval [DSLAM_RunProcomm $rowval $procom_inifile \
            $Func_INI::list_DSLAMInfo $testbedlogfile $list_dslamprofile]
    ##store current test profile into cache variable
    DSLAM_Common::inital $rowval
    
    ################################
    #Noise Generator command setting
    ################################
    set retval [NoiseCommon::DiffList $rowval $testbedlogfile]
    
    ##Initalize NoiseCommon::list_noisefile_ChNum
    NoiseCommon::inital $rowval
    
    ##Check difference between current and prvious nosie file profile
    set list_strmsg [NoiseCommon::_SendCmd $retval $$rowval $list_noise_pathfname \
            $testbedlogfile]
    ##Show Ch1~Ch4 noise file on widget
    Disp_NoiseFilePath $list_strmsg $testbedlogfile
    
    ###########################################
    #Check what kind of Loop Model and send cmd
    ###########################################
    ## Func_CSV::Loop_Type_BTLength $rowval
    
    set list_looptypeinfo [list $Func_CSV::LoopType $Func_CSV::LoopLen \
            $Func_CSV::BTLen]
    set list_loopinfo  [list $Func_INI::str_LoopSimulator \
            $Loop410A::GPIB411A $Loop410A::GPIB412A $Loop414E::GPIB414E \
            $Loop400IAE::GPIB400IAE1 $Loop400IAE::GPIB400IAE2]
    
    ######################################
    #Show Loop Type and Length
    ######################################
    ShowLoopTypeLength $list_looptypeinfo $list_loopinfo \
            $testbedlogfile $testbedcsv_logfname $list_414ELenCoeff \
            $list_410ALoopLen $list_410ACmdSet
    
    ####################################################
    ##current loopBTLen not equal previous that value
    ####################################################
    if {($LoopCommon::strLoopType == $Func_CSV::LoopType) \
                &&($LoopCommon::strLoopLen == $Func_CSV::LoopLen) \
                &&($LoopCommon::strLoopBT == $Func_CSV::BTLen)} {
        set strmsg "Current LoopType, LoopLen and LoopBT length as same as previous one."
        insertLogLine notice $strmsg
    } else  {
        LoopCommon::Loop_GPIBSendCmd $list_looptypeinfo $list_loopinfo \
                $testbedlogfile $testbedcsv_logfname $list_414ELenCoeff \
                $list_410ALoopLen $list_410ACmdSet
    }
    ##store cache value
    set LoopCommon::strLoopType $Func_CSV::LoopType
    set LoopCommon::strLoopLen $Func_CSV::LoopLen
    set LoopCommon::strLoopBT  $Func_CSV::BTLen
    
    ########################################################
    #Waiting 3 sec for Loop Simulator to settle down.
    ########################################################
    set maxretry 1;set timeout_interval 3;set strinfo "for Loop Simulator to settle down."
    Test_NoiseMargin::Timer $maxretry $timeout_interval $strinfo $testbedlogfile " "
    
    return true
}


                

proc main_cycleone {str_args list_csvrow list_multicpes_testconds_logfile_availflag_ptnum \
           list_414ELenCoeff list_410ALoopLen list_410ACmdSet \
            list_noise_pathfname list_dslamprofile} {
    
    global list_sock_multicpes
    global sock_desc_modem
    
    ###$looptimes ";" $modem_passwd ";" $procom_inifile ";" $timeout
    set list_args [split $str_args ";"]
    # set looptimes [lindex $list_args 0]
    set procom_inifile    [lindex $list_args 2]
    set timeout    [lindex $list_args 3]
     
    ####stor failed test case ID
    set list_fail_testcaseid_devid_bdptnum {}
    ####Show Test Std
    foreach temp [lrange $Func_INI::list_std 1 end] {
        insertLogLine info "Test Std: [lindex $Func_INI::list_std 0]= $temp"
    }
    ##rest value
    set DSLAM_Common::strTestProfile ""
    set LoopCommon::strLoopLen ""
    set LoopCommon::strLoopBT ""
    set LoopCommon::strLoopType ""
    set NoiseCommon::list_noisefile_ChNum {}
    
    foreach temp $list_multicpes_testconds_logfile_availflag_ptnum {
        set passflag [lindex $temp end-1]
        set logfile   [lindex $temp end-2]
        set csv_logfname [file tail $logfile]
        ## available port number in relay board
        if {[string tolower $passflag] == "pass"} {
            #Log variable title to csv file.
            MiscCommon::LogVariableTitle "$csv_logfname.csv"
            
        };#if {[string tolower $passflag] == "pass"}
        
    };#foreach temp $list_multicpes_logfile_availflag_ptnum
    
    #Show Test case CSV file name on widget
    ShowCSVTestCases $Func_INI::list_csvfname
    
    set testcase_counter 0
    
        
    ###get testbed log file name
    set temp [lindex $list_multicpes_testconds_logfile_availflag_ptnum 0]
    set testbed_logfile   [lindex $temp end-3];##
    set testbed_csv_logfname [file tail $testbed_logfile];##
    
    set list_fail_testcaseid_devid_bdptnum {}
    set list_fail_testcaseid {}
    
    #########################################
    # Log out multi CPEs DMT_NOS version
    #########################################
    set MiscCommon::verbose "on"
    MiscCommon::Log_MultiCPEsFWVer $str_args \
            $list_multicpes_testconds_logfile_availflag_ptnum
    
    #Check mdoem if TC router
    #####################################
    if {[info exist list_sock_multicpes] } {
        ##Set session timeout 500 miniutes
        set timeout_durtaion "500"
        foreach temp $list_sock_multicpes {
            set sock_desc_modem [lindex $temp end-1]
            set strmsg [Parse_TCModem::_SetDUT_Sysstdio $sock_desc_modem $timeout_durtaion]
            insertLogLine warn $strmsg
        };#foreach temp $list_sock_multicpes
    };#if {[llength $list_sock_multicpes] > 0}
    
    #############################################
    ##Get each DUT log file to store list
    ##############################################
    set list_pathlogfile {}
    foreach temp [lsort -index end $list_multicpes_testconds_logfile_availflag_ptnum] {
        set str_pathlogfile [lindex $temp end-2]
        lappend list_pathlogfile $str_pathlogfile
    };#foreach temp [lsort -index end $list_cpe_testconds_log_dslaminfo_relaybdptnum]
    
    foreach rowval $list_csvrow {
        ###########################################
        #Get Loop Len from csv file
        ###########################################
        Func_CSV::Loop_Type_BTLength $rowval
        
        ################################################################################
        #  Check multi CPEs variables
        #
        # list_multicpes_testconds_logfile_availflag_ptnum=
        # {00116 192.168.4.44 4 40 off ../Log/UR2v7_BRCM-B_03262007
        #     0           1   2  3  4            end-3
        #     ../Log/UR2v7_BRCM-B_DeviceID00116_03262007 pass  4}
        #                 end-2                          end-1 end   
        ################################################################################
        foreach temp [lsort -index end $list_multicpes_testconds_logfile_availflag_ptnum] {
            set strdutid [lindex $temp 0]
            set dut_ip    [lindex $temp 1]
            set waituptimes    [lindex $temp 2]
            set errorsecond [lindex $temp 3]
            set adslreset_iniopt [lindex $temp 4]
             
            set passflag [lindex $temp end-1]
            set logfile   [lindex $temp end-2]
            set bd_ptnum  [lindex $temp end]
            set csv_logfname [file tail $logfile]
            
            ################################
            ##Setup test environment
            ################################
            main_cycleone_testbed_setup $rowval $procom_inifile $testbed_logfile $testbed_csv_logfname \
                    $list_414ELenCoeff $list_410ALoopLen $list_410ACmdSet \
                    $list_noise_pathfname $list_dslamprofile $adslreset_iniopt
                    
            ###reset list_fail_testcaseid
            if [info exist list_fail_testcaseid] {unset list_fail_testcaseid}
            
            ## available port number in relay board
            if {[string tolower $passflag] == "pass"} {
                ################################
                ##Switch relay board
                ################################
                MiscCommon::ShowSwitchRelayBD $temp
                
                ############################################################
                #Waiting 5 seconds for moden to linkdown if "w adsl rest" off.
                ############################################################
                if {$adslreset_iniopt == "off"} {
                    set maxretry 1;set timeout_interval 5;set strinfo "For Modem to linkdown"
                    Test_NoiseMargin::Timer $maxretry $timeout_interval $strinfo $testbed_logfile \
                            $testbed_csv_logfname
                }
                
                ################################
                ##Show rest times
                ################################
                set strmsg "Total [llength $list_csvrow] test cases, \
                        current [incr testcase_counter] time(s)."
                insertLogLine warn $strmsg
                Log "info" $logfile [list $strmsg]
                
                #################################
                #Log Test plan Table name
                ################################
                set str_current_table [Func_CSV::TestplanTable $rowval]
                set str_current_testcaseid  [Func_CSV::TestcaseID $rowval]
                
                set strmsg "Test CaseID of $Func_INI::list_csvfname = $str_current_testcaseid; \
                        Current Table of Test plan= $str_current_table."
                insertLogLine warn $strmsg
                
                set retmsg [MiscCommon::LogTableNameMulitCPEsDSLAMs $Func_CSV::str_testplan_table \
                        $str_current_table $list_pathlogfile]
                # set retmsg [MiscCommon::LogTableName $Func_CSV::str_testplan_table \
                        # $str_current_table $logfile "$csv_logfname.csv"]
                ##store current test profile into cache variable
                set Func_CSV::str_testplan_table $str_current_table
                
                
                ########################################################
                #Dump DUT Modem data and Chk US/DS datarate pass or fail
                ########################################################
                set retestcounter 0
                set retest_times 3
                set str_passfail "fail"
                
                switch -regexp $strdutid {
                    {^[0-9][0-9][0-9][0-9][0-9]$} {
                        ###############################
                        ##Get each port socket channel
                        ###############################
                        set retsock [Func_Socket::Get_socketch_relaybdpt $list_sock_multicpes $bd_ptnum]
                        set sock_desc_modem $retsock
                        puts "In main_cycleone"
                        puts "list_sock_multicpes=$list_sock_multicpes "
                        puts "retsock=$retsock; sock_desc_modem=$sock_desc_modem"
                    }
                    default {
                        set Parse_TCPureBridge::COMPt_Num    $bd_ptnum
                    }
                };#switch -regexp $strdutid
                
                # Get adslreset_iniopt
                #######################################
                set list_adsldiag_adslrest [MiscCommon::main_cycleone_adslopt $adslreset_iniopt]
                
                while {$str_passfail != "pass"} {
                    incr retestcounter
                    if {$retestcounter > $retest_times} {
                        break;
                    };#if {$retestcounter > $retest_times}
                    
                    set strmsg [DUT_DumpInfo_Sel $strdutid $str_current_testcaseid \
                            $logfile $waituptimes $timeout $csv_logfname $rowval \
                            $list_adsldiag_adslrest $Func_INI::list_DSLAMInfo]
                    
                    ###pass,3584,736,5,5
                    ##  end-4    end-3     end-2    end-1  end
                    ####################
                    set list_strmsg [split $strmsg ,]
                    set str_passfail [lindex $list_strmsg end-4]
                    
                };#while {$str_passfail != "pass"}
                
                set adsldiag_opt "disable"; set adslrest_opt "disable"
                set list_adsldiag_adslrest [list $adsldiag_opt $adslrest_opt]
                set fail_testcaseid ""
                ################################
                #Waiting 60 seconds Error
                ################################
                switch -regexp $strmsg {
                    {False$} {
                        set overhead "False,,,,,,,,,,,,,,,,,,"
                        DUT_InfoFile  $logfile "$csv_logfname.csv" "False,$overhead" $strmsg
                        
                        ##Store failed test case ID
                        set fail_testcaseid $str_current_testcaseid
                    }
                    {fail} {
                        ##Store failed test case ID
                        set fail_testcaseid $str_current_testcaseid
                    }
                    default {
                        DUT_ErrorSecond $logfile $errorsecond;#60
                        
                        #Dump DUT Modem data by 3rd or TC modem
                        set strmsg [DUT_DumpInfo_Sel $strdutid $str_current_testcaseid \
                                $logfile $waituptimes $timeout $csv_logfname $rowval \
                                $list_adsldiag_adslrest $Func_INI::list_DSLAMInfo]
                        
                        set list_strmsg [split $strmsg ,]
                        set str_passfail [lindex $list_strmsg end-4]
                        if {[string tolower $str_passfail] != "pass" } {
                            set fail_testcaseid $str_current_testcaseid
                        } else  {
                            set fail_testcaseid ""
                        }
                        ;#if {[string tolower $strmsg] != "pass" }
                    }
                };#switch -regexp $strmsg
                
                ##############################
                ##Store failed test case ID
                ##############################
                if {[llength $fail_testcaseid] > 0} {
                    lappend list_fail_testcaseid $fail_testcaseid
                }
                
            };#if {[string tolower $passflag] == "pass"}
            
            if [info exist list_fail_testcaseid] {
                puts "list_fail_testcaseid=$list_fail_testcaseid"
                
                lappend list_fail_testcaseid_devid_bdptnum \
                        [list $list_fail_testcaseid $strdutid $bd_ptnum]
            };#if {[llength $list_fail_testcaseid] > 0}
            
        };#foreach temp [lsort -index end $list_multicpes_testconds_logfile_availflag_ptnum]
            
    };#foreach rowval $list_csvrow
    
    return $list_fail_testcaseid_devid_bdptnum
}

proc main {str_args list_csvrow list_multicpes_testconds_logfile_availflag_ptnum \
            list_414ELenCoeff list_410ALoopLen list_410ACmdSet \
            list_noise_pathfname list_dslamprofile} {
    
    global list_sock_multicpes
    global sock_desc_modem
    
    set list_args [split $str_args ";"]
    set looptimes [lindex $list_args 0]
    
    
    ####stor failed test case ID
    set list_fail_testcaseid_devid_bdptnum {}
    ####stor after 1st test failed test case ID
    set list_1stFail_csvrow {}
    
    set temp [lindex $list_multicpes_testconds_logfile_availflag_ptnum 0]
    set testbed_logfile   [lindex $temp end-3]
    
    #############################################
    ##Check which loop type and open noise source
    ##############################################
    set strmsg "Current Loop Modle= $Func_INI::str_LoopSimulator. Open Noise Source ON"
    insertLogLine warn $strmsg
    LoopCommon::Loop_OpenNoiseSource $Func_INI::str_LoopSimulator
    
    
    #########################################
    # Loop times star here
    #########################################
    for {set i 0} {$i < $looptimes} {incr i} {
        
        set list_fail_testcaseid_devid_bdptnum [main_cycleone $str_args $list_csvrow \
                $list_multicpes_testconds_logfile_availflag_ptnum \
                $list_414ELenCoeff $list_410ALoopLen $list_410ACmdSet \
                $list_noise_pathfname $list_dslamprofile]
        
        set retval [MiscCommon::ShowLogMultiCPEsFailTestCases $list_fail_testcaseid_devid_bdptnum \
                $list_multicpes_testconds_logfile_availflag_ptnum]
        
        if {$retval == "bypass"} {
            set  cpe_ptnum [lindex $list_multicpes_testconds_logfile_availflag_ptnum end]
            set testbed_logfile   [lindex $cpe_ptnum end-3];##
            set strmsg "All TestcaseID of all Devices Pass."
            insertLogLine critical $strmsg
            Log "info" $testbed_logfile [list $strmsg]
        }
        
    };#for {set i 0} {$i < $looptimes} {incr i}
    
    #Show complete msg
    Message::popup [list  "info" $::pref(appname) $::VERSION \
            "Congrulation!! Test Compeletion." $::COPYRIGHT]
    # exit
};#proc main

proc Noise_Disconnect {} {
    ################################
    #Close Noise Generator setting
    ################################
    set list_rtumsg [dls5204::Disconnect]
    insertLogLine info "Noise Soket Close status= $list_rtumsg"
    exit
}

################################################################################
# Start Here
################################################################################
set COPYRIGHT {
    Copyright (c) 2005, 2006 PQA, TrendChip
    This program is free to modify, extend at will.  The author(s)
    provides no warrantees, guarantees or any responsibility for usage.
}
set VERSION "MultiCPEs     If you have question, please contact PQA Dept."

array set pref [list \
        debug		0 \
        usesession	1 \
        sessionfile	[file join [file normalize $env(HOME)] .tksqlite]\
        appname		"TR067_TR100 AnnexA"  \
        enable_encoding	[lsort -dictionary [encoding names]] \
        recent_file [list] \
        openTypeSqlite	[list {"SQLite Files" {.db .db2 .db3 .sqlite}} {"All Files" *}] \
        openTypeSql		[list {"SQL Files" {.sql}} {"All Files" *}] \
        openTypeText	[list {"Text Files" {.txt .csv .tab .tsv .dat}} {"All Files" *}] \
        ]

set file_dls410A_looplen [file join $lib_path "Dls410A_LoopLength_Setting.csv"]
set file_dls410A_cmdset [file join $lib_path "Dls410ACommand.csv"]
set file_dls414E [file join $lib_path "Dls414E_Length_Coefficient.csv"]
set file_dslamprofile [file join $lib_path "DSLAMProfile.csv"]

set inifile [file join [file dirname [info script]] "setup_multicpes.ini"]
################################################################################
# Get INI file paramter
################################################################################
set Func_INI::verbose off;#on

Func_INI::_GetStd $inifile

set std [lindex $Func_INI::list_std 0]

Func_INI::_GetCSVFname $inifile
###Prefix library path to csv files
set list_csvfiles {}
foreach temp [lrange $Func_INI::list_csvfname 0 end] {
    set listtemp [file join $lib_path $temp]
    lappend list_csvfiles $listtemp
}

################################################################################
# Get wt100 testcase setting
################################################################################
set list_csvrow [Func_CSV::Read $list_csvfiles]
set list_414ELenCoeff [Func_CSV::Read [list $file_dls414E]]
set list_410ALoopLen [Func_CSV::Read [list $file_dls410A_looplen]]
set list_410ACmdSet  [Func_CSV::Read [list $file_dls410A_cmdset]]
set list_dslamprofile [Func_CSV::Read [list $file_dslamprofile]]
#############################################
# "00076" BRCM-A, "00071" BRCM-B
#############################################
Func_INI::_GetDSLAMInfo $inifile

set DSLAM_BRCM::DeviceID  [lindex $Func_INI::list_DSLAMInfo 0]
set DSLAM_BRCM::_verbose off;#"on"
# puts "Func_INI::list_DSLAMInfo=$Func_INI::list_DSLAMInfo"

switch -exact $DSLAM_BRCM::DeviceID  {
    "00071" {
        set DSLAM_BRCM::COMPt_Num    3
    }
    "00076" {
        set DSLAM_BRCM::COMPt_Num    3
    }
}

set DSLAM_BRCM::COMPt_DSLAMPasswd    "1234"
set procom_inifile "dslam.ini"
################################################################################
#Get test condition parameter from INI File. 
################################################################################
set timeout 10;set looptimes 1

################################################################################
# Loop parameter setting
################################################################################
Func_INI::_GetLoopSimulator $inifile
set TC_GPIB::verbose off;#off
set LoopCommon::verbose off;#off

set Loop410A::verbose off;#on
set Loop414E::verbose on;#on
set Loop400IAE::verbose on;#on

switch -regexp $Func_INI::str_LoopSimulator {
    {^DLS410A_ID00025$} {
        set Loop410A::GPIB411A "14"
        set Loop410A::GPIB412A "15"
    }
    {^DLS410A$} {
        set Loop410A::GPIB411A "15";#"14"
        set Loop410A::GPIB412A "16";#"15"
    }
    
};#switch -regexp $Func_INI::::str_LoopSimulator
#############################################
# IP Address of DLS5204/5500 Noise Generator
############################################
set ip5500   192.168.2.130
set ip5204   192.168.2.120
set ip5500_id00184   192.168.2.120

##Select port# by Loop Model
switch -regexp [string tolower $Func_INI::str_LoopSimulator] {
    {^(a|b)_dls400iae} {
        set port_noise 2006
    }
    default {
        set port_noise 2001
    }
};#switch -regexp [string tolower $Func_INI::str_LoopSimulator]

set Func_CSV::verbose off;#off
set NoiseCommon::::verbose off;#off

set path5204 ""
set dls5204::_verbose off;#off
set dls5204::_timeout 1000;#5000

# set dls5204::csv_nosiefname "noisefileDLS5204.csv"
set dls5204::csv_nosiefname "noisefileDLS5204_Jun282007.csv"
set dls5500::csv_nosiefname "Noise5500_Sep252006.csv"
# set dls5500::csv_nosiefname "noisefileDLS5500_Jun162006.csv"
set dls5500::csv_nosiefname_id00184 "NoiseDLS5500_ID00184.csv"

set csv_dls5204 [file join $lib_path $dls5204::csv_nosiefname]
set csv_dls5500 [file join $lib_path $dls5500::csv_nosiefname]
set csv_dls5500_id00184 [file join $lib_path $dls5500::csv_nosiefname_id00184]

set list_noise_pathfname {}

Func_INI::_GetNoiseGenerator $inifile

switch -regexp $Func_INI::str_NoiseGenerator {
    {^DLS5204} {
        set ip_noise $ip5204
        set list_noise_pathfname [Func_CSV::Read [list $csv_dls5204]]
        
    }
    {^DLS5500$} {
        set ip_noise $ip5500
        set list_noise_pathfname [Func_CSV::Read [list $csv_dls5500]]
    }
    {^DLS5500_ID00184$} {
        set ip_noise $ip5500_id00184
        set list_noise_pathfname [Func_CSV::Read [list $csv_dls5500_id00184]]
    }
}


#Check noise generator connection
set strmsg [dls5204::PingTest $ip_noise]
if {$strmsg == "false"} {
    set strmsg "Ping Noise Generator $ip_noise Fail!"
    #Show warming msg
    Message::popup [list  "warning" $::pref(appname) $::VERSION \
            "Check Noise Generator connection." $strmsg]
    exit
}

###############################
#Open Noise Generator setting
###############################
set list_rtumsg [dls5204::Connect $ip_noise $port_noise]
set strmsg "Noise Generator:$ip_noise connection ready."
#Show warming msg
Message::popup [list  "info" $::pref(appname) $::VERSION \
        $strmsg "Noise Socket ID= $list_rtumsg"]

###############################
#Select target testcases ID
###############################
Func_INI::_GetTestCasesID $inifile
set list_target_testcasesid [Func_INI::_GetTargetTestCasesID \
        $Func_INI::list_TestCasesID $list_csvrow]

###########################
#DUT modem setting
###########################
global sock_desc_modem telnet_line_modem

set modem_passwd "1234";#"1234"
set telnet_line_modem ""
set Func_Socket::_verbose "no";#off
set Parse_TCModem::verbose off
set Parse_TCPureBridge::verbose off;#off

################################################################################
# Get MultiCPEs parameters.
################################################################################
set Func_INI::verbose "off";#"on"
# Func_INI::_GetMultiCPEs $inifile
Func_INI::_GetMultiCPEsTestConds $inifile

################################################################################
# Log out aviable moden log file name.
################################################################################
set timestamp [clock format [clock seconds] -format "%m%d%Y"]

set stdname [lindex $Func_INI::list_std 0]
set stdver [lindex $Func_INI::list_std end]
set strdslam_devid    [lindex $Func_INI::list_DSLAMInfo 0]
set str_targetdslam [DSLAM_Common::_GetDSLAMModel $strdslam_devid]


set list_mutlicpes_testconds_log_ptinfo {}
##Gen mulitcpes logfilename
foreach list_relaybd_ptinfo $Func_INI::list_MultiCPEs_TestCondition {
    set strdutid [lindex $list_relaybd_ptinfo 0]
    set strdutip [lindex $list_relaybd_ptinfo 1]
    set strptnum [lindex $list_relaybd_ptinfo 2]
    
    set str_tcmodem_model [MiscCommon::GetTCCPEChipsets $strdutid]
    if [info exist strlogfname] {unset strlogfname}
    if [info exist strtestbed_logfname] {unset strtestbed_logfname}
    if [info exist logfile] {unset logfile}
            
    append strlogfname $stdname$stdver "_" $str_targetdslam "_" $str_tcmodem_model \
            "_" $timestamp        
    append strtestbed_logfname $stdname$stdver "_" $str_targetdslam "_" $timestamp
    
    set logfile [file join  $log_path $strlogfname]
    set testbed_logfile [file join  $log_path $strtestbed_logfname]
    
    set newList_old [linsert $list_relaybd_ptinfo end-1 $testbed_logfile]
    set newList [linsert $newList_old end-1 $logfile]
    lappend list_mutlicpes_testconds_log_ptinfo $newList
}

foreach list_id_ip_logfname_bdptnum $list_mutlicpes_testconds_log_ptinfo {
    set str_passfail "pass"
    set newList [linsert $list_id_ip_logfname_bdptnum end-1 $str_passfail]
    lappend list_multicpes_testconds_logfile_availflag_ptnum $newList
}

##Show Multi CPEs info on widget
##################################
MiscCommon::ShowMultiCPEsonWidget $list_multicpes_testconds_logfile_availflag_ptnum

#########################################
# Turn on/off test result "pass/fail" log
#########################################
set MiscCommon::verbose "off"

################################################################################
# Run Here
################################################################################
append  str_title $pref(appname) " " Version ":" $VERSION
    
if [info exist str_args] {unset str_args}
append str_args $looptimes ";" $modem_passwd ";" $procom_inifile ";" $timeout
        # $waituptimes ";" $timeout ";" $errorsec_duration ";" $adslreset_iniopt
                                                      
GUIRun $str_title $str_args $list_target_testcasesid $list_multicpes_testconds_logfile_availflag_ptnum \
        $list_414ELenCoeff $list_410ALoopLen $list_410ACmdSet \
        $list_noise_pathfname $list_dslamprofile
                