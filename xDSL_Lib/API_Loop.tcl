# ChangeLog
# By Philip Shen @PQA TrendChip Corp.
# ---------------------------------------------------------------------
# Apr27-2006  Initialize to comeout GPIB cmd string.
# May02-2006  Initialize to namespace Log
# Jun21-2006  Initialize to namespace Func_CSV to parse 410A and 414E command
# Aug30-2006 Verify Func_CSV::Read to read multiple csv files.
# Sep09-2006  Correct BT, ANSI, CSA of 410A GPIB Cmd set.
# Sep11-2066  Modify Loop410A::GetLoopBTLenIdx list index
# Sep16-2066 Correct proc Loop410A::_SendCmd
# Oct27-2066  Add both proc Loop414E::_NoiseSourceON and proc Loop414E::_NoiseSourceON
# Nov05-2006  Add proc LoopCommon::Loop_GPIBSendCmd
# Dec23-2006  Enhance namespace Loop400IAE
#             Add proc Loop400IAE::LoopBTGPIBCmdSet
# Dec24-2006  Add proc Loop400IAE::ANSI_13GPIBCmdSet and proc Loop400IAE::CSA_4GPIBCmdSet
#             Add 400IAE part in both LoopCommon::Loop_GPIBSendCmd 
#                 and LoopCommon::Loop_OpenNoiseSource
# Jan01-2007  Add strTemp1 in porc Loop400IAE::IAE2
################################################################################

namespace eval Loop400IAE {
    variable list_looplen_IAE2 { "19,1500" "14,1500" "7,1500" "5,1500"\
                "3,1500"}
    variable list_looplen_IAE2_18k { "19,1550" "14,1550" "7,1550" "5,1550"\
                "3,1550"}
    variable list_lead_looplen_IAE2 { "19," "14," "7," "5," "3," "1,"}
    variable list_400IAE1CmdSet {}
    variable list_400IAE2CmdSet {}
    variable verbose        off
    variable logfile        "../log/loop400IAE.log"
    variable GPIB400IAE1     "14"
    variable GPIB400IAE2     "15"
}
################################################################################
# VB original source code as below
# StrTmp1 = ":SET :CHANNEL :BYPASS NO"
# StrTmp2 = ":SET :CHAN :LOOP VARIABLE_26_AWG_IAE"
# StrTmp10 = ":SETTING:CHANNEL:DIRECTION Forward"
# 
#  Case "4000"
# 'Set 400IAE1 Loop Simulator
# StrTmp3 = ":SET :CHAN :LINE 2000 FT"
# 'Set 400IAE2 Loop Simulator
# StrTmp4 = ":SETTING:SLOT 14,500 ft,INLINE"
# StrTmp5 = ":SETTING:SLOT 19,1500 ft,INLINE"
# 
# Case "18000"
# 'Set 400IAE1 Loop Simulator
# StrTmp3 = ":SET :CHAN :LINE 9000 FT"
# 'Set 400IAE2 Loop Simulator
# StrTmp4 = ":SETTING:SLOT 1,1250 ft,INLINE"
# StrTmp5 = ":SETTING:SLOT 3,1550 ft,INLINE"
# StrTmp6 = ":SETTING:SLOT 5,1550 ft,INLINE"
# StrTmp7 = ":SETTING:SLOT 7,1550 ft,INLINE"
# StrTmp8 = ":SETTING:SLOT 14,1550 ft,INLINE"
# StrTmp9 = ":SETTING:SLOT 19,1550 ft,INLINE"
# Call Write_GPIB(GPIB_Device(Val(Cmd_arr(1))), StrTmp3; StrTmp10)
# Call Write_GPIB(GPIB_Device(Val(Cmd_arr(2))), StrTmp1; StrTmp4; StrTmp5)
# Call Write_GPIB(GPIB_Device(Val(Cmd_arr(2))), StrTmp6; StrTmp7; StrTmp8)
# Call Write_GPIB(GPIB_Device(Val(Cmd_arr(2))), StrTmp9)
################################################################################
proc Loop400IAE::IAE1 {{str_looplen ""}} {
    variable verbose
    variable logfile
    variable list_400IAE1CmdSet {}
    set str_lencmd_IAE1 ""

    if {$str_looplen != ""} {
        switch -- $str_looplen {
            "4600" {
                set len_IAE1 "2000"
            }
            "13800" {
                set len_IAE1 "6500"
            }
            default {
                set len_IAE1 [expr $str_looplen / 2]
            }
        };#switch -- $str_looplen
        
        append str_lencmd_IAE1 ":SET :CHAN :LINE " $len_IAE1 " FT"
    } else  {
        return false
    };#if {$str_looplen != ""}

    set StrTmp10 ":SETTING:CHANNEL:DIRECTION Forward"
    lappend list_len_gbip_IAE1 $str_lencmd_IAE1 $StrTmp10
    set list_400IAE1CmdSet    $list_len_gbip_IAE1
    
    if {$verbose == on} {
        Loop414E::Log "info" $logfile [list "looplen= $str_looplen" \
                "400IAE1_GPIBCmdSet=$list_400IAE1CmdSet"]
    };#if {$verbose == on}
    return true
}

proc Loop400IAE::IAE2_looplen {idxlist idxappend \
                                        list_looplen_IAE2} {
    variable list_lead_looplen_IAE2
    set list_lencmd_IAE2 {}
    
    for {set x 0} {$x < $idxlist} {incr x} {
        if [info exist strtemp] {
            unset strtemp;#clear up variable strtemp
        }
        append strtemp ":SETTING:SLOT " \
                [lindex $list_looplen_IAE2 $x] " ft,INLINE"
        lappend list_lencmd_IAE2 $strtemp
    };#for {set x 0} {$x < $idxlist} {incr x}
    
    if {$idxappend != 0} {
        if [info exist strtemp] {
            unset strtemp;#clear up variable strtemp
        }
        append strtemp ":SETTING:SLOT " \
                [lindex $list_lead_looplen_IAE2 $idxlist] \
                $idxappend " ft,INLINE"
        lappend list_lencmd_IAE2 $strtemp
    };#if {$idxappend != 0}
    
    return $list_lencmd_IAE2
}

proc Loop400IAE::IAE2 {{str_looplen ""}} {
    variable verbose
    variable logfile
    variable list_400IAE2CmdSet {}
    variable list_looplen_IAE2
    variable list_looplen_IAE2_18k
    set list_lencmd_IAE2 {}
    set StrTmp1 ":SET :CHANNEL :BYPASS NO"
    
    if {$str_looplen != ""} {
        set half [expr $str_looplen / 2]
        set idxlist [expr $half / 1500]
        set idxappend [expr $half % 1500]
        
        switch -- $str_looplen {
            "0" {
                return $list_lencmd_IAE2
            }
            "4600" {
                set rest [expr $str_looplen - 2000]
                set idxlist [expr $rest / 1500]
                set idxappend [expr $rest % 1500]
                set list_lencmd_IAE2 [IAE2_looplen $idxlist $idxappend \
                        $list_looplen_IAE2]
            }
            "13800" {
                set rest [expr $str_looplen - 6500]
                set idxlist [expr $rest / 1500]
                set idxappend [expr $rest % 1500]
                set list_lencmd_IAE2 [IAE2_looplen $idxlist $idxappend \
                        $list_looplen_IAE2]
            }
            "18000" {
                set idxlist [expr [expr $str_looplen / 2] / 1550]
                set idxappend [expr [expr $str_looplen / 2] % 1550]
                set list_lencmd_IAE2 [IAE2_looplen $idxlist $idxappend \
                        $list_looplen_IAE2_18k]
            }
            default {
                set list_lencmd_IAE2 [IAE2_looplen $idxlist $idxappend \
                        $list_looplen_IAE2]
            }
        };#switch -- $str_looplen
        
    } else  {
       return false 
    };#if {$str_looplen != ""}
    
    set list_400IAE2CmdSet [linsert $list_lencmd_IAE2 0 $StrTmp1]
    
    if {$verbose == on} {
        Loop414E::Log "info" $logfile [list "looplen= $str_looplen" \
                "400IAE2_GPIBCmdSet=$list_400IAE2CmdSet"]
    };#if {$verbose == on}
    
    return true
}
################################################################################
# StrTmp1 = ":SET :CHANNEL :BYPASS NO"
# StrTmp2 = ":SET :CHAN :LOOP VARIABLE_26_AWG_IAE"
# StrTmp10 = ":SETTING:CHANNEL:DIRECTION Forward"
# 
# Case "9K1500"
# 'Set 400IAE1 Loop Simulator
# StrTmp3 = ":SET :CHAN :LINE 4500 FT"
# 'Set 400IAE2 Loop Simulator
# StrTmp4 = ":SETTING:SLOT 7,1500 ft,INLINE"
# StrTmp5 = ":SETTING:SLOT 14,1500 ft,INLINE"
# StrTmp6 = ":SETTING:SLOT 19,1500 ft,INLINE"
# StrTmp7 = ":SETTING:SLOT 22,1500 ft,TAP"
# Call Write_GPIB(GPIB_Device(Val(Cmd_arr(1))), StrTmp2)
#  StrTmp3 StrTmp10)
# 
#  StrTmp1, StrTmp4, StrTmp5, StrTmp6, StrTmp7)
#
# Case "12K1500"
# 'Set 400IAE1 Loop Simulator
# StrTmp3 = ":SET :CHAN :LINE 6000 FT"
# 'Set 400IAE2 Loop Simulator
# StrTmp4 = ":SETTING:SLOT 5,1500 ft,INLINE"
# StrTmp5 = ":SETTING:SLOT 7,1500 ft,INLINE"
# StrTmp6 = ":SETTING:SLOT 14,1500 ft,INLINE"
# StrTmp7 = ":SETTING:SLOT 19,1500 ft,INLINE"
# StrTmp8 = ":SETTING:SLOT 22,1500 ft,TAP"
# Call Write_GPIB(GPIB_Device(Val(Cmd_arr(1))), StrTmp2)
# StrTmp3, StrTmp10)
# 
# StrTmp1, StrTmp4, StrTmp5, StrTmp6, StrTmp7, StrTmp8)
#
# Case "175K0"
# 'Set 400IAE1 Loop Simulator
# StrTmp3 = ":SET :CHAN :LINE 8750 FT"
# 'Set 400IAE2 Loop Simulator
# StrTmp4 = ":SETTING:SLOT 1,1250 ft,INLINE"
# StrTmp5 = ":SETTING:SLOT 3,1500 ft,INLINE"
# StrTmp6 = ":SETTING:SLOT 5,1500 ft,INLINE"
# StrTmp7 = ":SETTING:SLOT 7,1500 ft,INLINE"
# StrTmp8 = ":SETTING:SLOT 14,1500 ft,INLINE"
# StrTmp9 = ":SETTING:SLOT 19,1500 ft,INLINE"
# StrTmp11 = ":SETTING:SLOT 22,0 ft,TAP"
# Call Write_GPIB(GPIB_Device(Val(Cmd_arr(1))), StrTmp2)
# StrTmp3, StrTmp10)
# 
# Call Write_GPIB(GPIB_Device(Val(Cmd_arr(2))), StrTmp1)
# StrTmp4, StrTmp5, StrTmp6, StrTmp7, StrTmp8, StrTmp9, StrTmp11)
################################################################################

proc Loop400IAE::LoopBTGPIBCmdSet {{str_looplen ""} {str_btlen ""}} {
    variable list_400IAE1CmdSet {}
    variable list_400IAE2CmdSet {}
    variable list_looplen_IAE2
    variable verbose
    variable logfile
    
    set StrTmp2 ":SET :CHAN :LOOP VARIABLE_26_AWG_IAE"
    set StrTmp10 ":SETTING:CHANNEL:DIRECTION Forward"
    set list_bt_slotidx {}
    
    set len_IAE1 [expr $str_looplen / 2]
    append str_lencmd_IAE1 ":SET :CHAN :LINE " $len_IAE1 " FT"
    
    lappend list_400IAE1CmdSet $StrTmp2 $str_lencmd_IAE1 $StrTmp10
    
    if {$str_looplen != ""} {
        set half [expr $str_looplen / 2]
        set idxlist [expr $half / 1500]
        set idxappend [expr $half % 1500]
        set list_lencmd_IAE2 [IAE2_looplen $idxlist $idxappend $list_looplen_IAE2]
    } else  {
        return false
    };#if {$str_looplen != ""}
    
    if {$str_btlen != ""} {
        append StrTmp11 ":SETTING:SLOT 22," $str_btlen " ft,TAP"
    } else  {
        return false
    }
    
    set list_400IAE2CmdSet [linsert $list_lencmd_IAE2 end $StrTmp11]
    
    if {$verbose == on} {
        Loop414E::Log "info" $logfile [list "looplen= $str_looplen" \
                "400IAE1_GPIBCmdSet=$list_400IAE1CmdSet"]
        Loop414E::Log "info" $logfile [list "BTlen= $str_btlen" \
                "400IAE2_GPIBCmdSet=$list_400IAE2CmdSet"]        
    };#if {$verbose == on}
    
    return true
}
################################################################################
# StrTmp1 = ":SET :CHANNEL :BYPASS NO"
# StrTmp2 = ":SET :CHAN :LOOP VARIABLE_26_AWG_IAE"
# StrTmp10 = ":SETTING:CHANNEL:DIRECTION Forward"
#  Case "CSA_4"
# 'Set 400IAE1 Loop Simulator
# StrTmp3 = ":SET :CHAN :LOOP CSA_#4IAE"
# 'Set 400IAE2 Loop Simulator
# StrTmp4 = ":SETTING:SLOT 1,250 ft,INLINE"
# StrTmp5 = ":SETTING:SLOT 3,1500 ft,INLINE"
# StrTmp6 = ":SETTING:SLOT 5,1500 ft,INLINE"
# StrTmp7 = ":SETTING:SLOT 7,800 ft,INLINE"
# StrTmp8 = ":SETTING:SLOT 14,800 ft,INLINE"
# Call Write_GPIB(GPIB_Device(Val(Cmd_arr(1))), StrTmp3)
# Call Write_GPIB(GPIB_Device(Val(Cmd_arr(1))), StrTmp10)
# 
# StrTmp1, StrTmp4, StrTmp5, StrTmp6, StrTmp7, StrTmp8)
# 
# Case "ANSI_13"
# 'Set 400IAE1 Loop Simulator
# StrTmp3 = ":SET :CHAN :LOOP ANSI_#13IAE"
# 'Set 400IAE2 Loop Simulator
# StrTmp4 = ":SETTING:SLOT 7,1500 ft,INLINE"
# StrTmp5 = ":SETTING:SLOT 9,500 ft,INLINE"
# StrTmp6 = ":SETTING:SLOT 14,1500 ft,INLINE"
# StrTmp7 = ":SETTING:SLOT 16,500 ft,INLINE"
# Call Write_GPIB(GPIB_Device(Val(Cmd_arr(1))), StrTmp3)
# Call Write_GPIB(GPIB_Device(Val(Cmd_arr(1))), StrTmp10)
# 
#  StrTmp1, StrTmp4, StrTmp5, StrTmp6, StrTmp7)
# 
# Case "MIDCSA_6"
# 'Set 400IAE1 Loop Simulator
# StrTmp3 = ":SET :CHAN :LOOP MID-CSA_#6IAE"
# 'Set 400IAE2 Loop Simulator
# StrTmp4 = ":SETTING:SLOT 7,1500 ft,INLINE"
# StrTmp5 = ":SETTING:SLOT 14,1500 ft,INLINE"
# Call Write_GPIB(GPIB_Device(Val(Cmd_arr(1))), StrTmp3)
# Call Write_GPIB(GPIB_Device(Val(Cmd_arr(1))), StrTmp10)
# 
#  StrTmp1, StrTmp4, StrTmp5)
################################################################################

proc Loop400IAE::ANSI_13GPIBCmdSet { str_looptype} {
    variable list_400IAE1CmdSet {}
    variable list_400IAE2CmdSet {}
    variable verbose
    variable logfile
    
    set StrTmp3 ":SET :CHAN :LOOP ANSI_#13IAE"
    set StrTmp10 ":SETTING:CHANNEL:DIRECTION Forward"
    set StrTmp1 ":SET :CHANNEL :BYPASS NO"
    set StrTmp4 ":SETTING:SLOT 1,250 ft,INLINE"
    set StrTmp5 ":SETTING:SLOT 3,1500 ft,INLINE"
    set StrTmp6 ":SETTING:SLOT 5,1500 ft,INLINE"
    set StrTmp7 ":SETTING:SLOT 7,800 ft,INLINE"
    set StrTmp8 ":SETTING:SLOT 14,800 ft,INLINE"
    
    lappend list_400IAE1CmdSet $StrTmp3 $StrTmp10
    lappend list_400IAE2CmdSet $StrTmp1 $StrTmp4 $StrTmp5 $StrTmp6 $StrTmp7
    
    if {$verbose == on} {
        Loop414E::Log "info" $logfile [list "loop type= $str_looptype" \
                "400IAE1_GPIBCmdSet=$list_400IAE1CmdSet"]
        Loop414E::Log "info" $logfile [list "loop tyep= $str_looptype" \
                "400IAE2_GPIBCmdSet=$list_400IAE2CmdSet"]
    };#if {$verbose == on}
    
    return true
}
proc Loop400IAE::CSA_4GPIBCmdSet {str_looptype} {
    variable list_400IAE1CmdSet {}
    variable list_400IAE2CmdSet {}
    variable verbose
    variable logfile
    
    set StrTmp3 ":SET :CHAN :LOOP CSA_#4IAE"
    set StrTmp10 ":SETTING:CHANNEL:DIRECTION Forward"
    set StrTmp1 ":SET :CHANNEL :BYPASS NO"
    set StrTmp4 ":SETTING:SLOT 1,250 ft,INLINE"
    set StrTmp5 ":SETTING:SLOT 3,1500 ft,INLINE"
    set StrTmp6 ":SETTING:SLOT 5,1500 ft,INLINE"
    set StrTmp7 ":SETTING:SLOT 7,800 ft,INLINE"
    set StrTmp8 ":SETTING:SLOT 14,800 ft,INLINE"
    
    lappend list_400IAE1CmdSet $StrTmp3 $StrTmp10
    lappend list_400IAE2CmdSet $StrTmp1 $StrTmp4 $StrTmp5 $StrTmp6 $StrTmp7 $StrTmp8
    
    if {$verbose == on} {
        Loop414E::Log "info" $logfile [list "loop type= $str_looptype" \
                "400IAE1_GPIBCmdSet=$list_400IAE1CmdSet"]
        Loop414E::Log "info" $logfile [list "loop type= $str_looptype" \
                "400IAE2_GPIBCmdSet=$list_400IAE2CmdSet"]
    };#if {$verbose == on}
    
    return true
}
proc Loop400IAE::_SendCmd {list_dev_gpibaddr {list_400IAE1GPIBcmdset {}} \
            {list_400IAE2GPIBcmdset {}} {logfile ""}} {
    TC_GPIB::clear_existed_device
    ################################
    #Open GPIB setting
    ################################
    set list_opendevice [TC_GPIB::opendevice $list_dev_gpibaddr]
        
    ###400IAE1
    if {[llength $list_400IAE1GPIBcmdset] > 0} {
        TC_GPIB::write_listcmd [lindex $list_opendevice 0] $list_400IAE1GPIBcmdset
    }
    ###400IAE2
    if {[llength $list_400IAE2GPIBcmdset] > 0} {
        TC_GPIB::write_listcmd [lindex $list_opendevice 1] $list_400IAE2GPIBcmdset
    }
    
    ################################
    #Close GPIB setting
    ################################
    TC_GPIB::close $list_opendevice
    
    return true
}
proc Loop400IAE::_NoiseSourceON {list_dev_gpibaddr} {
    TC_GPIB::clear_existed_device
    ################################
    #Open GPIB setting
    ################################
    set list_opendevice [TC_GPIB::opendevice $list_dev_gpibaddr]
    TC_GPIB::write_listcmd [lindex $list_opendevice 0] \
            [list ":Setting:Channel:State NORMAL" ":SourceA:Noise ON"]
    TC_GPIB::write_listcmd [lindex $list_opendevice 1] \
            [list ":Setting:Channel:State NORMAL" ":SourceB:Noise ON"]
    
    ################################
    #Close GPIB setting
    ################################
    TC_GPIB::close $list_opendevice
    
    return true
}
################################################################################
# 
################################################################################

namespace eval Func_CSV {
    package require csv
    package require struct
    
    variable verbose off
    variable logfile "../log/csvfile.log"
    
    variable list_noisepathfname_ChNum {}
    variable str_noisepathfname_CO ""
    variable str_noisepathfname_CPE ""
    
    variable LoopLen
    variable BTLen
    variable TestProfile ""
    variable sepChar ","
    variable str_testplan_table ""
    variable LoopType ""
}
proc Func_CSV::Read {list_csvfname} {
    
    variable sepChar
    variable verbose
    variable logfile
    
    if {[llength $list_csvfname] == 0} {
        set list_csvfname -
    }
    
    set stdin 1
    set first 1
    if [info exists m] {m destroy }
    
    # struct::matrix m
    set list_csv {}
    
    foreach f $list_csvfname {
        if {![string compare $f -]} {
            if {!$stdin} {
                puts stderr "Cannot use - (stdin) more than once"
                exit -1
            }
            set in stdin; set stdin 0
        } else {
            set in [open $f r]
        };#if {![string compare $f -]}
        
        if {$first} {
            if {$verbose == on} {
                Log "info" $logfile [list "CSV File= $f"]
            }
            
            while {[eof $in] != 1} {
                if {[gets $in line] > 0} {
                    switch -regexp $line  {
                        {^(#|;)} {
                            continue
                        }
                        {([0-9]|[0-9][0-9]|[0-9][0-9][0-9]),} {
                            set list_data [::csv::split $line $sepChar]
                        }
                        {([0-9][0-9][0-9][0-9]),} {
                            set list_data [::csv::split $line $sepChar]
                        }
                        default {
                            set list_data [::csv::split $line $sepChar]
                        }
                    };#switch -regexp $linr
                    # m add columns [llength $list_data]
                    # m add row $list_data
                    lappend list_csv $list_data
                };#if {[gets $in line] > 0}
                
            };#while {[eof $in] != 1}
        };#if {$first}
        
        # csv::read2matrix $in m $sepChar
        
        if {[string compare $f -]} {
            close $in
        }
    };#foreach f $list_csvfname
    
    return $list_csv
}
################################################################################
# list row content as below
# {0037 A_1_4_2-AWGN AU_RA_I_30000k 26AWG 100 0 DLS5500  -na-    -na-    White_Noise_td.enc  White_Noise_td.enc}
#    0    1            2            3     4   5 6        7(ch1)   8(ch2)          9(ch3)        10(ch4)
#
#/ETSI/ADSL_Over_ISDN_FDD-ModFB_5B30v2-0/DnStream_atRem/ETSI_Lp1,FDD_AI_ModFB_R_ETSI_Lp1_2750m_xtk.enc
#/ETSI/ADSL_Over_ISDN_FDD-ModFB_5B30v2-0/UpStream_atCo/ETSI_Lp1,FDD_AI_ModFB_C_ETSI_Lp1_2750m_xtk.enc
#################################################################################
proc Func_CSV::_List_NoisePathFname {list_row list_noisepathfname} {
    variable list_noisepathfname_ChNum
    variable verbose
    variable logfile
    
    set prefix_path "C:\\Program\ Files\\Spirent\ Communications\\DLS\ 5204\\NoiseFiles"
    set list_noisepathfname_ChNum {}
    
    foreach temp [lrange $list_row 7 10] {
        switch -regexp $temp  {
            puts "In Func_CSV::_List_NoisePathFname temp=$temp"
            {(dat|enc)$} {
                foreach csvtemp $list_noisepathfname {
                    # set csv_noisefname [lindex $csvtemp 1]
                    set csv_noisefname [lindex $csvtemp end]
                    
                    if {$csv_noisefname==$temp} {
                        set csv_nosiepath [lindex $csvtemp 0]
                        
                        set path_noiseChNum [file nativename $csv_nosiepath]
                        ;#return "\DSL Forum\WT-100\Annex_A\A-1-4-2"
                        if [info exists str_noisepathfname_ChNum] {
                            unset str_noisepathfname_ChNum
                        }
                        
                        append str_noisepathfname_ChNum $prefix_path \
                                $path_noiseChNum "\\" $temp
                        
                        lappend list_noisepathfname_ChNum $str_noisepathfname_ChNum
                        
                        break;#force break
                    };#if {$csv_noisefname==$temp}
                    
                };#foreach csvtemp $$dls5500::list_CSVValue
            }
            
            default {
                lappend list_noisepathfname_ChNum $temp
            }
        }
        #;switch -regexp $temp
        puts "In Func_CSV::_List_NoisePathFname \
                list_noisepathfname_ChNum=$list_noisepathfname_ChNum"
        
        if {$verbose == "on"} {
            Log "info" $logfile $list_noisepathfname_ChNum
        }
        
    };#foreach temp [lrange $list_row 7 10]
    
    return true                
}
################################################################################
# list row content as below
# {0037 A_1_4_2-AWGN AU_RA_I_30000k 26AWG 100 0 DLS5500 {/DSL Forum/WT-100/Annex_A/A-1-4-2/} White_Noise_td.enc {/DSL Forum/WT-100/Annex_A/A-1-4-2/} White_Noise_td.enc}
#    0    1            2            3     4   5 6        7                                   8                   9                                   10
################################################################################

proc Func_CSV::NoisePathFname {list_row} {
    variable str_noisepathfname_CO
    variable str_noisepathfname_CPE
    
    set prefix_path "C:\\Program\ Files\\Spirent\ Communications\\DLS\ 5204\\NoiseFiles"
    set path_CO [file nativename [lindex $list_row 7]];#return "\DSL Forum\WT-100\Annex_A\A-1-4-2"
    set path_CPE [file nativename [lindex $list_row 9]]
    set noisefname_CO [lindex $list_row 8]
    set noisefname_CPE [lindex $list_row 10]
    #puts "$path_CO; $noisefname_CO; $path_CPE; $noisefname_CPE"
    if [info exists str_noisepathfname_CO] {
        unset str_noisepathfname_CO
    }
    if [info exists str_noisepathfname_CPE] {
        unset str_noisepathfname_CPE
    }
    append str_noisepathfname_CO $prefix_path $path_CO "\\" $noisefname_CO
    append str_noisepathfname_CPE $prefix_path $path_CPE "\\" $noisefname_CPE
    return true
}
################################################################################
# For both CSA#4 STANDARD LOOP and ANSI#13 STANDARD LOOP
################################################################################
proc  Func_CSV::Loop_Type_BTLength {list_row} {
    variable LoopType
    variable LoopLen
    variable BTLen
    
    set LoopType [lindex $list_row 3]
    set LoopLen [lindex $list_row 4]
    set BTLen [lindex $list_row 5]
    
    return true
}
proc Func_CSV::Profile {list_row} {
    variable TestProfile
    
    set TestProfile [lindex $list_row 2]
    return true
}
proc Func_CSV::TestplanTable {list_row} {
    
    set str_tablename [lindex $list_row 1]
    return $str_tablename
}
proc Func_CSV::TestcaseID {list_row} {
    
    set str_testcaseid [lindex $list_row 0]
    return $str_testcaseid
}
proc Func_CSV::Log {level filename list_msg} {
    foreach temp $list_msg {
        set msg "$temp"
        Log::tofile $level $filename $msg
    }
}
################################################################################
# namespace eval LoopCommon
#     Store both loop and BT lenghth of current test case
################################################################################
namespace eval LoopCommon {
    variable verbose        off
    variable logfile        "../log/loopCommon.log"
    
    variable strLoopLen ""
    variable strLoopBT ""
    variable strLoopType ""
    variable list_Loop_GPIBAddr {}
    
}
proc LoopCommon::Loop_GPIBAddress {looptype} {
    variable verbose
    variable logfile
    variable list_Loop_GPIBAddr
    set GPIB411A    16
    set GPIB412A    17
    set GPIB414E    18
    
    switch -regexp $looptype {
        {^DLS414E} {
            set list_Loop_GPIBAddr [list $GPIB414E]
        }
        ;#{^DLS414E}
        {^DLS410A} {
            set list_Loop_GPIBAddr [list $GPIB411A $GPIB412A]
        }
        ;#{^DLS410A}
    };#switch -regexp $looptype
    
    if {$verbose == on} {
        set listlog "looptype=$looptype; list_Loop_GPIBAddr=$list_Loop_GPIBAddr"
        Func_INI::PntMsg "$listlog"
        
        Log "info" $logfile $listlog
    }
    return true
}
proc LoopCommon::Loop_OpenNoiseSource {loopmodle} {
    switch -regexp $loopmodle {
        {^DLS414E} {
            set list_dev_gpibaddr [list $Loop414E::GPIB414E]
            Loop414E::_NoiseSourceON $list_dev_gpibaddr
        }
        {^DLS410A} {
            set list_dev_gpibaddr [list $Loop410A::GPIB411A $Loop410A::GPIB412A]
            Loop410A::_NoiseSourceON $list_dev_gpibaddr
        }
        default {
        }
    };#switch -regexp $Func_INI::str_LoopSimulator
    
    return true
}
proc LoopCommon::Loop_GPIBSendCmd {list_looptypeinfo list_loopinfo logfile csv_logfname list_414ELenCoeff list_410ALoopLen list_410ACmdSet} {
    ###################################################
    ## list_looptypeinfo
    ## [list $Func_CSV::LoopType $Func_CSV::LoopLen $Func_CSV::BTLen]
    ##list_loopinfo as below
    ##[list $Func_INI::str_LoopSimulator $Loop410A::GPIB411A $Loop410A::GPIB412A $Loop414E::GPIB414E]
    #####################################################
    # Log "info" $logfile [list $LoopCommon::strLoopLen $Func_CSV::LoopLen \
    # $LoopCommon::strLoopBT $Func_CSV::BTLen]
    set str_looptype [lindex $list_looptypeinfo 0]
    set str_looplen [lindex $list_looptypeinfo 1]
    set str_btlen [lindex $list_looptypeinfo 2]
    
    set str_loopsimulator [lindex $list_loopinfo 0]
    set str_gpib411A [lindex $list_loopinfo 1]
    set str_gpib412A [lindex $list_loopinfo 2]
    set str_gpib414E [lindex $list_loopinfo 3]
    set str_gpib400IAE1 [lindex $list_loopinfo 4]
    set str_gpib400IAE2 [lindex $list_loopinfo 5]
    
    #Check what kind of Loop Modle
    switch -regexp $str_loopsimulator {
        {^DLS414E} {
            ##unit from ft to m
            # set m [Loop414E::unit_fttom $Func_CSV::LoopLen]
            # set strmsg "feet=$Func_CSV::LoopLen; meter=$m"
            # Loop414E::LoopLength $m $list_414ELenCoeff
            
            ##unit meter
            set strmsg "In LoopCommon::Loop_GPIBSendCmd: LoopLen meter=$str_looplen"
            Loop414E::LoopLength $str_looplen $list_414ELenCoeff
            
            Loop414E::GPIBCmd
            ###########################################################
            #414E GPIB command setting
            ###[list $Func_INI::str_LoopSimulator $Loop410A::GPIB411A \
            ###$Loop410A::GPIB412A $Loop414E::GPIB414E]
            #########################################################
            set list_dev_gpibaddr [list $str_gpib414E]
            Loop414E::_SendCmd $list_dev_gpibaddr $Loop414E::strdls414Cmd $logfile
        }
        
        {^DLS410A} {
            ################################
            #Check Loop Type CSA or ANSI
            ################################
            set list_410ALoopBTLenIdx [Loop410A::LoopBTLength $str_looplen \
                    $str_btlen $list_410ALoopLen]
            #Checking input loop length is correct.
            if {[llength $list_410ALoopBTLenIdx] > 0} {
                set strmsg "In LoopCommon::Loop_GPIBSendCmd: Loop=$str_looplen; BT=$str_btlen"
                
                #first element is T1.413; second one is G996.1
                Loop410A::LoopBTGPIBCmdSet [lindex $list_410ALoopBTLenIdx 0] \
                        $list_410ACmdSet
            } else  {
                ######################################################
                ## list_looptypeinfo
                ## [list $Func_CSV::LoopType $Func_CSV::LoopLen $Func_CSV::BTLen]
                #######################################################
                set strmsg "In LoopCommon::Loop_GPIBSendCmd: LoopType= $str_looptype; Loop= $str_looplen; BT= $str_btlen"
                append strmsg "GPIB411A= $str_gpib411A; GPIB412A=$str_gpib412A"
                Log "info" $logfile [list $strmsg]
                
                set list_410A_ANSI_CSA_LenIdx [Loop410A::ANSI_CSALength $str_looptype \
                        $list_410ALoopLen]
                
                #{77 3} {204 7}
                #first element is T1.413; second one is G996.1
                Loop410A::ANSI_CSAGPIBCmdSet [lindex $list_410A_ANSI_CSA_LenIdx 0] \
                        $list_410ACmdSet
            }
            ;#if {[llength $list_410ALoopBTLenIdx] > 0}
            
            ###################################################
            #410A GPIB command setting
            ###[list $Func_INI::str_LoopSimulator $Loop410A::GPIB411A \
            ###$Loop410A::GPIB412A $Loop414E::GPIB414E]
            ##################################################
            set list_dev_gpibaddr [list $str_gpib411A $str_gpib412A]
            # insertLogLine "warn" "410AGPIB, list_dev_gpibaddr=$list_dev_gpibaddr"
            Loop410A::_SendCmd $list_dev_gpibaddr $Loop410A::list_411ACmdSet \
                    $Loop410A::list_412ACmdSet $logfile
        }
        
        {^A_DLS400IAE} {
            set len_str_looptype [string length $str_looptype]
            set len_str_looplen  [string length $str_looplen]
            set len_str_btlen    [string length $str_btlen]
            
            #################################
            #Check Loop Type CSA or ANSI
            ################################
            if {$len_str_looptype > 0 } {
                switch -regexp [string tolower $str_looptype] {
                    {^csa\ 4} {
                        Loop400IAE::CSA_4GPIBCmdSet $str_looptype
                    }
                    {^ansi\ 13} {
                        Loop400IAE::ANSI_13GPIBCmdSet $str_looptype
                    }
                };#switch -regexp [string tolower $str_looptype]
            ################################
            #Check if BT loop
            ################################
            } elseif {($len_str_looplen > 0) && ($len_str_btlen > 0)} {
                
                Loop400IAE::LoopBTGPIBCmdSet $str_looplen $str_btlen
            
            ################################
            #Check if Straight loop
            ################################
            } elseif {$len_str_looplen > 0}  {
                Loop400IAE::IAE1 $str_looplen
                Loop400IAE::IAE2 $str_looplen
            };#if {($len_str_looptype > 0) && ($len_str_looplen < 0) && ($len_str_btlen < 0)}
            
            set list_dev_gpibaddr [list $str_gpib400IAE1 $str_gpib400IAE2]
            Loop400IAE::_SendCmd $list_dev_gpibaddr $Loop400IAE::list_400IAE1CmdSet \
                    $Loop400IAE::list_400IAE2CmdSet $logfile
        }
    };#switch -regexp $str_loopsimulator
    return true
}
################################################################################
# namespace eval Loop410A
################################################################################
namespace eval Loop410A {
    variable list_411ACmdSet {}
    variable list_412ACmdSet {}
    variable verbose        off
    variable logfile        "../log/loop410A.log"
    variable GPIB411A "16"
    variable GPIB412A "17"
}
proc Loop410A::unit_mtoft {m} {
    set ft [expr $m * 3.2808]
    return [expr round($ft)]
}
proc Loop410A::GetLoopLenIdx {looplen list_dls410ALoopLen} {
    variable verbose
    variable logfile
    set  list_410Alooplenidx {}
    
    #For WT100 100,400kft
    if {$looplen < 500} { set looplen 500
    }
    foreach rowval $list_dls410ALoopLen {
        #################################
        #0 1 {Straight Loop 0.5Kft} 500
        #################################
        set looplen_dls410A [lindex $rowval 3]
        
       # if {$verbose == on} {
            # set strmsg "looplen_dls410A=$looplen_dls410A"
            # Func_INI::PntMsg "looplen_dls410A=$looplen_dls410A"
            # Log "info" $logfile [list $strmsg]
        # };#if {$verbose == on}
        
        if {$looplen_dls410A == $looplen} {
            if [info exist str_retval] { unset str_retval }
            append str_retval [lindex $rowval 0] ";" [lindex $rowval 1]
            
            lappend list_410Alooplenidx $str_retval
        }
        
    };#foreach rowval $list_dls410ALoopLen
    
    if {$verbose == on} {
        set strmsg "In Loop410A::GetLoopLenIdx str_retval=$str_retval"
        #Func_INI::PntMsg $strmsg
        Log "info" $logfile [list $strmsg]
    };#if {$verbose == on}
    
    return $list_410Alooplenidx
}

proc Loop410A::GetLoopBTLenIdx {looplen btlen list_dls410ALoopLen} {
    variable verbose
    variable logfile
    set list_410Aloopbtlenidx {}
    if {$btlen == 0} {
        set btlen 50
    }
    foreach rowval $list_dls410ALoopLen {
        ##########################################
        # 56,2,Bridged Tap 9.0Kft with 50ft,9000,50
        # 57,2,Bridged Tap 9.0Kft with 150ft,9000,150
        ##########################################
        set looplen_dls410A [lindex $rowval 3]
        set btlen_dls410A [lindex $rowval 4]
        
        if {($looplen_dls410A == $looplen) && \
                    ($btlen_dls410A == $btlen)} {
            if [info exist str_retval] {unset str_retval}
            append str_retval [lindex $rowval 0] ";" [lindex $rowval 1]
            
            lappend list_410Aloopbtlenidx $str_retval
        } else  {
            set str_retval "Loop Type is not Straight Loop and Bridged Tap."
        }
        ;#if {($looplen_dls410A == $looplen) && ($btlen_dls410A == $btlen)}
        
    };#foreach rowval $list_dls410ALoopLen
    
    if {$verbose == on} {
        Func_INI::PntMsg "In Loop410A::GetLoopBTLenIdx \
                str_retval=$str_retval"
    };#if {$verbose == on}
    
    return $list_410Aloopbtlenidx
}
proc Loop410A::GetANSI_CSA_LenIdx {looptype list_dls410ALoopLen} {
    variable verbose
    variable logfile
    set list_ansi_csaidx {}
    
    foreach rowvalue $list_dls410ALoopLen {
        #############################################
        #77,3,ANSI 13 (Standard),9000,2000,1500,500,1500,500
        #80,4,CSA 4 (Standard),550,400,6250,800,800
        #############################################
        set looptypedls410A [lindex $rowvalue 2]
        
        if {$looptype == $looptypedls410A} {
            lappend list_ansi_csaidx [lrange $rowvalue 0 1]
            # if [info exist str_retval] {unset str_retval}
            # append str_retval [lindex $rowvalue 0] ";" [lindex $rowvalue 1]
        }
        
    };#foreach rowvalue $list_dls410ALoopLen
    
    if {$verbose == on} {
        set strmsg "looptype=$looptype; looptypedls410A=$looptypedls410A; \
                list_ansi_csaidx=$list_ansi_csaidx"
        Log "info" $logfile [list $strmsg]
    }
    
    return $list_ansi_csaidx
}
proc Loop410A::_NoiseSourceON {list_dev_gpibaddr} {
    TC_GPIB::clear_existed_device
    ################################
    #Open GPIB setting
    ################################
    set list_opendevice [TC_GPIB::opendevice $list_dev_gpibaddr]
    TC_GPIB::write_listcmd [lindex $list_opendevice 0] \
            [list ":Setting:Channel:State NORMAL" ":SourceA:Noise ON"]
    TC_GPIB::write_listcmd [lindex $list_opendevice 1] \
            [list ":Setting:Channel:State NORMAL" ":SourceB:Noise ON"]
    
    ################################
    #Close GPIB setting
    ################################
    TC_GPIB::close $list_opendevice
    
    return true
}
#############################
#Sep16-2006 411A: ":Setting:Channel:State NORMAL" ":SourceA:Noise ON"
#           412A: ":Setting:Channel:State NORMAL" ":SourceB:Noise ON"
#############################
proc Loop410A::_SendCmd {list_dev_gpibaddr {list_411aGPIBcmdset {}} {list_412aGPIBcmdset {}} {logfile ""}} {
    TC_GPIB::clear_existed_device
    ################################
    #Open GPIB setting
    ################################
    set list_opendevice [TC_GPIB::opendevice $list_dev_gpibaddr]
    # TC_GPIB::write_listcmd [lindex $list_opendevice 0] \
            # [list ":Setting:Channel:State NORMAL" ":SourceA:Noise ON"]
   # TC_GPIB::write_listcmd [lindex $list_opendevice 1] \
            # [list ":Setting:Channel:State NORMAL" ":SourceB:Noise ON"]
    ###411A
    if {[llength $list_411aGPIBcmdset] > 0} {
        TC_GPIB::write_listcmd [lindex $list_opendevice 0] $list_411aGPIBcmdset
    }
    ###412A
    if {[llength $list_412aGPIBcmdset] > 0} {
        TC_GPIB::write_listcmd [lindex $list_opendevice 1] $list_412aGPIBcmdset
    }
    
    ################################
    #Close GPIB setting
    ################################
    TC_GPIB::close $list_opendevice
    
    return true
}

################################################################################
# Get DLS410A LoopBT Length Index
# Sep16-2006 Straigh loop BT length == ""
# Sep18-2006 CSA#4 and ANSI#14 looplen=="" btlen==""
# Sep28-2006 WT100 AnnexA ETSI Loop#1 issue
# Nov05-2006 Modify StraightLoopIdx should from zero
################################################################################
proc Loop410A::LoopBTLength {looplen btlen list_dls410ALoopLen} {
    variable verbose
    variable logfile
    set list_410Aloop_idx {}
    set list_410Aloopbt_idx {}
    set list_410Aloopbtlen_idx {}
    
    set list_StraightLoop [list 500 1000 1500 2000 2500 3000 3500 4000 4500 5000 5500 \
            6000 6500 7000 7500 8000 8500 9000 9500 10000 10500 \
            11000 11500 12000 12500 13000 13500 14000 14500 15000 15500 \
            16000 16500 17000 17500 18000 18500 19000 19500 20000 20500 \
            21000]
    ####################################################################
    #;ID,table,testprofile,looptype,looplength,taplength,noise,noise_Ch1,noise_Ch2
    # 1039,White Noise impairment-Interleave Mode,A2P_Interleave,,18000,,,White_Noise_xtk.enc,White_Noise_xtk.enc,,,,,,
    # 1040,Bridged Tap  with 9 KFT Loop,A2P_Fast,,9000,0,,White_Noise_xtk.enc,White_Noise_xtk.enc,,,,,,
    # 1107,CSA#4 STANDARD LOOP with WHITE NOISE,A2P_Fast,CSA 4 (Standard),,,DLS5204,White_Noise_xtk.enc,White_Noise_xtk.enc,,,,,,
    # 1111,ANSI#13 STANDARD LOOP with WHITE NOISE,A2P_Fast,ANSI 13 (Standard),,,DLS5204,White_Noise_xtk.enc,White_Noise_xtk.enc,,,,,,
    ######################################################################
    if {($looplen != "") && ($btlen == "") } {
        ###########################################
        # #list index start from zero should - 1
        ###########################################
        set StraightLoopIdx [expr [expr $looplen / 500] - 1]
        
        if {$StraightLoopIdx > 41} {
            set retmsg "Input Loop Length is over 21000 ft."
            if {$verbose == on} {
                Log "info" $logfile $retmsg
            }
            return $retmsg
        };#if {$StraightLoopIdx > 41}
        
        if {$StraightLoopIdx < 0} {
            set StraightLoopIdx 0
        }
        set StraightLoopLen [lindex $list_StraightLoop $StraightLoopIdx]
        
        set list_410Aloop_idx [GetLoopLenIdx $StraightLoopLen $list_dls410ALoopLen]
        set list_410Aloopbtlen_idx $list_410Aloop_idx
    } elseif {($looplen != "") && ($btlen != "")} {
        set list_410Aloopbt_idx [GetLoopBTLenIdx $looplen $btlen $list_dls410ALoopLen]
        set list_410Aloopbtlen_idx $list_410Aloopbt_idx
    }
    ;#if {$btlen == "0"}
    
    if {$verbose == on} {
        lappend listlog "list_410Aloop_idx= $list_410Aloop_idx"
        lappend listlog "list_410Aloopbt_idx= $list_410Aloopbt_idx"
        lappend listlog "list_410Aloopbtlen_idx= $list_410Aloopbtlen_idx"
        Log "info" $logfile $listlog
    }
    return $list_410Aloopbtlen_idx
}
proc Loop410A::ANSI_CSALength {looptype list_dls410ALoopLen} {
    variable verbose
    variable logfile
    set list_410_ANSI_CSA_idx {}
    
    set list_410_ANSI_CSA_idx [GetANSI_CSA_LenIdx $looptype $list_dls410ALoopLen]
    
    if {$verbose == on} {
            lappend listlog "LoopType= $looptype"
            lappend listlog "list_410_ANSI_CSA_idx= $list_410_ANSI_CSA_idx"
            Log "info" $logfile $listlog
   }
    return $list_410_ANSI_CSA_idx
}
proc Loop410A::Log {level filename list_msg} {
    foreach temp $list_msg {
        set msg "$temp"
        Log::tofile $level $filename $msg
    }
}
################################################################################
# 35,1,-,-,C7F41FC2FC7F41F42F01FC4641F01F81FC5F01FC1FC5F01FC1FC5F,
# 83F81FC2F03F81F42F82F03F82F82FC2F000C2F41F000C2F41F800,
# 03F81FC2F03F81F42F82F03F82F82FC2F000C2F41F000C2F41F000
#
# 35,2,-,-,C5F02F81F00001F01F01FC7F41F41F01F00001FC1FC7F01F01F000,
# 80081F81F00001F01F02F03F81FC2F01F000C2F41F03F02F01F800,
# 00081F81F00001F01F02F03F81FC2F01F000C2F41F03F02F01F000
################################################################################
################################################################################
# :setM1 C7F41FC2FC7F41F42F01FC4641F01F81FC5F01FC1FC5F01FC1FC5F,
# :setM2 83F81FC2F03F81F42F82F03F82F82FC2F000C2F41F000C2F41F800,
# :setM3 03F81FC2F03F81F42F82F03F82F82FC2F000C2F41F000C2F41F000
#
# :setM1 C5F02F81F00001F01F01FC7F41F41F01F00001FC1FC7F01F01F000,
# :setM2 80081F81F00001F01F02F03F81FC2F01F000C2F41F03F02F01F800,
# :setM3  00081F81F00001F01F02F03F81FC2F01F000C2F41F03F02F01F000
################################################################################
################################################################################
# loopid, chassis
# 36,2,Bridged Tap 3.0Kft with 50ft,3000,50
# 37,2,Bridged Tap 3.0Kft with 100ft,3000,100
# 77,3,ANSI 13 (Standard),9000,2000,1500,500,1500,500
# 78,3,ANSI 13 (Modified-1),1000,2000,1500,500,1500,500
# 79,3,ANSI 13 (Modified-2),2000,2000,1500,500,1500,500
# 80,4,CSA 4 (Standard),550,400,6250,800,800
################################################################################
proc Loop410A::GPIBCmdSet {loopid chassis list_dls410ACmd} {
    variable list_411ACmdSet
    variable list_412ACmdSet
    variable verbose
    variable logfile
    
    #BT, ANSI and CSA case
    if {$chassis > 1} {
        set chassis 1
    }
    
    foreach rowval $list_dls410ACmd {
        set loopidx [lindex $rowval 0]
        set chassisidx_411A [lindex $rowval 1]
        set chassisidx_412A [lindex $rowval 1]
        
        if {($loopidx == $loopid) && ($chassisidx_411A == $chassis)} {
            if [info exist list_411ACmdSet] {unset list_411ACmdSet}
            lappend list_411ACmdSet [append temp4_411a ":setM1 " [lindex $rowval 4]]
            lappend list_411ACmdSet [append temp5_411a ":setM2 " [lindex $rowval 5]]
            lappend list_411ACmdSet [append temp6_411a ":setM3 " [lindex $rowval 6]]
            
            if {$verbose == on} {
                Loop414E::Log "info" $logfile [list "list_411ACmdSet=" $list_411ACmdSet]
            };#if {$verbose == on}
            
        };#411A GPIB Cmd Set
        
        if {($loopidx == $loopid) && ($chassisidx_412A == [expr $chassis + 1])} {
            if [info exist list_412ACmdSet] {unset list_412ACmdSet}
            lappend list_412ACmdSet [append temp4_412a ":setM1 " [lindex $rowval 4]]
            lappend list_412ACmdSet [append temp5_412a ":setM2 " [lindex $rowval 5]]
            lappend list_412ACmdSet [append temp6_412a ":setM3 " [lindex $rowval 6]]
            
            if {$verbose == on} {
                Loop414E::Log "info" $logfile [list "list_412ACmdSet=" $list_412ACmdSet]
            };#if {$verbose == on}
            
        };#412A GPIB Cmd Set
    };#foreach rowval $list_dls410ACmd
    
    return true
}

# ################################################################################
# Get DLS410A LoopBT command set
################################################################################
proc Loop410A::LoopBTGPIBCmdSet {str_loopbtlenidx list_dls410ACmd} {
    variable verbose
    variable logfile
    
    set list_loopbtidx [split $str_loopbtlenidx ";"]
    set loopid [lindex $list_loopbtidx 0]
    set chassis [lindex $list_loopbtidx 1]
    
    if {$verbose == on} {
        Loop414E::Log "info" $logfile [list "list-loopbtlenidx= $list_loopbtidx" \
                "loopid=$loopid" "chassis=$chassis"]
    };#if {$verbose == on}
    
    GPIBCmdSet $loopid $chassis $list_dls410ACmd
    
    return true
}
proc Loop410A::ANSI_CSAGPIBCmdSet {list_ansicsilenidx list_dls410ACmd} {
    variable verbose
    variable logfile
    
    #ex. list_ansicsilenidx = {77 3}
    set loopid [lindex $list_ansicsilenidx 0]
    set chassis [lindex $list_ansicsilenidx 1]
    
    # if {$verbose == on} {
        # Loop414E::Log "info" $logfile [list \
                # "list-ansi_csi_lenidx= $list_ansicsilenidx loopid=$loopid chassis=$chassis"]
    # }
    
    GPIBCmdSet $loopid $chassis $list_dls410ACmd
    
    return true
}
################################################################################
# namespace eval Loop414E
################################################################################
namespace eval Loop414E {
    variable N_fine ""
    variable N_coarse ""
    variable strdls414Cmd ""
    variable verbose        off
    variable logfile        "../log/loop414E.log"
    variable GPIB414E   18
}
proc Loop414E::unit_fttom {ft} {
    set m [expr $ft * 0.3048]
    return [expr round($m)]
}
proc Loop414E::LoopLength {m list_dls414ELenCoeff} {
    #if [$m == "0"] { return false }
    variable N_fine
    variable N_coarse
    variable verbose
    variable logfile
    
    set index [expr [expr $m - 50] / 25]
    # Check index boundary
    if {$index < 0} { set index 0 }
    if {$index > [llength $list_dls414ELenCoeff]} {
        return false
    }
    set temp [lindex $list_dls414ELenCoeff $index]
    set N_fine [lindex $temp 1]
    set N_coarse [lindex $temp 2]
    
    if {$verbose == on} {
        lappend listlog "index=$index; distance_m=[lindex $temp 0]"
        Func_INI::PntMsg "index=$index; distance_m=[lindex $temp 0]"
        lappend listlog "N_fine=$N_fine; N_coarse=$N_coarse"
        Log "info" $logfile $listlog
    }
    
    return true
}
proc Loop414E::Log {level filename list_msg} {
    foreach temp $list_msg {
        set msg "$temp"
        Log::tofile $level $filename $msg
    }
}
proc Loop414E::_SendCmd_NoiseOff {list_dev_gpibaddr strGPIBCmd {logfile ""}} {
    TC_GPIB::clear_existed_device
    ################################
    #Open GPIB setting
    ################################
    set list_opendevice [TC_GPIB::opendevice $list_dev_gpibaddr]
    TC_GPIB::write_listcmd [lindex $list_opendevice 0] \
            [list ":SET:CHAN:STATE Normal" ":SourceA :Noise OFF" ":SourceB :Noise OFF"]
    set list_gpib_errmsg [TC_GPIB::DispGPIBMsg $list_opendevice]
    
    TC_GPIB::write [lindex $list_opendevice 0] $strGPIBCmd
    set list_gpib_errmsg [TC_GPIB::DispGPIBMsg $list_opendevice]
    
    if {$logfile != ""} {
        Log "info" $logfile $list_gpib_errmsg
    }
    ################################
    #Close GPIB setting
    ################################
    TC_GPIB::close $list_opendevice
    
    return true
}
proc Loop414E::_NoiseSourceON {list_dev_gpibaddr} {
    TC_GPIB::clear_existed_device
    ################################
    #Open GPIB setting
    ################################
    set list_opendevice [TC_GPIB::opendevice $list_dev_gpibaddr]
    TC_GPIB::write_listcmd [lindex $list_opendevice 0] \
            [list ":SET:CHAN:STATE Normal" ":SourceA :Noise ON" ":SourceB :Noise ON "]
    
    ################################
    #Close GPIB setting
    ################################
    TC_GPIB::close $list_opendevice
    
    return true
}
proc Loop414E::_SendCmd {list_dev_gpibaddr strGPIBCmd {logfile ""}} {
    TC_GPIB::clear_existed_device
    set list_gpib_errmsg {}
    ################################
    #Open GPIB setting
    ################################
    set list_opendevice [TC_GPIB::opendevice $list_dev_gpibaddr]
    # TC_GPIB::write_listcmd [lindex $list_opendevice 0] \
            # [list ":SET:CHAN:STATE Normal" ":SourceA :Noise ON" ":SourceB :Noise ON "]

    set list_gpib_errmsg [TC_GPIB::DispGPIBMsg $list_opendevice]
    
    TC_GPIB::write [lindex $list_opendevice 0] $strGPIBCmd
    lappend list_gpib_errmsg [TC_GPIB::DispGPIBMsg $list_opendevice]
    
    if {$logfile != ""} {
        Log "info" $logfile $list_gpib_errmsg
    }
    ################################
    #Close GPIB setting
    ################################
    TC_GPIB::close $list_opendevice
    
    return true
}

################################################################################
# From Zyxel's VB program
# '�v�r�ˬd
# 'GpibCmd_str(0) = GPIB_CMD1  (:SET:CHAN:LINE)
# 'GpibCmd_str(1) = Start      (17000)
# 'GpibCmd_str(2) = End        (18000)
# 'GpibCmd_str(3) = Step       (500)
# 'GpibCmd_str(4) = GPIB_CMD2  (FT)
# 
# Case "3900"
#     StrTmp1 = "190"
#     StrTmp2 = "114"
# Case "4000"
#     StrTmp1 = "210"
#     StrTmp2 = "117"
# Case Else
#     MsgBox (i1 & "is not default")
# End Select
# StrTmp = GpibCmd_str(0) & " " & StrTmp1 & "," & StrTmp2
# ComPort.Display = StrTmp + Enter
# 
# 'Set DSL414E Loop Simulator
# Call Write_GPIB(GPIB_Device(Val(Cmd_arr(1))), StrTmp)
# Call Write_GPIB(GPIB_Device(Val(Cmd_arr(1))), "*ESR?")
# Call Read_GPIB(GPIB_Device(Val(Cmd_arr(1))))
################################################################################
proc Loop414E::GPIBCmd {} {
    variable N_fine
    variable N_coarse
    variable strdls414Cmd
    
    set prefix_GPIBCmdStr ":SET:CHAN:LINE"
    if [info exists strdls414Cmd] {
        unset strdls414Cmd
    }
    append strdls414Cmd $prefix_GPIBCmdStr " " $N_fine "," $N_coarse
    # set strdls414Cmd $strtemp; # puts "GPIBCmd=$strdls414Cmd"
    return true
}