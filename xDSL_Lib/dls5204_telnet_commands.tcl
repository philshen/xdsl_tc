package provide dls 2.2.1

#
# Copyright 2002 Spirent Communications
# 

## ]-- Start Global Variables

# Socket descriptor
set _sock_desc ""

# Server Response (Must be global for vwait)
set line ""

# time out
set _timeout 60000
# for debugging
set _verbose off



## ]-- End Global Variables


# ]-- Private Functions --[ #



# Procedure : write_socket
# Purpose   : Sends 'command' to the 5204
# Exits if a connection is not established

proc write_socket { command } {
   global _sock_desc _verbose
   set snd "Sending: "
  
   if {[eof $_sock_desc]} {
      puts "Error: A connection has not been established!!"
   }
   if { $_verbose == "on" } {
    puts "$snd $command"
   }
   puts $_sock_desc $command
}



# Procedure : read_socket
# Purpose   : Reads response from 5204
# Sets 'line' to reponse if successful, otherwise 
# sets 'line' to "Timeout" if a timeout occured while
# waiting for a response.
# Returns a parsed 5204 response or 'timed out' on failure.

proc read_socket { } {

  #global sock descriptor and line
  global _sock_desc line _verbose _timeout
   
  # set the timer
  set time_id [after $_timeout {set line -1}]

  # when socket becomes readable
  set line ""
  fileevent $_sock_desc readable {
    # Disable fileevent until set again
    fileevent $_sock_desc readable ""
    # poll until read line or timed out
   
    while { $line == "" } {
      set line [read $_sock_desc]
      update
    } 
    
    
  }

  # wait for variable line to change
  vwait ::line
  
  if { $line == -1 } {
      # Timeout Occured
      set line "Timeout"
      set result "Connection timed out."
  } else {
      # recieved message. disable timer
      after cancel $time_id
      if { $_verbose == "on" } {
        puts "Recieved: $line"
      }
      set result [parse_line]
  }
  return $result
}


# Procedure : IsConnected
# Purpose   : checks if already connected
# Returns   : 1 connected - 0 not connected

proc IsConnected { } {
  global _sock_desc
  
  if { $_sock_desc == "" } {
    return 0
  }
  return 1

}

# Procedure : parse_line
# Purpose   : Parse the response form the 5204

proc parse_line { } {
  global line
  
  if {[regexp {^!STX:([A-Z]+)\(([A-Z_]+)\):VAL\(([A-Z0-9_]+)\)} $line -> command type response_code] == 1} {
    # found a match
    switch -- $command {
    
      "SET" {               
        if { $type == "REPORT" } {
          set result [handle_report $response_code]
        } else {
          perror "parse_line" "Cannot parse $type response"
        } 
      }
    
      "TRAP" {
        if { $type == "ERROR" } {
          set result [handle_error $response_code]
        } else {
          perror "parse_line" "Cannot parse $type response"
        }       
      }
        
      "REPLY" {

     
        if { $type == "M_SELECTED_OUTPUT" } {
          set result [handle_reply $response_code]
        } else {
          perror "parse_line" "Cannot parse $type response"
        }
     #   set result [handle_reply $response_code]
      }
      
      default {
         return "parse_line: Unknown case."
      }
    }
    
    return $result
  } else {
    # Don't know what we have. This SHOULD never happen.
    perror "ERROR" "Cannot parse response."
    
  } 
}


# Procedure : handle_report
# Purpose   : Display errors regarding REPORT messages
# Returns   : Translation of error code

proc handle_report { report_type } {
 
  switch $report_type {
 
    "CMD_ACCEPTED" {
      return "Successful."
    }
   
    "LEVEL_TOO_HIGH" {
      return "Power level of combined crosstalk exceeds maximum output power level."
    }
    
    "PACKAGE_MANAGER_ERROR" {
      return "Package manager error."
    }
    
    "CREST_FACTOR_UNATTAINED" {
      return "The resulting crest factor is less than 5" 
    }
    
    "SET_XTALK_GAIN_BLOCKED" {
      return "Attempted to set crosstalk gain while crosstalk is muted. OR The resulting crest factor is less than 5."
    }
    
    "SET_RFI_GAIN_BLOCKED" {
      return "Attempted to set RFI gain while crosstalk is muted. OR RFI has not been licensed."
    }
   
    "RFI_INVALID_LICENSE" {
      return "Invalid RFI license."
    }
    
    "LICENSE_SERVER_ERROR" {
      return "Invalid RFI license."
    }
    
    "LICENSE_SERVER_LOST" {
      return "Connection to license server was lost."
    }
    
    "FILE_ACCESS_ERROR" {
      return "The file does not exist"
    }
   
    default {
      return "Unknown error type: $report_type"
    }
  }
}


# Procedure : handle_error
# Purpose   : Display errors regarding ERROR messages
# Returns   : Translation of error code

proc handle_error { error_type } {

  switch $error_type {
  
    "FAILURE" {
      return "Unspecified error."
     }
     
     "BAD_PARAMETER_ID" {
       return "Invalid task or parameter identifer in message. OR. A command is previous to this is required."
     }
     
     "BUSY" {
       return "Busy operating on a pervious command."
     }
     
     "CMD_OVERFLOW" {
       return "Remote control access is disabled. Cannot process command."
     }
   
    default {
      return "Unknown error type: $error_type"
    }
  }

}


# Procedure : handle_reply
# Purpose   : Converts relpy code to channel number
# Returns   : Translation of reply code "


proc handle_reply { reply_type } {
  
  switch $reply_type {
     
    "OUTPUT_1" {
      return "Channel_1"
    }
   
    "OUTPUT_2" {
      return "Channel_2"
    }
   
    "OUTPUT_3" {
      return "Channel_3"
    }
   
    "OUTPUT_4" {
      return "Channel_4"
    }
   
    default {
      return "Unknown error type: $error_type"
    }
  }
}



# Procedure : perror
# Purpose   : Report Errors to console

proc perror { command errormsg } {

  # Display Error messages 
  error "$command: $errormsg"
}



# ]-- Public Functions --[ #

# Procedure : Dls_5204_Connect
# Purpose   : Establish a connection to a remote DLS 5204
# Exits on failure. 

proc Dls_5204_Connect { ip port } {
  global _sock_desc
  # Check for valid args
  if { $ip != "" && $port != "" } {
   
    # check if already connected
    if { [IsConnected] } {
      perror "Dls_5204_Connect" "Already Connected"
    }
   
    set _sock_desc [socket $ip $port]
    if {[eof $_sock_desc]} {
      perror "Dls_5204_Connect:" "Cannot establish connection to $ip on port $port."
    } else {
      fconfigure $_sock_desc -blocking 0 -buffering line -translation auto
      puts "Dls_5204_Connect: Sucessful."
      return $_sock_desc      
    }
  }
  perror "Dls_5204_Connect" "Invalid amount of parameters."
}




# Procedure : Dls_5204_SetGain
# Purpose   : Sets the gain on the 5204

proc Dls_5204_SetGain { gain_type gain_value } {

  # check if  connected
  if { ![IsConnected] } {
    perror "Dls_5204_SetGain" "Not connected to unit."
  }
    
  # check for non null parameters
  if { $gain_type == "" || $gain_value == "" } {
    perror "Dls_5204_SetGain" "Invalid parameters."
  }
  
  switch [string toupper $gain_type] {
  
    "XTALK" {
      write_socket "!STX:SET(M_XTALK_GAIN):VAL($gain_value);ETX!"
    }
    
    "RFI" {
      write_socket "!STX:SET(M_RFI_GAIN):VAL($gain_value);ETX!"
    }
    
    default {
      perror "Dls_5204_SetGain" "Invalid parameters."
    }
  }
  set result [read_socket]
  
  if  { $result == "Successful." } {
    puts "Dls_5204_SetGain: Command successful. $gain_type set to $gain_value dBm."
  } else {
    perror "Dls_5204_SetGain" $result
  }
  
}


# Procedure : Dls_5204_GetSelectedChannel
# Purpose   : Determine what output is selected
# Returns   : Channel_1 ... Channel_4 depending on the current selcted output

proc Dls_5204_GetSelectedChannel { } {

  # check if  connected
  if { ![IsConnected] } {
    perror "Dls_5204_GetSelectedChannel" "Not connected to unit."
  }

  write_socket "!STX:GET(M_SELECTED_OUTPUT);ETX!"
  
  set result [read_socket]
  
  #get result returns 2 responses.. get the second one.
  set result2 [read_socket]

  # check for a valid result
  if  { $result == "Channel_1" || $result == "Channel_2" || $result == "Channel_3" || $result == "Channel_4" } {
    puts "Dls_5204_GetSelectedOutput: Current selected output is $result."
    return $result
  } else {
    puts "--> $result is result"
    perror "Dls_5204_GetSeelectedOutput" "$result"
  } 
}

# Procedure : Dls_5204_SetSelectedChannel
# Purpose   : Select the current channel to operate on.

proc Dls_5204_SetSelectedChannel { channel } {

  # check if  connected
  if { ![IsConnected] } {
    perror "Dls_5204_SetSelectedChannel" "Not connected to unit."
  }

  # check for valid channel
  set tmp_channel [string toupper $channel]
  switch $tmp_channel {
  
    "CHANNEL_1" {
      write_socket "!STX:SET(M_SELECT_OUTPUT):VAL(OUTPUT_1);ETX!"
      set channel 1
    }
    
    "CHANNEL_2" {
      write_socket "!STX:SET(M_SELECT_OUTPUT):VAL(OUTPUT_2);ETX!"
      set channel 2
    }
    
    "CHANNEL_3" {
      write_socket "!STX:SET(M_SELECT_OUTPUT):VAL(OUTPUT_3);ETX!"
      set channel 3
    }
    
    "CHANNEL_4" {
      write_socket "!STX:SET(M_SELECT_OUTPUT):VAL(OUTPUT_4);ETX!"
      set channel 4
    }
    
    default {
      perror "Dls_5204_SetSelectedOutput" "$channel is an invalid channel"
    }
  
  }
  
  # get reply
  set result [read_socket]
  
  # check result
  if  { $result == "Successful." } {
    puts "Dls_5204_SetSelectedOutput: Command successful. Now using channel #$channel."
  } else {
    perror "Dls_5204_SetSelectedOutput" $result
  }
}


# Procedure : Dls_5204_EnableOutput
# Purpose   : Select the current channel to operate on.

proc Dls_5204_EnableOutput { channel channel_status } {

  # check if  connected
  if { ![IsConnected] } {
    perror "Dls_5204_EnableOutput" "Not connected to unit."
  }

  # check for valid channel
  set tmp_channel [string toupper $channel]
  switch $tmp_channel {
  
    "CHANNEL_1" {
      set channel "OUTPUT_1"
      set chan_num 1
    }
    
    "CHANNEL_2" {
      set channel "OUTPUT_2"
      set chan_num 2
    }
    
    "CHANNEL_3" {
      set channel "OUTPUT_3"
      set chan_num 3
    }
    
    "CHANNEL_4" {
      set channel "OUTPUT_4"
      set chan_num 4
    }
    
    default {
      perror "Dls_5204_EnableOutput" "$channel is an invalid channel"
    }
  
  }
    
  # Check for a valid status
  set tmp_chan_status [string toupper $channel_status]
  if { $tmp_chan_status != "ON" && $tmp_chan_status != "OFF" } {
    perror "Dls_5204_EnableOutput" "Invalid status. Either \"ON\" or \"OFF\"."
  }
  
  # send command
  write_socket "!STX:SET(M_ENABLE_OUTPUT):VAL($channel:$tmp_chan_status);ETX!"
  
  # get reply
  set result [read_socket]
  
  # check result
  if  { $result == "Successful." } {
    puts "Dls_5204_EnableOutput: Command successful. Channel $chan_num is now $tmp_chan_status."
  } else {
    perror "Dls_5204_EnableOutput" $result
  }
}


# Procedure : Dls_5204_GenerateSample
# Purpose   : Select the current channel to operate on.

proc Dls_5204_GenerateSample { } {

  # check if  connected
  if { ![IsConnected] } {
    perror "Dls_5204_GenerateSample" "Not connected to unit."
  }
  
  write_socket "!STX:SET(M_GENERATE_SAMPLE);ETX!"
 
  # get reply
  set result [read_socket]
  
  # check result
  if  { $result == "Successful." } {
    puts "Dls_5204_GenerateSample: Command successful."
  } else {
    perror "Dls_5204_GenerateSample" $result
  }
}


# Procedure : Dls_5204_LoadNoiseFile
# Purpose   : Selects a noise file to load.

proc Dls_5204_LoadNoiseFile { noise_type file_name } {

  # check if  connected
  if { ![IsConnected] } {
    perror "Dls_5204_LoadNoiseFile" "Not connected to unit."
  }
  
  # check file type
  
  set n_type [string toupper $noise_type]

  switch $n_type {
  
    "XTALK" {
      set n_type "M_LOAD_XTALK_FILE"
    }
    
    "RFI" {
      set n_type "M_LOAD_RFI_FILE"
    }
    
    default {
      perror "Dls_5204_LoadNoiseFile" "Invalid noise type \"XTALK\" or \"RFI\"."
    }
  }
    
  # check for non null file name
  
  if { $file_name == "" } {
    perror "Dls_5204_LoadNoiseFile" "Invalid noise file."
  }
  
  # send command
  write_socket "!STX:SET($n_type):VAL($file_name);ETX!"
  
  # get reply
  set result [read_socket]
  
  if  { $result == "Successful." } {
    puts "Dls_5204_LoadNoiseFile: Command successful."
  } else {
    perror "Dls_5204_LoadNoiseFile" $result
  }
}


# Procedure : Dls_5204_Disconnect
# Purpose   : Disconnects the current connection.

proc Dls_5204_Disconnect { } {

  global _sock_desc

  # check if  connected
  if { ![IsConnected] } {
    perror "Dls_5204_Disconnect" "Not connected to unit."
  }
   
  # close socket
  close $_sock_desc
  set _sock_desc ""

  puts "Dls_5204_Disconnect: Command successful."
}



# Procedure : Dls_5204_LoadOutput
# Purpose   : Loads the interpolated noise to the tality card ram.
# Precond   : Channel *MUST* be disabled prior to loading

proc Dls_5204_LoadOutput { } {

  # check if  connected
  if { ![IsConnected] } {
    perror "Dls_5204_LoadOutput" "Not connected to unit."
  }
 
  write_socket "!STX:SET(M_LOAD_OUTPUT);ETX!"

  # get reply
  set result [read_socket]
  
  if  { $result == "Successful." } {
    puts "Dls_5204_LoadOutput: Command successful."
  } else {
    perror "Dls_5204_LoadOutput" $result
  }
  
}



# Procedure : Dls_5204_SetCrestFactor
# Purpose   : Sets or unsets the Crest factor


proc Dls_5204_SetCrestFactor { status } {

  # check if  connected
  if { ![IsConnected] } {
    perror "Dls_5204_SetCrestFactor" "Not connected to unit."
  }

  set status [string toupper $status]

  if { $status != "ON"  && $status != "OFF" } {
    perror "Dls_5204_SetCrestFactor" "Invalid status. Either \"ON\" or \"OFF\"."
  }

  write_socket "!STX:SET(M_CREST_FACTOR):VAL($status);ETX!"

  # get reply
  set result [read_socket]
  
  if  { $result == "Successful." } {
    puts "Dls_5204_SetCrestFactor: Command successful. Crest factor >= 5 is : $status."
  } else {
    perror "Dls_5204_SetCrestFactor" $result
  }
  
}



# Procedure : Dls_5204_SetNumberOfSamples
# Purpose   : Sets or unsets the Crest factor

proc Dls_5204_SetNumberOfSamples { samples } {

  # check if  connected
  if { ![IsConnected] } {
    perror "Dls_5204_SetNumberOfSamples" "Not connected to unit."
  }

  # check for non null
  if { $samples == "" } {
    perror "Dls_5204_SetNumberOfSamples" "Invalid parameter." 
  }    
  
  write_socket "!STX:SET(M_NUMBER_SAMPLES):VAL($samples);ETX!"
  
  # get reply
  set result [read_socket]
  
  if  { $result == "Successful." } {
    puts "Dls_5204_SetNumberOfSamples: Command successful. Number of samples used: $samples."
  } else {
    perror "Dls_5204_SetNumberOfSamples" $result
  }

}


# Procedure : Dls_5204_SelectNoise
# Purpose   : Selects which noise(s) will be used to create the sample

proc Dls_5204_SelectNoise { noise_type noise_status } {

  # check if  connected
  if { ![IsConnected] } {
    perror "Dls_5204_SelectNoise" "Not connected to unit."
  }

  # check file type
  
  set n_type [string toupper $noise_type]
  switch $n_type {
  
    "XTALK" {
      set n_type "M_XTALK_MUTE"
    }
    
    "RFI" {
      set n_type "M_RFI_MUTE"
    }
    
    default {
      perror "Dls_5204_SelectNoise" "Invalid noise type \"XTALK\" or \"RFI\"."
    }
  }
  
  set n_status [string toupper $noise_status]
  if { $n_status != "ON"  && $n_status != "OFF" } {
    perror "Dls_5204_SelectNoise" "Invalid status. Either \"ON\" or \"OFF\"."
  }
  
  # switch noise
  if { $n_status == "ON" } {
    set real_n_status "OFF"
  } else {
    set real_n_status "ON"
  }
  write_socket "!STX:SET($n_type):VAL($real_n_status);ETX!"
  
  # get reply
  set result [read_socket]
  
  if  { $result == "Successful." } {
    puts "Dls_5204_SelectNoise: Command successful. $noise_type is $n_status."
  } else {
    perror "Dls_5204_SelectNoise" $result
  }


}











 