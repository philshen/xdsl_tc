# ChangeLog
# By Philip Shen @PQA TrendChip Corp.
# ---------------------------------------------------------------------
#May02-2006 Initialize namespace eval Log from API_Loop.tcl
#Jun21-2006 Initialize namespace eval Func_INI to manipulate ini file
# Jun27-2006 Initialize namespace eval Func_Telnet to control modem
# Jun29-2006 Initialize namespace eval Parse_TCModem to parse modem socket dump
# Jun30-2006 Verify namespace eval Parse_TCModem by test_wt100_modem.tcl
# Jul11-2006 from global _sock_desc telnet_line to sock_desc_modem telnet_line_modem
#            Add proc Message::popup to show up warning message
# Jul25-2006 Add Parse_TCModem::_setDUT_DSLOpMode and
#             Parse_TCModem::_GetInfo_adsl-OpMode to check adsl opmode
# Aug04-2006 Add Parse_TCModem::_GetInfo_MinPCB-DS and Parse_TCModem::_GetInfo_DMT-SW
#             for WT100 AnnexB
# Aug08-2006 Add Func_INI::_GetLoopSimulator to get loop simulator modle
# Aug22-2006 Add Parse_TCModem::_SetDUT_TCMOn to turn Trellis Coding on
# Sep04-2006  Add proc Func_INI::_GetDSLAMInfo to get DSLAM paramter.
# Sep11-2006 Add Func_INI::_GetTargetTestCasesID to get target testcase ID
# Sep12-2006 Add Parse_TCModem::_SetDUT_LargeD to set largeD
# Sep14-2006 Add Parse_TCModem::_GetInfo_ADSLDiag to show adsl diagnostic info
# Oct31-2006 Add MiscCommon::Chk_TestResult to check test result pass or fail
# Nov13-2006 Add both Parse_TCModem::GenProCommSetupFile and Parse_TCModem::WanRest
#            to issue "wan adsl reset" thru Procomm
# Nov20-2006 Add three proc Parse_TCModem::DUT_DumpInfo, Parse_TCModem::DUT_DumpADSLDiag, and
#            Parse_TCModem::DUT_InfoFile
# Nov29-2006 Add namespace Parse_TCPureBridge for pure bridge modem
# Dec13-2006 Add proc Parse_TCPureBridge::ZyNOS_GetInfo_chandata
#            Parse_TCPureBridge::ZyNOS_GetInfo_adsl-OpMode
#            Parse_TCPureBridge::ZyNOS_DumpInfo
# Jan13-2007 Add proc MiscCommon::CalcBER_Exp ,MiscCommon::Timer
#            and proc Parse_TCModem::_GetInfo_DumpInfo_PassFail
# Jan18-2007 Modify proc Parse_TCModem::DUT_DumpInfo and 
#            proc Parse_TCPureBridge::DUT_DumpInfo_PureBridge in "wan adsl resset" option
#            Add namespace Test_NoiseMargin proc Test_NoiseMargin::MicroGainIncr
#             proc Test_NoiseMargin::BERDuration
# Feb08-2007 Modify proc Parse_TCModem::DUT_DumpInfo and
#            proc Parse_TCPureBridge::DUT_DumpInfo_PureBridge cause UR2v7 testcase.
# Mar15-2007 Modify proc Parse_TCPureBridge::DUT_DumpInfo_PureBridge and
#            proc Parse_TCModem::DUT_DumpInfo to meet TR100 US/DS datarate in differnet path
# Mar16-2007 Add proc Func_INI::_GetTestCondition
# Mar20-2007 Add adsldiag_iniopt from ini from in proc proc Func_INI::_GetTestCondition
# Mar21-2007 Add proc Func_INI::_GetMultiCPEs to add relay board function.
# Mar26-2007 Add proc Func_Socket::Get_all_devids_sockch and
#             proc Func_Socket::Get_target_devid_sockch
# Mar27-2007 Add proc proc MiscCommon::Log_MultiCPEsFWVer
# Apr02-2007 Add proc Func_Socket::Get_socketch_relaybdpt
# Apr17-2007 Add MiscCommon::LogTableNameMulitCPEsDSLAMs to log out table name to each modem
#            log file.
# Apr19-2007 Add proc Parse_TCModem::_SetDUT_Sysstdio to set session timeout
# May23-2007 For reduce raw csv to csv2xls file, check csv log file if exist or not,
#            then to reduce tile rows.
# Jun252007 Modify proc MiscCommon::Log_MultiDSLAMsFWVer by philip
################################################################################

namespace eval Func_INI {
    package require inifile
    
    variable verbose off
    variable logfile "../log/inifile.log"
    variable list_sections {}
    variable list_keys     {}   
    variable list_values    {}
    variable list_std        {}
    variable list_LoopLen    {}
    variable list_BTLen    {}
    variable list_DUTIP    {}
    variable list_DUTID    {}
    variable str_LoopSimulator ""
    variable list_csvfname {}
    variable list_DSLAMInfo {}
    variable str_NoiseGenerator ""
    variable list_TestCasesID {}
    variable list_TestCondition {}
    variable list_MultiCPEs    {}
    variable list_MultiCPEs_IP    {}    
    variable list_MultiCPEs_ID    {}
    variable list_MultiCPEs_PtNum    {}
    variable list_MultiCPEs_TestCondition    {}
 }
proc Func_INI::PntMsg {strmsg} {
    puts $strmsg
}
################################################################################
# Procedure : PingTest
# Purpose   : Before connecting, check host if exists.
################################################################################
proc Func_INI::PingTest {host} {
    catch {exec ping.exe $host -n 1} pingmsg
    set result [string first "Reply" $pingmsg]
    
    if { $result == -1} {
        # puts "Ping $host no Reply"; update
        return false
    } else {
        # puts "Ping $host OK"; update
        return true
    }
}
proc Func_INI::ChkDUTalive {strIPAddr} {
    set retval [PingTest $strIPAddr]
    
    return $retval
}
proc Func_INI::_GetDUTIP_GetDUTID {inifname} {
    variable list_sections
    variable list_keys
    variable list_values
    variable list_DUTIP
    variable verbose
    variable logfile
    variable list_DUTID
    
    set retval [GetSections $inifname]
    if {$retval == false} {
        return false
    }
    
    foreach temp $list_sections {
        switch -regexp $temp {
            {^DUT} {
                set sec_target $temp
            }
        };#switch
    }
    
    foreach temp [lrange $list_sections 0 end] {
        
        switch -regexp $temp  {
            {^DUT$} {
                set SectionsKey [GetSectionsKeys $inifname $temp]
                set list_msg [GetSectionsKeysValue $inifname $SectionsKey]
                # PntMsg "SectionsKey=$SectionsKey"
                ##Check if input more than 2 IP address
                # if {[llength $list_msg] != 2} {
                    # PntMsg "Please input only one DUT IP Address"
                    # return false
                # }
                ###Get DUT IP address
                foreach x [lrange $list_msg 0 end] {
                    # PntMsg "x=$x"
                    switch -regexp $x {
                        {^192\.[0-9][0-9][0-9]\.} {
                            lset list_DUTIP $x
                        }
                        {[a-zA-Z0-9][a-zA-Z0-9][a-zA-Z0-9][a-zA-Z0-9][a-zA-Z0-9]} {
                            lset list_DUTID $x
                            # PntMsg "x=$x"
                        }
                        {[A-Z][A-Z][A-Z][A-Z]d$} {
                            lset list_DUTID $x
                        }
                        {[0-9][0-9][0-9][0-9][0-9]d$} {
                            lset list_DUTID $x
                        }
                    };#switch
                };#foreach
            }
            
        };#switch -regexp $temp
        
    };#foreach temp
    
    if {$verbose == on} {
        Log "info" $logfile [list "list_DUTIP = $list_DUTIP"]
    }
    return true
}
################################################################################
# proc Func_INI::_GetDUTID {inifname} {
    # variable list_sections
    # variable list_keys
    # variable list_values
    # variable list_DUTID
    # variable verbose
    # variable logfile
    
    # set retval [GetSections $inifname]
    # if {$retval == false} {
        # return false
    # }
    
    # foreach temp $list_sections {
        # switch -regexp $temp {
            # {^DUT} {
                # set sec_target $temp
            # }
        # };#switch
    # }
    
    # set retval [GetKeys $inifname $sec_target]
    # if {$retval == false} {
        # return false
    # }
    
    # foreach temp $list_keys {
        # set retval [GetValue $inifname $sec_target $temp]
        # set list_DUTID $list_values
        # PntMsg "temp=$temp; retval=$retval; list_values=$list_values"
    # }
    
    # if {$verbose == on} {
        # Log "info" $logfile [list "[lindex $list_keys 1] = $list_DUTID"]
    # }
    
    # return true
# }
# #
################################################################################
proc Func_INI::GetSections {inifname} {
    variable list_sections
    
    set hinifile [ini::open $inifname r]
    set list_sections [ini::sections $hinifile]
    ini::close $hinifile
    #list length == 0
    if ![llength $list_sections] {
        return false
    }
    return true
}
proc Func_INI::GetKeys {inifname section} {
    variable list_keys
    
    set hinifile [ini::open $inifname r]
    set list_keys [ini::keys $hinifile $section]
    ini::close $hinifile
    if ![llength $list_keys] {
        return false
    }
    return true
}
proc Func_INI::GetValue {inifname section key} {
    variable list_values
    
    set hinifile [ini::open $inifname r]
    set keyvalue [ini::value $hinifile $section $key]
    ini::close $hinifile
    
    if ![string length $keyvalue] {
        return false
    }
    
    set list_values [split $keyvalue ',']
    return true
}
proc Func_INI::Chosetoken {token list_in} {
    
    foreach temp $list_in {
        switch -regexp $temp {
            {^$token} {
                return $temp
            }
        };#switch
    }
    
    return false
}
proc Func_INI::ChoseStd {list_in} {
    set temp ""
    
    foreach temp $list_in {
        switch -regexp $temp {
            {^WT} {
                return $temp
            }
            {^TR} {
                return $temp
            }
            {^PD} {
                return $temp
            }
            {^UR2} {
                return $temp
            }
        };#switch -regexp $temp
    }
    
    return false
}
################################################################################
# proc Func_INI::GetStd
#     Return "WT 100"
################################################################################

proc Func_INI::_GetStd {inifname} {
    variable list_sections
    variable list_keys
    variable list_values
    variable list_std
    variable verbose
    variable logfile
    
    set sec_std ""
    set sec_key ""
    
    set retval [GetSections $inifname]
    if {$retval == false} {
        return false
    }
    # if {$verbose == on} {
    # Log "info" $logfile $list_sections
    # }
    
    #set sec_std [Chosetoken "Std" $list_sections]
    foreach temp $list_sections {
        switch -regexp $temp {
            {^Std} {
                set sec_std $temp
            }
        };#switch
    }
    if {$verbose == on} {
        Log "info" $logfile [list "SectionStd= $sec_std"]
    }
    
    set retval [GetKeys $inifname $sec_std]
    if {$retval == false} {
        return false
    }
    if {$verbose == on} {
        Log "info" $logfile $list_keys
    }
    
    set sec_key [ChoseStd $list_keys]
    if {$verbose == on} {
        Log "info" $logfile [list "SectionKey= $sec_key"]
    }
    
    set retval [GetValue $inifname $sec_std $sec_key]
    if {$retval == false} {
        return false
    }
    
    lappend list_std $sec_key $list_values
}
################################################################################
# proc Func_INI::_GetLoopSimulator {inifname}
# Result:
# Loop Simulator= LoopSimulator
# keys of Loop simulator= Model
# Value of Loop simulator= DLS410A
################################################################################
proc Func_INI::_GetLoopSimulator {inifname} {
    variable list_sections
    variable list_keys
    variable list_values
    variable str_LoopSimulator
    variable verbose
    variable logfile
    
    set sec_loopmodle ""
    set sec_key ""
    
    set retval [GetSections $inifname]
    if {$retval == false} {
        return false
    }
    
    foreach temp $list_sections {
        switch -regexp $temp {
            {^LoopSimulator} {
                set sec_loopmodle $temp
            }
        };#switch
    }
    
    set retval [GetKeys $inifname $sec_loopmodle]
    if {$retval == false} {
        return false
    }
    
    set sec_key [lindex $list_keys 0]
    set retval [GetValue $inifname $sec_loopmodle $sec_key]

    if {$verbose == on} {
        Log "info" $logfile [list "Loop Simulator= $sec_loopmodle" \
                "keys of Loop simulator= $list_keys" \
                "Value of Loop simulator= [lindex $list_values 0]"]
    }
    
    if {$retval == false} {
        return false
    }
    
    set str_LoopSimulator [lindex $list_values 0]
}

proc Func_INI::GetSectionsKeys {inifname what} {
    variable list_sections
    variable verbose
    variable logfile
    if {$verbose == on} {
        Log "info" $logfile [list "what=$what"  "Sections=$list_sections"]
    }
    
    foreach temp $list_sections {
        if [string match "*$what" $temp ] {
            GetKeys $inifname $temp
            set section $temp
            return $section
        }
    };#foreach temp $list_sections
    
    return false
}
################################################################################
# proc Func_INI::GetSectionsKeysValue
#
# Return list of value from SectionKeys
################################################################################
proc Func_INI::GetSectionsKeysValue {inifname what} {
    variable list_keys
    variable list_values
    variable verbose
    
    foreach temp $list_keys {
        GetValue $inifname $what $temp
        foreach x $list_values {
            if {$verbose == on} {
                puts "$temp=$x"
            }
            
            #append str_msg $temp "=" $x; lappend list_rtumsg $str_msg
        }
        lappend list_msg $temp $list_values
    };#foreach temp $Func_INI::list_keys
    
    return $list_msg
}
################################################################################
#while LoopLength=100,200 in steup.ini
#Func_INI::_Chk_LoopBT-Len could get more than one length
################################################################################
proc Func_INI::_Chk_LoopBT-Len {list_SectionKeyValue} {
    variable list_LoopLen
    variable list_BTLen
    variable verbose
    variable logfile
    
    array set arr_SectionKeyValue $list_SectionKeyValue
    set list_LoopLen [split $arr_SectionKeyValue(LoopLength) ',']
    set list_BTLen [split $arr_SectionKeyValue(BTLength) ',']
    set len_listLoopLen [llength $list_LoopLen]
    set len_listBTLen [llength $list_BTLen]
    
    if {($len_listLoopLen > 1) && ($len_listBTLen > 1)} {
        puts "Pls check both LoopLength and BTLength setting"
        return false
    } elseif {($len_listLoopLen == 1) | ($len_listBTLen > 1)} {
        set strmsg "LoopLength=$list_LoopLen; BTLength=$list_BTLen"
    } elseif {($len_listLoopLen > 1) | ($len_listBTLen == 1)}  {
        set strmsg "LoopLength=$list_LoopLen; BTLength=$list_BTLen"
    }
    
    if {$verbose == on} {
        puts $strmsg
        Log "info" $logfile [list $strmsg]
    }
    return true
}
proc Func_INI::Log {level filename list_msg} {
    foreach temp $list_msg {
        set msg "$temp"
        Log::tofile $level $filename $msg
    }
}

proc Func_INI::_GetCSVFname {inifname} {
    variable list_sections
    variable list_keys
    variable list_values
    variable list_csvfname
    variable verbose
    variable logfile
    
    set retval [GetSections $inifname]
    if {$retval == false} {
        return false
    }
    
    foreach temp $list_sections {
        switch -regexp $temp {
            {^TestCases} {
                set sec_csvfname $temp
            }
        };#switch
    }
    
    set retval [GetKeys $inifname $sec_csvfname]
    if {$retval == false} {
        return false
    }
    
    set sec_key [lindex $list_keys 0]
    set retval [GetValue $inifname $sec_csvfname $sec_key]
    
    if {$verbose == on} {
        Log "info" $logfile [list "TestCases = $sec_csvfname" \
                "keys of TestCases = $list_keys" \
                "Value of TestCases = $list_values"]
    }
    
    if {$retval == false} {
        return false
    }
    
    set list_csvfname $list_values
}
proc Func_INI::_GetDSLAMInfo {inifname} {
    variable list_sections
    variable list_keys
    variable list_values
    variable list_DSLAMInfo
    variable verbose
    variable logfile

    set retval [GetSections $inifname]
    if {$retval == false} {
        return false
    }
    
    foreach temp $list_sections {
        switch -regexp $temp {
            {^DSLAM} {
                set sec_dslam $temp
            }
        };#switch
    }

    set retval [GetKeys $inifname $sec_dslam]
    if {$retval == false} {
        return false
    }
    
    foreach temp $list_keys {
        set retval [GetValue $inifname $sec_dslam $temp]
        lappend list_DSLAMInfo $list_values
    }
    
    if {$verbose == on} {
        Log "info" $logfile [list "[lindex $list_keys 0] = [lindex $list_DSLAMInfo 0]" \
                "[lindex $list_keys 1] = [lindex $list_DSLAMInfo 1]"]
    }
    
    return true
}
proc Func_INI::_GetCPETestConds {inifname} {
    variable list_sections
    variable list_keys
    variable list_values
    variable list_CPE_IP
    variable list_CPE_ID
    variable list_CPE_TestCondition
    variable verbose
    variable logfile
    
    set retval [GetSections $inifname]
    if {$retval == false} {
        return false
    }
    
    foreach temp $list_sections {
        switch -regexp $temp {
            {^DUT$} {
                set sec_testcond $temp
                
                set retval [GetKeys $inifname $sec_testcond]
                if {$retval == false} {
                    return false
                };#if {$retval == false}
                
                set list_ID {}
                set list_IP {}
                foreach temp $list_keys {
                    
                    switch -regexp [string tolower $temp] {
                        {waitshowtime_times} {
                            GetValue $inifname $sec_testcond $temp
                            set waitlinkup_times $list_values
                        }
                        {aftershowtime_duration} {
                            GetValue $inifname $sec_testcond $temp
                            set stability $list_values
                        }
                        {adslreset_opt} {
                            GetValue $inifname $sec_testcond $temp
                            set adslrest $list_values
                        }
                        {^dut_ip} {
                            GetValue $inifname $sec_testcond $temp
                            lappend list_CPE_IP $list_values
                            lset list_IP $list_values
                        }
                        {^dut_id} {
                            GetValue $inifname $sec_testcond $temp
                            lappend list_CPE_ID $list_values
                            lset list_ID $list_values
                        }
                    };#switch -regexp [string tolower $temp]
                    
                };#foreach temp $list_keys
                lappend list_CPE [list $list_ID $list_IP]
                
                lappend list_CPE_TestCondition [list $list_ID $list_IP \
                        $waitlinkup_times $stability $adslrest]
            }
        };#switch -regexp $temp
    };#foreach temp $list_sections
    
    if {$verbose == on} {
        foreach x $list_CPE_ID y $list_CPE_IP {
            if [info exist strmsg] {unset strmsg}
            append strmsg "ID=$x; IP=$y"
            puts $strmsg
        }
        
        puts "$list_CPE; [llength $list_CPE]"
        
        puts "list_CPE_TestCondition=$list_CPE_TestCondition."
        Log "info" $logfile $list_CPE_TestCondition
    }
    
    return true
}
proc Func_INI::_GetMultiDSLAMs {inifname} {
    variable list_sections
    variable list_keys
    variable list_values
    variable list_MultiDSLAMs_ID
    variable list_MultiDSLAMs_PtNum
    variable list_MultiDSLAMs_PtNum_RelayBDPtNum
    variable verbose
    variable logfile
    
    set retval [GetSections $inifname]
    if {$retval == false} {
        return false
    }
    
    foreach temp $list_sections {
        switch -regexp $temp {
            {^RelayBD_Pt0[0,1,2,3,4,5,6,7]$} {
                set sec_testcond $temp
                
                set retval [GetKeys $inifname $sec_testcond]
                if {$retval == false} {
                    return false
                };#if {$retval == false}
                
                set list_Devic_ID {}
                set list_DSLAM_PtNum {}
                foreach temp $list_keys {
                    
                    switch -regexp [string tolower $temp] {
                        {^portnum} {
                            GetValue $inifname $sec_testcond $temp
                            lappend list_MultiDSLAMs_PtNum $list_values
                            lset list_DSLAM_PtNum $list_values
                        }
                        {^device_id} {
                            GetValue $inifname $sec_testcond $temp
                            lappend list_MultiDSLAMs_ID $list_values
                            lset list_Devic_ID $list_values
                        }
                    };#switch -regexp [string tolower $temp]
                    
                    ##Get Pt number
                    # puts "temp=$temp"
                    set list_temp [split $temp '_']
                    set strpt_num [lindex $list_temp end]
                    scan $strpt_num "%d" relaybd_pt_num
                    
                };#foreach temp $list_keys
                
                lappend list_MultiDSLAMs_PtNum_RelayBDPtNum \
                        [list $list_Devic_ID $list_DSLAM_PtNum $relaybd_pt_num]
            }
        };#switch -regexp $temp
    };#foreach temp $list_sections
    
    if {$verbose == on} {
        foreach x $list_MultiDSLAMs_ID z $list_MultiDSLAMs_PtNum {
            if [info exist strmsg] {unset strmsg}
            append strmsg "DSLAM_ID=$x; Pt_Num=$z"
            puts $strmsg
        }
        
        puts "list_MultiDSLAMs_PtNum_RelayBDPtNum=$list_MultiDSLAMs_PtNum_RelayBDPtNum."
        Log "info" $logfile $list_MultiDSLAMs_PtNum_RelayBDPtNum
    }
    
    return true

}
proc Func_INI::_GetMultiCPEsTestConds {inifname} {
    variable list_sections
    variable list_keys
    variable list_values
    variable list_MultiCPEs
    variable list_MultiCPEs_IP
    variable list_MultiCPEs_ID
    variable list_MultiCPEs_PtNum
    variable list_MultiCPEs_TestCondition
    variable verbose
    variable logfile
    
    set retval [GetSections $inifname]
    if {$retval == false} {
        return false
    }
    
    foreach temp $list_sections {
        switch -regexp $temp {
            {^RelayBD_Pt0[0,1,2,3,4,5,6,7]$} {
                set sec_testcond $temp
                
                set retval [GetKeys $inifname $sec_testcond]
                if {$retval == false} {
                    return false
                };#if {$retval == false}
                
                set list_ID {}
                set list_IP {}
                foreach temp $list_keys {
                    
                    switch -regexp [string tolower $temp] {
                        {waitshowtime_times} {
                            GetValue $inifname $sec_testcond $temp
                            set waitlinkup_times $list_values
                        }
                        {aftershowtime_duration} {
                            GetValue $inifname $sec_testcond $temp
                            set stability $list_values
                        }
                        {adslreset_opt} {
                            GetValue $inifname $sec_testcond $temp
                            set adslrest $list_values
                        }
                        {^dut_ip} {
                            GetValue $inifname $sec_testcond $temp
                            lappend list_MultiCPEs_IP $list_values
                            lset list_IP $list_values
                        }
                        {^dut_id} {
                            GetValue $inifname $sec_testcond $temp
                            lappend list_MultiCPEs_ID $list_values
                            lset list_ID $list_values
                        }
                    };#switch -regexp [string tolower $temp]
                    
                    ##Get Pt number
                    # puts "temp=$temp"
                    set list_temp [split $temp '_']
                    set strpt_num [lindex $list_temp end]
                    scan $strpt_num "%d" pt_num
                    
                };#foreach temp $list_keys
                lappend list_MultiCPEs_PtNum $pt_num
                lappend list_MultiCPEs [list $list_ID $list_IP $pt_num]
                
                lappend list_MultiCPEs_TestCondition [list $list_ID $list_IP \
                        $waitlinkup_times $stability $adslrest $pt_num]
            }
        };#switch -regexp $temp
    };#foreach temp $list_sections
    
    if {$verbose == on} {
        foreach x $list_MultiCPEs_ID y $list_MultiCPEs_IP z $list_MultiCPEs_PtNum {
            if [info exist strmsg] {unset strmsg}
            append strmsg "ID=$x; IP=$y; Pt_Num=$z"
            puts $strmsg
        }
        
        puts "$list_MultiCPEs; [llength $list_MultiCPEs]"
        
        puts "list_MultiCPEs_TestCondition=$list_MultiCPEs_TestCondition."
        Log "info" $logfile $list_MultiCPEs_TestCondition
    }
    
    return true
}
proc Func_INI::_GetMultiCPEs {inifname} {
    variable list_sections
    variable list_keys
    variable list_values
    variable list_MultiCPEs
    variable list_MultiCPEs_IP
    variable list_MultiCPEs_ID
    variable list_MultiCPEs_PtNum
    variable verbose
    variable logfile
    
    set retval [GetSections $inifname]
    if {$retval == false} {
        return false
    }
    
    foreach temp $list_sections {
        switch -regexp $temp {
            {^RelayBD_Pt0[0,1,2,3,4,5,6,7]$} {
                set sec_testcond $temp
                
                set retval [GetKeys $inifname $sec_testcond]
                if {$retval == false} {
                    return false
                };#if {$retval == false}
                
                set list_ID {}
                set list_IP {}
                foreach temp $list_keys {
                    set retval [GetValue $inifname $sec_testcond $temp]
                    
                    ###Get DUT IP address
                    foreach x [lrange $list_values 0 end] {
                        # PntMsg "x=$x"
                        switch -regexp $x {
                            {^192\.[0-9][0-9][0-9]\.} {
                                lappend list_MultiCPEs_IP $x
                                lset list_IP $x
                            }
                            {[a-zA-Z0-9][a-zA-Z0-9][a-zA-Z0-9][a-zA-Z0-9][a-zA-Z0-9]} {
                                lappend list_MultiCPEs_ID $x
                                lset list_ID $x
                            }
                            {[A-Z][A-Z][A-Z][A-Z]d$} {
                                lappend list_MultiCPEs_ID $x
                                lset list_ID $x
                            }
                            {[0-9][0-9][0-9][0-9][0-9]d$} {
                                lappend list_MultiCPEs_ID $x
                                lset list_ID $x
                            }
                        };#switch
                    };#foreach x [lrange $list_values 0 end]
                    
                    ##Get Pt number
                    # puts "temp=$temp"
                    set list_temp [split $temp '_']
                    set strpt_num [lindex $list_temp end]
                    scan $strpt_num "%d" pt_num
                   
                };#foreach temp $list_keys
                lappend list_MultiCPEs_PtNum $pt_num
                lappend list_MultiCPEs [list $list_ID $list_IP $pt_num]

            }
            
        };#switch -regexp $temp
    };#foreach temp $list_sections
    
    if {$verbose == on} {
        # Log "info" $logfile [list "[lindex $list_keys 0] = [lindex $list_MultiCPEs_ID 0]" \
                # "[lindex $list_keys 1] = [lindex $list_MultiCPEs_IP 0]"]
                # "[lindex $list_keys 2] = [lindex $list_TestCondition 2]"]
        foreach x $list_MultiCPEs_ID y $list_MultiCPEs_IP z $list_MultiCPEs_PtNum {
            if [info exist strmsg] {unset strmsg}
            append strmsg "ID=$x; IP=$y; Pt_Num=$z"
            puts $strmsg
        }
        
        puts "$list_MultiCPEs; [llength $list_MultiCPEs]"
        
    }
    
    return true
    
}
proc Func_INI::_GetTestCondition {inifname} {
    variable list_sections
    variable list_keys
    variable list_values
    variable list_TestCondition
    variable verbose
    variable logfile
    
    set retval [GetSections $inifname]
    if {$retval == false} {
        return false
    }
    
    foreach temp $list_sections {
        switch -regexp $temp {
            {^TestCondition} {
                set sec_testcond $temp
            }
        };#switch
    }
    
    set retval [GetKeys $inifname $sec_testcond]
    if {$retval == false} {
        return false
    }
    
    foreach temp $list_keys {
        set retval [GetValue $inifname $sec_testcond $temp]
        lappend list_TestCondition $list_values
    }
    
    if {$verbose == on} {
        Log "info" $logfile [list "[lindex $list_keys 0] = [lindex $list_TestCondition 0]" \
                "[lindex $list_keys 1] = [lindex $list_TestCondition 1]" \
                "[lindex $list_keys 2] = [lindex $list_TestCondition 2]"]
    }
    
    return true
}
proc Func_INI::_GetNoiseGenerator {inifname} {
    variable list_sections
    variable list_keys
    variable list_values
    variable str_NoiseGenerator
    variable verbose
    variable logfile
    
    set retval [GetSections $inifname]
    if {$retval == false} {
        return false
    }
    
    foreach temp $list_sections {
        switch -regexp $temp {
            {^NoiseGenerator} {
                set sec_noise $temp
            }
        };#switch
    }
    
    set retval [GetKeys $inifname $sec_noise]
    if {$retval == false} {
        return false
    }
    
    foreach temp $list_keys {
        set retval [GetValue $inifname $sec_noise $temp]
        set str_NoiseGenerator $list_values
    }
    
    if {$verbose == on} {
        Log "info" $logfile [list "[lindex $list_keys 0] = $str_NoiseGenerator"]
    }
    
    return true
}
proc Func_INI::_GetTestCasesID {inifname} {
    variable list_sections
    variable list_keys
    variable list_values
    variable list_TestCasesID
    variable verbose
    variable logfile
    
    set retval [GetSections $inifname]
    if {$retval == false} {
        return false
    }
    
    foreach temp $list_sections {
        switch -regexp $temp {
            
            {^TestCasesID} {
                set sec_target $temp
            }
        };#switch
    }
    
    set retval [GetKeys $inifname $sec_target]
    if {$retval == false} {
        return false
    }
    
    set sec_key [lindex $list_keys 0]
    set retval [GetValue $inifname $sec_target $sec_key]
    
    if {$verbose == on} {
        Log "info" $logfile [list "TestCasesID = $sec_target" \
                "keys of TestCasesID = $list_keys" \
                "Value of TestCasesID = $list_values"]
    }
    
    if {$retval == false} {
        return false
    }
    
    set list_TestCasesID $list_values
}
proc Func_INI::_GetTargetTestCasesID {target_casesID list_all_csvrow} {
    set list_target_casesid {}
    
    foreach temp [lrange $target_casesID 0 end] {
        switch -regexp [string tolower $temp] {
            {^all} {
                ##Check if exists both "all" and "1040-1087"
                if {[llength $target_casesID] > 1} {
                    puts "Pls select \"$temp\" only at TestCasesID in setup.ini."
                    break
                } else  {
                    set list_target_casesid $list_all_csvrow
                };#if {[llength $Func_INI::list_TestCasesID] > 1}
            }
            
            {-} {
                set list_value [split $temp "-"]
                #Check ID number order
                set firstnum [scan [lindex $list_value 0] "%d"]
                set secondnum [scan [lindex $list_value 1] "%d"]
                if {[expr $secondnum - $firstnum] < 0} {
                    puts "TestCasesID:[lindex $list_value 1] > TestCasesID:[lindex $list_value 0] \
                            in $temp."
                    exit
                };#if {[expr [lindex $list_value 1] - [lindex $list_value 0]] < 0}
                
                foreach csvrow_value $list_all_csvrow {
                    if {([lindex $csvrow_value 0] <= [lindex $list_value 1])&& \
                                ([lindex $csvrow_value 0] >= [lindex $list_value 0])} {
                        lappend list_target_casesid $csvrow_value
                    };#if
                };#foreach csvrow_value $list_csvrow
            }
            
            default {
                foreach csvrow_value $list_all_csvrow {
                    if {[lindex $csvrow_value 0] == $temp} {
                        lappend list_target_casesid $csvrow_value
                    };#if {[lindex $csvrow_value 0] == $temp}
                };#foreach csvrow_value $list_csvrow
            }
            
        };#switch -regexp [string tolower $temp]
    };#foreach temp $Func_INI::list_TestCasesID
    
    
    return $list_target_casesid
}
########################################
# Define namespace for raw csv to csv2xls
##########################################
namespace eval LogDict {
    global auto_path
    
    set perv_path [file dirname [pwd]]
    set lib_path [file join $perv_path "xDSL_Lib"]
    set lib_path_dict [file join $lib_path "dict8_5_2"]
    #append dict library path to auto_path
    lappend auto_path $lib_path_dict
    
    package require dict
    
}
proc LogDict::replace_list_times {token list_input} {
    set indx -1
    set ret_list $list_input
    
    foreach element $list_input {
        incr indx
        if {[llength $element] > 2} {
            switch -regexp [lindex $element 0] {
                {^[0-9][0-9][0-9][0-9]$} {
                    if {$token == [lindex $element 0]} {
                        ##Reduce list overlap content
                        set ret_list [replace_times $indx $ret_list]
                        break
                    };#if {$temp == $element}
                }
            };#switch -regexp [lindex 0 $element]
            
        };#if {[llength $element] > 1}
        
    };#foreach element $list_input
    
    return $ret_list
}

################################################################################
# Reduce overlape table name:
# below case
# 1531,0, ,06:19:19 020807,B1_RA_Interleave,6144,640,0,0,0,0,0,0,0,0,0,0,0,0,0,0,16.5,13.0,G.992.1(G.DMT),fail,4864,768,6,6
#   0  1 2          3              4
# Current Table of Test plan= TableB_2-5-1-1_White_Fix_0864_0160.
# 1549,0,,06:20:10 020807,B1_Fix_I_0864_0160,0,0,0,0,0,0,0,0,864,608,0,0,0,0,0,0,42.5,19.0,G.992.1(G.DMT),pass,0,0,6,6
# 1549,0,,06:20:23 020807,B1_Fix_I_0864_0160,0,0,0,0,0,0,0,0,864,608,0,0,0,0,0,0,42.5,19.0,G.992.1(G.DMT),pass,0,0,6,6
# Current Table of Test plan= TableB_2-5-1-1_White_Fix_0864_0160.
# 1550,0,,06:20:10 020807,B1_Fix_I_0864_0160,0,0,0,0,0,0,0,0,864,608,0,0,0,0,0,0,42.5,19.0,G.992.1(G.DMT),fail,0,0,6,6
################################################################################
proc LogDict::replace_overlapetable {list_token list_input} {
    foreach temp $list_token {
        switch -regexp [string tolower $temp]  {
            {^current\ table\ of\ } {
                set indx -1
                foreach element $list_input {
                    incr indx
                    if {$temp == [lindex $element 0]} {
                        set last_element [lindex $list_input [expr $indx - 1]]
                        set last_element_profile [lindex $last_element 4]
                        set next_element [lindex $list_input [expr $indx + 1]]
                        set next_element_profile [lindex $next_element 4]
                        ##Test cases of the same table
                        if {$last_element_profile == $next_element_profile} {
                            # puts "last_element= $last_element\n"; puts "next_element= $next_element"
                            
                            set list_input [lreplace $list_input $indx $indx ""]
                        };#if {$last_element_profile == $next_element_profile}
                        
                    };#if {$temp == $element}
                };#foreach element $data_list1
            }
        };#switch -regexp [string tolower $temp]
        
    };#foreach temp $list_noteven_caseid
    
    set ret_list $list_input
    return $ret_list
}
proc LogDict::get_token_list {stropt orginal_dict} {
    set opt [string tolower $stropt]
    upvar 1 $orginal_dict input_dict
    # puts "In proc get_token_list, [dict size $input_dict]"
    set ret_list_caseid {}
    
    if {$opt == "noteven"} {
        foreach id [dict keys $input_dict] {
            # puts  "In proc get_token_list, id=$id"
            ################################################################################
            # element
            #  {1531 0 {} {06:19:10 020807} B1_RA_Interleave 6144 640 0 0 0 0 0 0 0 0 0
            #    0 0 0 0 0 16.5 13.0 G.992.1(G.DMT) fail 4864 768 6 6}
            #  {1531 0 {} {06:19:14 020807} B1_RA_Interleave 6144 640 0 0 0 0 0 0 0 0 0
            #    0 0 0 0 0 16.5 13.0 G.992.1(G.DMT) fail 4864 768 6 6}
            #  {1531 0 {} {06:19:19 020807} B1_RA_Interleave 6144 640 0 0 0 0 0 0 0 0 0
            #    0 0 0 0 0 16.5 13.0 G.992.1(G.DMT) fail 4864 768 6 6}
            ################################################################################
            set element [dict get $input_dict $id]
            set count [llength $element]
            # puts "Hello, $element!"
            if {[expr $count % 2] != 0} {
                ##Get test case id only
                switch -regexp $id {
                    {^[0-9][0-9][0-9][0-9]$} {
                        lappend ret_list_caseid $id
                    }
                };#switch -regexp $id
                
            };#if {[expr $count % 2] != 0}
            ################################################################################
            #Current Table of Test plan= TableB_2-5-1-1_White_Fix_0864_0160.
            #Current Table of Test plan= TableB_2-5-1-1_White_Fix_0864_0160.
            ################################################################################
            switch -regexp [string tolower $id] {
                {^current\ table\ of\ } {
                    # puts "id=$id"
                    set element [dict get $input_dict $id]
                    set count [llength $element]
                    if {$count > 1} {
                        lappend ret_list_caseid $id
                    };#if {$count > 1}
                }
            };#switch -regexp [string tolower $id]
            
        };#foreach id [dict keys $input_dict]
        
    } elseif {$opt == "4times"} {
        foreach id [dict keys $input_dict] {
            set element [dict get $input_dict $id]
            set count [llength $element]
            if {[expr $count % 4] == 0} {
                ##Get test case id only
                switch -regexp $id {
                    {^[0-9][0-9][0-9][0-9]$} {
                        lappend ret_list_caseid $id
                    }
                };#switch -regexp $id
            };#if {[expr $count % 4] == 0}
        };#foreach id [dict keys $input_dict]
        
    };#if {$opt == "noteven"}
    
    return $ret_list_caseid
}
proc LogDict::replace_times {index list_input} {
    ##Reduce list overlap content
    set ret_list [lreplace $list_input $index $index ""]
    
    return $ret_list
}

proc LogDict::lcount {list_input} {
    set count {}
    foreach element $list_input {dict incr count $element}
    # set count;
    return $count
}
proc LogDict::createdic {id list_input} {
    foreach element $list_input {
        dict lappend temp_dic $id $element
    }
    
    return $temp_dic
}
proc LogDict::createdicbyid {list_input} {
    foreach element $list_input {
        dict lappend temp_dic [lindex $element 0] $element
    }
    
    return $temp_dic
}


########################################
# Define a simple custom logproc
##########################################
namespace eval Log {
    package require logger
}

# Define a simple custom logproc
proc Log::log_to_file {lvl logfiletxt} {
    # set logfile "mylog.log"
    set msg "\[[clock format [clock seconds] -format "%H:%M:%S %m%d%Y"]\] \
            [lindex $logfiletxt 1]"
    set f [open [lindex $logfiletxt 0] {WRONLY CREAT APPEND}] ;# instead of "a"
    fconfigure $f -encoding utf-8
    puts $f $msg
    close $f
}
################################################################################
#${log}::logproc level
# ${log}::logproc level command
# ${log}::logproc level argname body
# This command comes in three forms - the third, older one is deprecated
# and may be removed from future versions of the logger package.
# The current set version takes one argument, a command to be executed
# when the level is called. The callback command takes on argument,
# the text to be logged. If called only with a valid level logproc returns
# the name of the command currently registered as callback command.
# logproc specifies which command will perform the actual logging for a given level.
# The logger package ships with default commands for all log levels,
# but with logproc it is possible to replace them with custom code.
# This would let you send your logs over the network, to a database, or anything else.
################################################################################
proc Log::tofile {level strfilename strtext} {
    # Initialize the logger
    set log [logger::init global]
    
    # Install the logproc for all levels
    foreach lvl [logger::levels] {
        interp alias {} log_to_file_$lvl {} Log::log_to_file $lvl
        ${log}::logproc $lvl log_to_file_$lvl
    }
    
    ${log}::$level [list $strfilename $strtext]
    
    return true
}

proc Log::parray {a {pattern *}} {
    upvar 1 $a array
    if ![array exists array] {
        error "\"$a\" isn't an array"
    }
    set maxl 0
    foreach name [lsort [array names array $pattern]] {
        if {[string length $name] > $maxl} {
            set maxl [string length $name]
        }
    }
    set maxl [expr {$maxl + [string length $a] + 2}]
    foreach name [lsort [array names array $pattern]] {
        set nameString [format %s(%s) $a $name]
        puts stdout [format "%-*s = %s" $maxl $nameString $array($name)]
    }
}
proc Log::LogList_level {level filename list_msg} {
    set path_logfile [file join ".." "log" $filename]
    
    foreach temp $list_msg {
        set strmsg "$temp"
        Log::tofile $level $path_logfile $strmsg
    }
}
proc Log::LogOut {str_out logfname} {
    
    set path_logfile [file join ".." "log" $logfname]
    # set clk [clock format [clock seconds] -format "%b%d%H%M%S%Y"]
    # append str_out;# "  ";append Str_Out $clk
    
    set rnewinfd [open $path_logfile a]
    puts $rnewinfd $str_out
    flush $rnewinfd;close $rnewinfd
}
################################################################################
# 
################################################################################
namespace eval Func_Socket {
    variable _verbose off
    variable logfile "../log/Socket.log"
    
    variable _prompt "*> *"
    variable socketport 23;#default telnet port
}

proc Func_Socket::Socket_Close {ch_sock} {
    
    if { ![IsConnected $ch_sock] } {
        Func_INI::PntMsg "Not connected to unit."
    }
    # close socket
    close $ch_sock
    set ch_sock ""
    Func_INI::PntMsg "DUT Disconnect successful."
}

proc Func_Socket::write_socket {ch_sock command } {
    ;#ch_sock
    variable _verbose
    variable logfile
    
    set snd "Sending: "
    if {[eof $ch_sock]} {
        Func_INI::PntMsg "Error: A connection has not been established!!"
    }
    if { $_verbose == "on" } {
        set strmsg "$snd $command"
        Func_INI::PntMsg $strmsg
        Log::tofile "info" $logfile $strmsg
    }
    update
    puts $ch_sock $command;flush $ch_sock
}

proc Func_Socket::Socket_Open { ip {port ""} } {
    variable socketport
    
    # Check for valid args
    if {$port == ""} {
        set port $socketport
    }
    if { $ip != "" && $port != "" } {
        # if { [IsConnected] } {
        # Func_INI::PntMsg "DUT had already connected"
        # }
        set ch_sock [socket $ip $port]
        if {[eof $ch_sock]} {
            Func_INI::PntMsg "Cannot establish connection to $ip on port $port."
        } else {
            fconfigure $ch_sock -blocking 0 -buffering line -translation auto
            Func_INI::PntMsg "DUT connection is sucessful."
            return $ch_sock
        }
    }
    puts  "Invalid amount of parameters."
}
proc Func_Socket::SendCmd {hdsock strcmd {prompt " "}} {
    variable _prompt
    
    if {$prompt == " "} {
        set prompt $_prompt
    }
    write_socket $hdsock $strcmd
    # set strdut [read_socket]
    # set find [string match "$prompt" $strdut]
    set find 0
    while {$find == 0} {
        set  strtemp [read_socket]
        append  strdut $strtemp
        set find [string match "$prompt" [string tolower $strdut]]
    }
    #output socket dump
    set fdout [open  "sock.temp" w+]
    puts $fdout $strdut; flush $fdout
    close $fdout
    
    return $strdut
}
proc Func_Socket::Waitfor {hdsock {prompt " "}} {
    variable _prompt
    
    if {$prompt == " "} {
        set prompt $_prompt
    }
    set strdut [read_socket]
    set find [string match $prompt $strdut]
    while {$find == 0} {
        set strdut [read_socket]
        set find [string match $prompt $strdut]
    }
    
    return true
}
proc Func_Socket::IsConnected { ch_sock} {
    # global _sock_desc
    
    if { $ch_sock == "" } {
        return 0
    }
    return 1
}
proc Func_Socket::Get_all_devids_sockch {dut_id dut_ip bd_ptnum} {
    
    set strmsg "DUT $dut_ip is alive."
    if {[Func_INI::ChkDUTalive $dut_ip] == false} {
        set strmsg "DUT $dut_ip is not alive."
        #Show warming msg
        Message::popup [list "warning" $::pref(appname) $::VERSION \
                "Check Modem Cat5 connection." $strmsg]
        exit
    }
    insertLogLine info $strmsg
    
    set sock_modem [Func_Socket::Socket_Open $dut_ip]
    set list_temp [list $dut_id $dut_ip $sock_modem $bd_ptnum]
    
    return $list_temp
}
proc Func_Socket::Get_target_devid_sockch {list_sock_cpes relaybd_ptnum} {
    
    foreach temp_sock_moden $list_sock_cpes {
        if {[lindex $temp_sock_moden end] == $relaybd_ptnum} {
            set sock_ch_moden [lindex $temp_sock_moden end-1]
            return $sock_ch_moden
        }
    };#foreach temp $list_sock_multicpes
    
    return false
}
proc Func_Socket::Get_socketch_relaybdpt {list_sock_multicpes bd_ptnum} {
    set retsock [Func_Socket::Get_target_devid_sockch $list_sock_multicpes $bd_ptnum]
    
    if {$retsock == "false"} {
        set strmsg "Check Relayboard port $bd_ptnum is available or not?"
        set strinfo "Socket: $retsock  (Relayboard port $bd_ptnum) isn't available."
        #Show warming msg
        Message::popup [list  "warning" $::pref(appname) $::VERSION $strmsg $strinfo]
        exit
    } else  {
        
        return $retsock
    }
}
proc read_socket {{timeout ""} {verbose ""}} {
    global sock_desc_modem telnet_line_modem
    
    if {$timeout == ""} {
        set _timeout 20000
    }
    if {$verbose == ""} {
        set _verbose off
    }
    set time_id [after $_timeout {set telnet_line_modem -1}]
    set telnet_line_modem ""
    fileevent $sock_desc_modem readable {
        fileevent $sock_desc_modem readable ""
        #poll until read telnet_line or timed out
        while { $telnet_line_modem == "" } {
            set telnet_line_modem [read $sock_desc_modem]
            update
        }
    }
    vwait ::telnet_line_modem
    if { $telnet_line_modem == -1 } {
        # set telnet_line "Timeout"; puts "$telnet_line"
        set result "Connection timed out."
    } else {
        after cancel $time_id
        if { $_verbose == "on" } {
            # puts "Recieved: $telnet_line"
        }
        set result $telnet_line_modem
    }
    update
    return $result
}
################################################################################
# Parse TrendChip modem socket dump 
################################################################################

namespace eval Parse_TCModem {
    variable verbose off
    variable logfile "../log/TCModem.log"
}
proc Parse_TCModem::GetInfoList {strmsg splitchars} {
    set list_strmsg [split $strmsg $splitchars]
    if {[expr [llength $list_strmsg] % 2] != 0} {
        lappend list_strmsg "!End$"
    }
    return $list_strmsg
}
proc Parse_TCModem::FilterInfoList {list_msg key} {
    variable verbose
    
    array set arr_retval $list_msg
    foreach index [lsort [array names arr_retval]] {
        if { [string last $key [string tolower $index]] > 0} {
            if {$verbose == "on"} {
                Func_INI::PntMsg "Index=$index; Value= $arr_retval($index)"
            }
            return [string trimleft $arr_retval($index) " "];#delete space
        };#if
    };#foreach index [lsort [array names arr_retval]]
}
proc Parse_TCModem::getvalue {listcnt backnum} {
    set value [lindex $listcnt [expr [llength $listcnt] - $backnum]]
    return $value
}
proc Parse_TCModem::parsefile {list_infnames strtoken backward} {
    variable verbose
    set list_temp {}
    
    foreach InFileName $list_infnames {
        set InputFileName [file tail  $InFileName]
        if {$verbose == "on"} {
            Func_INI::PntMsg "Input File is $InputFileName"
        }
        set rdinfd [open $InputFileName]
        set token $strtoken
        set str_temp ""
        
        while { [eof $rdinfd] != 1 } {
            gets $rdinfd line
            set test [string length $line]
            set token_loc [string first $token $line]
            
            if {$verbose == "on"} {
                Func_INI::PntMsg "$test; $token; $token_loc"
            }
            
            if { ($test > 0) && ($token_loc > 0)} {
                set records [split $line ": "]
                if {$verbose == "on"} {
                    Func_INI::PntMsg "line=$line"
                    Func_INI::PntMsg "record=$records"
                }
                
                switch -regexp $line {
                    {near|far} {
                        set value [getvalue $records $backward]
                        append str_temp $value ","
                    }
                    {(down|up)stream} {
                        set value [getvalue $records $backward]
                        append str_temp $value ","
                    }
                    {status} {
                        set value [getvalue $records $backward]
                        append str_temp $value
                    }
                    {(route|operational) mode} {
                        set value [getvalue $records $backward]
                        append str_temp $value
                    }
                    {TCM} {
                        set value [getvalue $records $backward]
                        append str_temp $value ","
                    }
                    {^%  MOS             =} {
                        set value [getvalue $records $backward]
                        append str_temp $value ","
                    }
                    {CG} {
                        set value [getvalue $records $backward]
                        append str_temp $value ","
                    }
                    {ROS} {
                        set value [getvalue $records $backward]
                        append str_temp $value ","
                    }
                    {(R|C)_MIN-PCB-DS} {
                        set value [getvalue $records $backward]
                        append str_temp $value ","
                    }
                    {\ FwVer\:} {
                        set value [getvalue $records $backward]
                        append str_temp $value ","
                    }
                    {RAS} {
                        set value [getvalue $records $backward]
                        append str_temp $value ","
                    }
                    {standar} {
                        set value [getvalue $records $backward]
                        append str_temp $value
                    }
                };#switch -regexp $line
                
            };###if { }
            
        };#while { [eof $rdinfd] != 1 }
        if [info exists str_total]  {
            unset str_total
        }
        append str_total $str_temp ;#"\n";#each file each row
        lappend list_temp $str_total
    };#foreach InputFileName $list_infnames
    if {$verbose == "on"} {
        foreach x $list_temp {
            Func_INI::PntMsg "$x"
        }
    };#if {$verbose == "on"}
    
    return $list_temp
}
proc Parse_TCModem::_GetInfo {str_sockdump getwhat} {
    variable verbose
    
    if {$str_sockdump == ""} {
        set strmsg "Input socket dump string is empty."
        Func_INI::PntMsg $strmsg
        exit
    }
    
    
    switch -regexp [string tolower $getwhat] {
        
        {ras ver} {
            set list_retval [GetInfoList $str_sockdump "\n:"]
            array set arr_retval $list_retval
            # if {$verbose == "on"} {
                # Log::parray arr_retval
            # }
            set str_return [FilterInfoList $list_retval "ras"]
            # split content from "|"
            set temp [split $str_return "|"]
            return [lindex $temp 0]
        }
        
        {dmt ver} {
            set list_retval [GetInfoList $str_sockdump ":H"]
            set str_return [FilterInfoList $list_retval "dmt"]
            return  $str_return
            
        }
        
        {adsl status} {
            set token "modem status"
            set list_temp [parsefile [list "sock.temp"] $token 1]
            return  $list_temp
        }
        
        {err fast} {
            set token "fast"
            set list_temp [parsefile [list "sock.temp"] $token 1]
            return  $list_temp
        }
        
        {err interleave} {
            set token "interleaved"
            set list_temp [parsefile [list "sock.temp"] $token 1]
            return  $list_temp
        }
        
        {noisemargin (near|far)} {
            set token "margin"
            set list_temp [parsefile [list "sock.temp"] $token 2]
            return  $list_temp
        }
        {chandata fast} {
            set token "fast"
            set list_temp [parsefile [list "sock.temp"] $token 2]
            return  $list_temp
        }
        {chandata interleave} {
            set token "interleaved"
            set list_temp [parsefile [list "sock.temp"] $token 2]
            return  $list_temp
        }
        {iproute} {
            set token "route mode"
            set list_temp [parsefile [list "sock.temp"] $token 1]
            return  $list_temp
        }
        {adsl opmode} {
            set token "ational mode"
            # set records [split "operational mode: ITU G.992.1(G.DMT)" ": "]
            #  lindex $records 4 ===> G.992.1(G.DMT)
            set list_temp [parsefile [list "sock.temp"] $token 1]
            return  $list_temp
        }
        {tcm} {
            set token "TCM"
            # set records [split "%  TCM             = 1" ": "]
            set list_temp [parsefile [list "sock.temp"] $token 1]
            return  $list_temp
        }
        {mos} {
            set token "MOS"
            # set records [split "%  MOS             = -256" ": "]
            set list_temp [parsefile [list "sock.temp"] $token 1]
            return  $list_temp
        }
        {cgmodcg} {
            set token "CG"
            # set records [split "%  CG= 3987" ": "]
            # set records [split "%  ModCG = 0" ": "]
            set list_temp [parsefile [list "sock.temp"] $token 1]
            return  $list_temp
        }
        {ros} {
            set token "ROS"
            set list_temp [parsefile [list "sock.temp"] $token 1]
            return  $list_temp
        }
        {(co|remote)_pcb} {
            set token "_MIN-PCB-DS"
            # set records [split "R_MIN-PCB-DS    = 33 dB" ": "]
            # lindex $records 5 ===> 33
            set list_temp [parsefile [list "sock.temp"] $token 2]
            return  $list_temp
        }
        {standard} {
            set token "DSL standard"
            set list_temp [parsefile [list "sock.temp"] $token 2]
            return  $list_temp
        }
        {near-end} {
            set token "near-end"
            set list_temp [parsefile [list "sock.temp"] $token 2]
            return  $list_temp
        }
        {far-end} {
            set token "far-end"
            set list_temp [parsefile [list "sock.temp"] $token 2]
            return  $list_temp
        }
        default {
        }
    };#switch -regexp $getwhat
    
}
;#wan adsl perfdata
################################################################################
# wan adsl perfdata =>
# near-end FEC error fast: 0
# near-end FEC error interleaved: 5945
# near-end CRC error fast: 0
# near-end CRC error interleaved: 5
# near-end HEC error fast: 0
# near-end HEC error interleaved: 97
# far-end FEC error fast: 0
# far-end FEC error interleaved: 161
# far-end CRC error fast: 0
# far-end CRC error interleaved: 0
# far-end HEC error fast: 0
# far-end HEC error interleaved: 0
# Error second in 15min           : 4
# Error second in 24hr            : 0
# Error second after power-up     : 0
# ADSL uptime     0:04:39
################################################################################
proc Parse_TCModem::_GetInfo_err-fast {sockchanel } {
    variable verbose
    variable logfile
    
    set strCmd "wan adsl perfdata"
    set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
    set strmsg [_GetInfo $strmsg "err fast"]
    if {$verbose == "on"} {
        set strlogmsg "Error NearEnd/FEC,CRC,HEC FarEnd/FEC,CRC,HEC in fast mode= $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    
    return $strmsg
}
proc Parse_TCModem::_GetInfo_err-interleave {sockchanel} {
    variable verbose
    variable logfile
    
    set strCmd "wan adsl perfdata"
    set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
    set strmsg [_GetInfo $strmsg "err interleave"]
    if {$verbose == "on"} {
        set strlogmsg "Error NearEnd/FEC,CRC,HEC FarEnd/FEC,CRC,HEC in interleaved mode= $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
}
################################################################################
# ras> w ad c
# DSL standard: ADSL2+ Mode
# near-end bit rate: 11932 kbps
# far-end bit rate: 1063 kbps
################################################################################
proc Parse_TCModem::ZyxelTI_GetInfo_chandata-near {sockchanel} {
    variable verbose
    variable logfile
    
    set strCmd "wan adsl c"
    set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
    set strmsg [_GetInfo $strmsg "near-end"]
    
    if {$verbose == "on"} {
        set strlogmsg "Channel NearEnd datarate = $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
}
proc Parse_TCModem::ZyxelTI_GetInfo_chandata-far {sockchanel} {
    variable verbose
    variable logfile
    
    set strCmd "wan adsl c"
    set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
    set strmsg [_GetInfo $strmsg "far-end"]
    
    if {$verbose == "on"} {
        set strlogmsg "Channel NearEnd datarate = $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
}
################################################################################
#wan adsl chandata =>
# near-end interleaved channel bit rate: 17646 kbps
# near-end fast channel bit rate: 0 kbps
# far-end interleaved channel bit rate: 999 kbps
# far-end fast channel bit rate: 0 kbps
################################################################################
    
proc Parse_TCModem::_GetInfo_chandata-fast {sockchanel} {
    variable verbose
    variable logfile
    
    set strCmd "wan adsl chandata"
    set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
    set strmsg [_GetInfo $strmsg "chandata fast"]
    if {$verbose == "on"} {
        set strlogmsg "Channel NearEnd; FarEnd datarate in fase mode= $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
}
proc Parse_TCModem::_GetInfo_chandata-interleave {sockchanel} {
    variable verbose
    variable logfile
    
    set strCmd "wan adsl chandata"
    set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
    set strmsg [_GetInfo $strmsg "chandata interleave"]
    if {$verbose == "on"} {
        set strlogmsg "Channel NearEnd; FarEnd datarate in interleaved mode= $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
}
;#wan adsl linedata near
################################################################################
# wan adsl linedata near =>
# relative capacity occupation: 100%
# noise margin downstream: 8.5 db
# output power upstream: 11.3 dbm
# attenuation downstream: 34.0 db
################################################################################
;#wan adsl linedata far
################################################################################
# wan adsl linedata far =>
# relative capacity occupation: 100%
# noise margin upstream: 11.9 db
# output power downstream: 18.3 dbm
# attenuation upstream: 22.7 db
################################################################################
proc Parse_TCModem::_GetInfo_NM-near {sockchanel} {
    variable verbose
    variable logfile
    
    set strCmd "wan adsl linedata near"
    set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
    set strmsg [_GetInfo $strmsg "noisemargin near"]
    if {$verbose == "on"} {
        set strlogmsg "Noise margin in near end= $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
}

proc Parse_TCModem::_GetInfo_NM-far {sockchanel} {
    variable verbose
    variable logfile
    
    set strCmd "wan adsl linedata far"
    set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
    set strmsg [_GetInfo $strmsg "noisemargin far"]
    if {$verbose == "on"} {
        set strlogmsg "Noise margin in far end= $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
}
proc Parse_TCModem::_GetInfo_adsl-status {sockchanel} {
    variable verbose
    variable logfile
    
    set strCmd "wan adsl status"
    set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
    set strmsg [_GetInfo $strmsg "adsl status"]
    if {$verbose == "on"} {
        set strlogmsg "Current modem status is $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
}
################################################################################
# tc> w dmt2 show dmt
#
# ============= DMT SW setting =============
# %  txfltr_gain      = 0x392d0
# %  txfltr_type     = 0x7
# %  AGC reverb vref1, vref2, vref3 = 470  470  470
# %  rxfltr_type     = 0x5
# %  Loop length     = 1
# %  pilot tone      = 323
# %  watch tone      = 324
# %  TCM             = 1
# %  One Bit flag    = 1
# %  TargetNoiseMarg = 3072
# %  Sync flag       = 1
# %  Sync position   = 0
# %  Sync offset     = 110
# %  echocancel flag = 0
# %  MOS             = -256
# %  Budget MOS      = 0
# %  shapefilterflag = 1
# %  AGC clip counter= 0
# %  Rate4Sel= 0
# %  Rs= 1
# %  CG= 3987
# %  OlrCom= 1
# %  ModCG = 0
# %  ROS = 0
# %  IntlSNRM= 768 , FastSNRM= -256
# %  force zero bit = 0
# %  ST SyncSym update = 0
# %  ST SyncSym Update Pattern = REVERB
#
# opt_param==> "TCM" "MOS" "CG" "ModCG" "ROS"
################################################################################
proc Parse_TCModem::_GetInfo_DMT-SW  {sockchanel opt_param} {
    variable verbose
    variable logfile
    
    set strCmd "wan dmt2 show dmt"
    set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
    set strmsg [_GetInfo $strmsg $opt_param]
    
    if {$verbose == "on"} {
        set strlogmsg "$opt_param of DMT SW is $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
}
################################################################################
# tc> w dmt2 show rpcb
#
# **********************
# R-MSG-PCB
# **********************
# R_MIN-PCB-DS    = 33 dB
# R_MIN-PCB-US    = 0 dB
# HOOK_STATUS     = 11
# Reserve1        = 00 (Should be 0)
# C-PILOT         = 323 tone
# Reserve2        = 00000 (Should be 0)
#
# tc> w dmt2 show cpcb
#
# ************
# C-MSG-PCB
# ****************
# C_MIN-PCB-DS    = 0 dB
# C_MIN-PCB-US    = 11 dB
# HOOK_STATUS     = 11
# Reserve1        = 00 (Should be 0)
################################################################################
proc Parse_TCModem::_GetInfo_MinPCB-DS {sockchanel opt_param} {
    variable verbose
    variable logfile
    
    switch -regexp $opt_param {
        {co_pcb} {
            set Cmdopt "c"
        }
        {remote_pcb} {
            set Cmdopt "r"
        }
    }
    ;#switch -regexp $opt_param
    
    append strCmd "wan dmt2 show " $Cmdopt "pcb"
    set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
    set strmsg [_GetInfo $strmsg $opt_param]
    
    if {$verbose == "on"} {
        set strlogmsg "Pwr cutback of  $opt_param side is $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
}
proc Parse_TCModem::ZyxelTI_GetInfo_adsl-OpMode {sockchanel} {
    variable verbose
    variable logfile
    
    set strCmd "wan adsl c"
    set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
    set strmsg [_GetInfo $strmsg "standard"]
    
    if {$verbose == "on"} {
        set strlogmsg "Zyxel_TI modem opmode is $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
}
proc Parse_TCModem::_GetInfo_DumpInfo_PassFail {strmsg} {
    if {[string length $strmsg] < 0} {
        return fail
    }
    ###8.5,  4.8,gdmt,   pass,  8000, 800,5,5
    ## end-7 end-6 end-5 end-4  end-3 end-2  end-1  end
    ####################
    set list_strmsg [split $strmsg ,]
    set str_passfail [lindex $list_strmsg end-4]
    
    return $str_passfail
}
################################################################################
# tc> w ad opm
# operational mode: ITU G.992.1(G.DMT)
################################################################################
proc Parse_TCModem::_GetInfo_adsl-OpMode {sockchanel} {
    variable verbose
    variable logfile
    
    set strCmd "wan adsl opmode"
    set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
    set strmsg [_GetInfo $strmsg "adsl opmode"]
    
    if {$verbose == "on"} {
        set strlogmsg "Current modem opmode is $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
}
;#sys version
################################################################################
# sys version =>
# RAS version: 2.7.0.26(UE0.B2C)3.5.10.0| 2006/4/6
# System   ID: 2.7.0.26(UE0.B2C)3.5.10.0[Apr 06 2006 16:39:21]
# romRasSize: 1097038
# system up time:     0:05:39 (8498 ticks)
# bootbase version: VTC1.08 | 5/27/2004
################################################################################
proc Parse_TCModem::_GetInfo_RAS-Ver {sockchanel} {
    set strCmd "sys version"
    set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
    set strmsg [_GetInfo $strmsg "ras ver"]
    Func_INI::PntMsg $strmsg
    
    return $strmsg
}
proc Parse_TCModem::_GetInfo_DMT-Ver {sockchanel} {
    set strCmd "wan adsl fwversion"
    set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
    set strmsg [_GetInfo $strmsg "dmt ver"]
    Func_INI::PntMsg $strmsg
    
    return $strmsg
}
proc Parse_TCModem::Get_Router_ver-ras_dmt {sockchannel passwd} {
    
    if [Func_Socket::IsConnected $sockchannel] {
        Func_Socket::Waitfor $sockchannel "*Passwor*"
        set strmsg [Func_Socket::SendCmd $sockchannel $passwd]
        # Func_INI::PntMsg $strmsg
        insertLogLine info $strmsg
        
        set ras_ver [Parse_TCModem::_GetInfo_RAS-Ver $sockchannel]
        set dmt_ver [Parse_TCModem::_GetInfo_DMT-Ver $sockchannel]
        set str_adslstatus [Parse_TCModem::_GetInfo_adsl-status $sockchannel]
        set str_iproute [Parse_TCModem::_ChkModem_IPRoute $sockchannel]
        
    };#if [Func_Socket::IsConnected $sock_desc_modem]
    
    set strmsg "DMTVer= $dmt_ver; RASVer= $ras_ver"
    return $strmsg
}
################################################################################
# tc> show cpe
# Hostname        = tc
# Message         = <empty>
# ip route mode   = No
# bridge mode     = Yes
################################################################################
proc Parse_TCModem::_GetInfo_IPRoute {sockchanel} {
    variable verbose
    variable logfile
    
    set strCmd "show cpe"
    set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
    set strmsg [_GetInfo $strmsg "iproute"]
    if {$verbose == "on"} {
        set strlogmsg "IP Route mode= $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
}
proc Parse_TCModem::_GetInfo_ADSLDiag {sockchanel} {
    variable verbose
    variable logfile
    
    set strCmd "wan adsl diag"
    set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
    if {$verbose == "on"} {
        set strlogmsg "WAN ADSL Diag= $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
}
proc Parse_TCModem::_SetDUT_IPRoute {sockchanel} {
    variable verbose
    variable logfile
    
    #Set WAN side static IP address
    set list_IProuteCmd [list "set cpe iproute on" \
            "wan node index 1" \
            "wan node encap enet" \
            "wan node wanip static 168.95.1.22" \
            "wan node remoteip 168.95.1.254 255.255.255.0" \
            "wan node bridge off" \
            "wan node routeip on" \
            "wan node nat sua" \
            "wan node save"]
    
    foreach strCmd $list_IProuteCmd {
        set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
        if {$verbose == "on"} {
            set strlogmsg "WAN setting = $strmsg"
            Func_INI::PntMsg $strlogmsg
            Log "info" $logfile [list $strlogmsg]
        }
    }
    
    return $strmsg
}

proc Parse_TCModem::_SetDUT_DSLOpMode {sockchanel opmode_option} {
    variable verbose
    variable logfile
    
    #Set DUT DSL Op mode {glite t1.413 gdmt multimode adsl2 adsl2plus}
    switch -regexp $opmode_option {
        {auto} {
            set opmode "multimode"
        }
        {gdmt} {
            set opmode "gdmt"
        }
        {glite} {
            set opmode "glite"
        }
        {t1413} {
            set opmode "t1.413"
        }
        {g992-5} {
            set opmode "adsl2plus"
        }
    }
    
    append strCmd "wan adsl opencmd " $opmode
    set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
    
    if {$verbose == "on"} {
        set strlogmsg "WAN adsl opmode = $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    };#{$verbose == "on"}
    
    return $strmsg
}

proc Parse_TCModem::_ChkModem_IPRoute {sockchanel} {
    variable verbose
    
    set str_iproute    [_GetInfo_IPRoute $sockchanel]
    
    # Check DUT if routing mode on, bridge mode off
    switch -regexp $str_iproute {
        {Yes} {
            set strmsg "routing mode"
            return $strmsg
        }
        
        {No} {
            set strmsg [_SetDUT_IPRoute $sockchanel]
            switch -regexp $strmsg {
                {save ok} {
                    if {$verbose == "on"} {
                        Func_INI::PntMsg "Setting Routing mode successful."
                    };#if {$verbose == "on"}
                    set strmsg "routing mode"
                    return $strmsg
                }
            };#switch -regexp $strmsg
        }
    };#switch -regexp $str_iproute
}

proc Parse_TCModem::_SetDUT_TCMOn {sockchanel} {
    variable verbose
    variable logfile
    
    set list_TCMCmd [list "wan dmt set tcm on" \
            "wan dmt2 set tcm on"]
    
    foreach strCmd $list_TCMCmd {
        set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
        if {$verbose == "on"} {
            set strlogmsg "TCM setting = $strmsg"
            Func_INI::PntMsg $strlogmsg
            Log "info" $logfile [list $strlogmsg]
        }
    }
    
    return $strmsg
}
proc Parse_TCModem::_SetDUT_Dbg {sockchanel op_opt} {
    variable verbose
    variable logfile
    
    set stropt [string tolower $op_opt]
    switch -regexp $stropt {
        {on} {
            set list_TCMCmd [list "wan rts dbg on"]
        }
        {off} {
            set list_TCMCmd [list "wan rts dbg off"]
        }
    };#switch -regexp $stropt
    
    foreach strCmd $list_TCMCmd {
        set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
        if {$verbose == "on"} {
            set strlogmsg "TCM setting = $strmsg"
            Func_INI::PntMsg $strlogmsg
            Log "info" $logfile [list $strlogmsg]
        }
    }
    
    return $strmsg
}
proc Parse_TCModem::_SetDUT_Sync {sockchanel op_opt list_value} {
    variable verbose
    variable logfile
    
    set stropt [string tolower $op_opt]
    switch -regexp $stropt {
        {on} {
            set list_TCMCmd [list "wan dmt2 set sync on \
                    [lindex $list_value 0] [lindex $list_value 1]"]
        }
        {off} {
            set list_TCMCmd [list "wan dmt2 set sync off \
                    [lindex $list_value 0] [lindex $list_value 1]"]
        }
    };#switch -regexp $stropt
    
    foreach strCmd $list_TCMCmd {
        set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
        if {$verbose == "on"} {
            set strlogmsg "TCM setting = $strmsg"
            Func_INI::PntMsg $strlogmsg
            Log "info" $logfile [list $strlogmsg]
        }
    }
    
    return $strmsg
}
proc Parse_TCModem::_SetDUT_Sysstdio {sockchanel timeout_durtaion} {
    variable verbose
    variable logfile
    
    set list_TCMCmd [list "sys stdio $timeout_durtaion"]
    
    foreach strCmd $list_TCMCmd {
        set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
        if {$verbose == "on"} {
            set strlogmsg "TCM setting = $strmsg"
            Func_INI::PntMsg $strlogmsg
            Log "info" $logfile [list $strlogmsg $strmsg]
        };#if {$verbose == "on"}
    };#foreach strCmd $list_TCMCmd
    
    return $strmsg
}
proc Parse_TCModem::_SetDUT_Reset {sockchanel} {
    variable verbose
    variable logfile
    
    set list_TCMCmd [list "wan adsl r"]
    
    foreach strCmd $list_TCMCmd {
        set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
        if {$verbose == "on"} {
            set strlogmsg "TCM setting = $strmsg"
            Func_INI::PntMsg $strlogmsg
            Log "info" $logfile [list $strlogmsg]
        }
    }
    
    return $strmsg
}
proc Parse_TCModem::_SetDUT_LargeD {sockchanel} {
    variable verbose
    variable logfile
    
    set list_TCMCmd [list "wan dmt2 db tlb 1a" "wan dmt2 set largeD 2"]
    # set list_TCMCmd [list "wan dmt2 db tlb 1a"]
    
    foreach strCmd $list_TCMCmd {
        set strmsg [Func_Socket::SendCmd $sockchanel $strCmd]
        lappend list_msg $strmsg
        if {$verbose == "on"} {
            set strlogmsg "TCM setting = $strmsg"
            Func_INI::PntMsg $strlogmsg
            Log "info" $logfile [list $strlogmsg]
        }
    }
    
    return $list_msg
}
################################################################################
#
################################################################################
proc Parse_TCModem::GenProCommSetupFile {cfgfilename  stropt {compt_num ""} {mdeom_passwd ""}} {
    set wroutfd [open $cfgfilename w]
    set filecnt ""
    
    ################################################################################
    # TC modem INI File name
    # 1,115200,N,8,1|1234
    #
    # TC modem rest cfg file
    # wan adsl reset ^M;
    #^M;^M;^M;^M;
    ################################################################################
    switch -regexp  [string tolower $stropt] {
        {inifile} {
            append filecnt $compt_num "," "115200," "N,8,1|" $mdeom_passwd
        }
        {cfgfile} {
            append filecnt "wan adsl reset ^M;"
            puts $wroutfd $filecnt
            
            if [info exists filecnt] { unset filecnt}
            append filecnt " ^M" ";" " ^M" ";" " ^M" ";"
        }
    };#switch -regext [string tolower $stropt]
    
    puts $wroutfd $filecnt
    close $wroutfd
    
    return true
}

proc Parse_TCModem::WanRest {tcmodem_inifile tcmodem_cfgfile tcmodem_dump} {
    set currpath [file normalize [file dirname [info script]]]
    
    Parse_TCModem::GenProCommSetupFile $tcmodem_inifile "inifile" "1" "1234"
    Parse_TCModem::GenProCommSetupFile $tcmodem_cfgfile "cfgfile" "" ""
    
    ##Kill Procomm process first
    DSLAM-Procomm::KillProcomm
    
    set co_wasfile "test_dslam.wax"
    set co_was [file join $currpath $co_wasfile]
    
    set str_progdir [DSLAM-Procomm::GetStrPath_ProgFile]
    
    set path_procomm [file join $str_progdir "Symantec" "Procomm Plus" "PROGRAMS"]
    set path_pw5 [file join $path_procomm "pw5.exe"]
    set retval [exec $path_pw5 $co_was $tcmodem_inifile $tcmodem_cfgfile $tcmodem_dump]
    
    return $retval
}
proc Parse_TCModem::DUT_InfoFile  {logfile csvfile loghead log} {
    Log::LogOut $loghead $logfile
    Log::LogOut $log $logfile
    Log::LogOut $log $csvfile
    
    return true
}
proc Parse_TCModem::DUT_DumpADSLDiag {logfname} {
    global sock_desc_modem
    
    set strmsg [Parse_TCModem::_GetInfo_ADSLDiag $sock_desc_modem]
    Log "info" $logfname [list $strmsg]
    
    set strmsg "Log out ADSL Diag Info"
    return $strmsg
}
######################################################################3
##Check test result
#
# ##ID,table,testprofile,looptype,looplength,taplength,noise,
#    0   1         2
# noise_Ch1,noise_Ch2,noise_Ch3,noise_Ch4,microgain_Ch1,microgain_Ch2,
# microgain_Ch3,microgain_Ch4,EnWhiteNoise_Ch1,EnWhiteNoise_Ch2,EnWhiteNoise_Ch3,
# EnWhiteNoise_Ch4,Std_DS_DataRate,Std_US_DataRate,Std_DS_NM,Std_US_NM
#
##########################################################################
proc Parse_TCModem::GetDUT_Datarate_FastInterleave {rowval str_chandata_fast \
            str_chandata_interleave} {
    
    set dslamprofile [lindex $rowval 2]
    switch -regexp [string tolower $dslamprofile] {
        {fast} {
            set list_dut_datarate [split $str_chandata_fast ,]
        }
        {_f_} {
            set list_dut_datarate [split $str_chandata_fast ,]
        }
        {interleave} {
            set list_dut_datarate [split $str_chandata_interleave ,]
        }
        {_(l|i)_} {
            set list_dut_datarate [split $str_chandata_interleave ,]
        }
    };#switch -regexp [string tolower $dslamprofile]
    
    return $list_dut_datarate
}
proc Parse_TCModem::DUT_DumpInfo {testcaseid logfname maxretry timeout {csvfname ""} stradsldiag_opt stradslreset_opt rowval} {
    global sock_desc_modem
    
    set log_path [file join   ".." "Log"]
    if {$csvfname == ""} { set csvfname [file join $log_path "chandata_err.csv"]}
    
    ##First time to dump TC modem
    ##############################
    if {[string tolower $stradslreset_opt] == "enable"} {
        set strmsg "wan adsl reset"
        insertLogLine info $strmsg
        Log "info" $logfname [list $strmsg]
        Parse_TCModem::_SetDUT_Reset $sock_desc_modem
    }
    
    set str_adslstatus [Parse_TCModem::_GetInfo_adsl-status $sock_desc_modem]
    set retrytimes 0
    
    while {$str_adslstatus != "up"} {
            incr retrytimes
            ##Check retry times to break
            if {$retrytimes > $maxretry} { break }
            
            set timeout_duration [expr $timeout * $retrytimes]
            set strmsg "Waiting $timeout_duration seconds that modem links up to dump info. \
                    Retry=$retrytimes times; Maxtimes=$maxretry"
            # Func_INI::PntMsg "Waiting $timeout seconds that modem links up"
            insertLogLine info $strmsg
            after [expr $timeout * 1000]; update
            set str_adslstatus [Parse_TCModem::_GetInfo_adsl-status $sock_desc_modem]
            
    };#while {$str_adslstatus != "up"}
    
    
    set looplen "$Func_CSV::LoopLen,$Func_CSV::BTLen"
    set time_tag [clock format [clock seconds] -format "%H:%M:%S %m%d%y"]
    append log_info $testcaseid "," $looplen "," $time_tag "," $Func_CSV::TestProfile ","
    #set log $log_csv
    
    if {$str_adslstatus == "up"} {
        
        set str_NM_near    [Parse_TCModem::_GetInfo_NM-near $sock_desc_modem]
        set str_NM_far    [Parse_TCModem::_GetInfo_NM-far $sock_desc_modem]
        
        set str_err_fast [Parse_TCModem::_GetInfo_err-fast $sock_desc_modem]
        set str_err_interleave [Parse_TCModem::_GetInfo_err-interleave $sock_desc_modem]
        set str_chandata_fast [Parse_TCModem::_GetInfo_chandata-fast $sock_desc_modem]
        set str_chandata_interleave [Parse_TCModem::_GetInfo_chandata-interleave $sock_desc_modem]
        set str_opmode_adsl [Parse_TCModem::_GetInfo_adsl-OpMode $sock_desc_modem]
        
        if [info exists loghead] { unset loghead }
        if [info exists log] {unset log}
        
        ##For reduce raw csv file to csv2xls log, check csvfname if exist
        ####################################################################
        if {[file exists $csvfname] == 0} {
            set loghead_info "TestCaseID, LoopLength, BTLength, TimeStamp, TestPorfile,"
            set loghead_fast "NearEnd-datarate,FarEnd-datarate,NearEndFEC,NearEndCRC,NearEndHEC,FarEndFEC,FarEndCRC,FarEndHEC_FastMode"
            set loghead_interleave "NearEnd-datarate,FarEnd-datarate,NearEndFEC,NearEndCRC,NearEndHEC,FarEndFEC,FarEndCRC,FarEndHEC_InterleavedMode"
            set loghead_NM "NearEnd_NM,FarEnd_NM"
            set loghead_other "ADSL_OpMode,"
            
            append loghead $loghead_info $loghead_fast "," $loghead_interleave $loghead_NM \
                    $loghead_other
        };#if {[file exists $csvfname] == 0}
        
        append log_up $log_info \
                $str_chandata_fast $str_err_fast $str_chandata_interleave \
                $str_err_interleave $str_NM_near $str_NM_far \
                $str_opmode_adsl
        
        ##############################
        ##Check test result
        #
        # ##ID,table,testprofile,looptype,looplength,taplength,noise,
        #    0   1         2
        # noise_Ch1,noise_Ch2,noise_Ch3,noise_Ch4,microgain_Ch1,microgain_Ch2,
        # microgain_Ch3,microgain_Ch4,EnWhiteNoise_Ch1,EnWhiteNoise_Ch2,EnWhiteNoise_Ch3,
        # EnWhiteNoise_Ch4,Std_DS_DataRate,Std_US_DataRate,Std_DS_NM,Std_US_NM
        #
        ##############################
        set dslamprofile [lindex $rowval 2]
        switch -regexp [string tolower $dslamprofile] {
            {fast} {
                set list_dut_datarate [split $str_chandata_fast ,]
            }
            {_f_} {
                set list_dut_datarate [split $str_chandata_fast ,]
            }
            {_f[0-9]db} {
                set list_dut_datarate [split $str_chandata_fast ,]
            }
            {_i[0-9]db} {
                set list_dut_datarate [split $str_chandata_interleave ,]
            }
            {interleave} {
                set list_dut_datarate [split $str_chandata_interleave ,]
            }
            {_i_} {
                set list_dut_datarate [split $str_chandata_interleave ,]
            }
            {^(a2|b2|au)} {
                set chandata_fast_ds          [lindex [split $str_chandata_fast ,] 0]
                set chandata_fast_us          [lindex [split $str_chandata_fast ,] 1]
                set chandata_interleave_ds    [lindex [split $str_chandata_interleave ,] 0]
                set chandata_interleave_us    [lindex [split $str_chandata_interleave ,] 1]
                set chandata_ds                [expr $chandata_fast_ds + $chandata_interleave_ds]
                set chandata_us                [expr $chandata_fast_us + $chandata_interleave_us]
                set list_dut_datarate         [list $chandata_ds $chandata_us]
            }
        };#switch -regexp [string tolower $Func_CSV::TestProfile]
        
        ## {_(l|i|i1\/2)_} {set list_dut_datarate [split $str_chandata_interleave ,]}
        
        set strresult [MiscCommon::Chk_TestResult $list_dut_datarate \
                $str_NM_near $str_NM_far $rowval]
        set std_ds_datarate [lindex $rowval end-3]
        set std_us_datarate [lindex $rowval end-2]
        set std_ds_NM        [lindex $rowval end-1]
        set std_us_NM        [lindex $rowval end]
        
        append log_up "," $strresult "," $std_ds_datarate \
                "," $std_us_datarate "," $std_ds_NM "," $std_us_NM
        
        # show log header on widget
        insertLogLine critical $loghead
        Parse_TCModem::DUT_InfoFile  $logfname $csvfname $loghead $log_up
        
        ##Log out ADSL Diag info
        ##############################
        switch -regexp [string tolower $stradsldiag_opt] {
            {enable} {
                append logfile_adsldiag $logfname "_adsldiag"
                set retmsg [Parse_TCModem::DUT_DumpADSLDiag $logfile_adsldiag]
                insertLogLine warn $retmsg
            }
            default {
            }
        };#switch -regexp [string tolower $stradsldiag_opt]
        
        return $log_up
    } else  {
        append log_down $log_info "False"
        Parse_TCModem::DUT_InfoFile  $logfname $csvfname "False" $log_down
        
        return $log_down
    }
    ;#if {$str_adslstatus == "up"}
}

################################################################################
# Parse TrendChip Pure Bridge modem COM port (from Procomm) dump
################################################################################

namespace eval Parse_TCPureBridge {
    variable verbose off
    variable logfile "../log/TCPureBridge.log"
    variable COMPt_Num 1
    variable list_TCModem_Cmdset { "wan adsl fw" \
                "wan adsl status" "wan adsl chandata" "wan adsl chandata" \
                "wan adsl perfdata" "wan adsl perfdata" "wan adsl linedata near" \
                "wan adsl linedata far"  "wan adsl opmode" "sys version"}
    
    variable list_TCModem_Token  { "dmt ver" \
                "adsl status" "chandata fast" "chandata interleave" \
                "err fast" "err interleave" "noisemargin near" \
                "noisemargin far" "adsl opmode" "ras ver" }
}
proc Parse_TCPureBridge::RunProComm {tcmodem_inifile tcmodem_cfgfile tcmodem_dump} {
    ##Kill Procomm process first
    DSLAM-Procomm::KillProcomm
    
    set currpath [file normalize [file dirname [info script]]]
    set co_wasfile "test_dslam.wax"
    set co_was [file join $currpath $co_wasfile]
    
    set str_progdir [DSLAM-Procomm::GetStrPath_ProgFile]
    
    set path_procomm [file join $str_progdir "Symantec" "Procomm Plus" "PROGRAMS"]
    set path_pw5 [file join $path_procomm "pw5.exe"]
    set retval [exec $path_pw5 $co_was $tcmodem_inifile $tcmodem_cfgfile $tcmodem_dump]
    
    return $retval
}
proc Parse_TCPureBridge::_GetInfo {str_sockdump str_logfname getwhat} {
    variable verbose
    
    if {$str_sockdump == ""} {
        set strmsg "Input socket dump string is empty."
        Func_INI::PntMsg $strmsg
        exit
    }
    
    
    switch -regexp [string tolower $getwhat] {
        
        {ras ver} {
            ##RAS F/W Version : 3.3.0.7(BE0.B1)3.1.0.6
            ##RAS F/W Version {} {} 3.3.0.7(BE0.B1)3.1.0.6
            set token "AS "
            set str_return [Parse_TCModem::parsefile [list $str_logfname] $token 4]
            return  $str_return
        }
        {dmt ver} {
            ##DMT FwVer: 3.5.20.85_B_TC3085 HwVer: T14F7_0.0
            ##DMT FwVer {} 3.5.20.85_B_TC3085 HwVer {} T14F7_0.0
            set token "FwVer"
            set str_return [Parse_TCModem::parsefile [list $str_logfname] $token 4]
            return  $str_return
            
        }
        {adsl status} {
            set token "modem status"
            set list_temp [Parse_TCModem::parsefile [list $str_logfname] $token 1]
            return  $list_temp
        }
        {err fast} {
            set token "fast"
            set list_temp [Parse_TCModem::parsefile [list $str_logfname] $token 1]
            return  $list_temp
        }
        {err interleave} {
            set token "interleaved"
            set list_temp [Parse_TCModem::parsefile [list $str_logfname] $token 1]
            return  $list_temp
        }
        {noisemargin (near|far)} {
            set token "margin"
            set list_temp [Parse_TCModem::parsefile [list $str_logfname] $token 2]
            return  $list_temp
        }
        {chandata fast} {
            set token "fast"
            set list_temp [Parse_TCModem::parsefile [list $str_logfname] $token 2]
            return  $list_temp
        }
        {chandata interleave} {
            set token "interleaved"
            set list_temp [Parse_TCModem::parsefile [list $str_logfname] $token 2]
            return  $list_temp
        }
        {iproute} {
            set token "route mode"
            set list_temp [Parse_TCModem::parsefile [list $str_logfname] $token 1]
            return  $list_temp
        }
        {adsl opmode} {
            set token "ational mode"
            # set records [split "operational mode: ITU G.992.1(G.DMT)" ": "]
            #  lindex $records 4 ===> G.992.1(G.DMT)
            set list_temp [Parse_TCModem::parsefile [list $str_logfname] $token 1]
            return  $list_temp
        }
        {tcm} {
            set token "TCM"
            # set records [split "%  TCM             = 1" ": "]
            set list_temp [Parse_TCModem::parsefile [list $str_logfname] $token 1]
            return  $list_temp
        }
        {mos} {
            set token "MOS"
            # set records [split "%  MOS             = -256" ": "]
            set list_temp [Parse_TCModem::parsefile [list $str_logfname] $token 1]
            return  $list_temp
        }
        {cgmodcg} {
            set token "CG"
            # set records [split "%  CG= 3987" ": "]
            # set records [split "%  ModCG = 0" ": "]
            set list_temp [Parse_TCModem::parsefile [list $str_logfname] $token 1]
            return  $list_temp
        }
        {ros} {
            set token "ROS"
            set list_temp [Parse_TCModem::parsefile [list $str_logfname] $token 1]
            return  $list_temp
        }
        {(co|remote)_pcb} {
            set token "_MIN-PCB-DS"
            # set records [split "R_MIN-PCB-DS    = 33 dB" ": "]
            # lindex $records 5 ===> 33
            set list_temp [Parse_TCModem::parsefile [list $str_logfname] $token 2]
            return  $list_temp
        }
        {standard} {
            set token "\ standard"
            set list_temp [Parse_TCModem::parsefile [list $str_logfname] $token 1]
            return  $list_temp
        }
        {near-end} {
            set token "near-end"
            set list_temp [Parse_TCModem::parsefile [list $str_logfname] $token 2]
            return  $list_temp
        }
        {far-end} {
            set token "far-end"
            set list_temp [Parse_TCModem::parsefile [list $str_logfname] $token 2]
            return  $list_temp
        }
        {chandata\ zynos} {
            set token "bit rate"
            set list_temp [Parse_TCModem::parsefile [list $str_logfname] $token 2]
            return  $list_temp
        }
        default {
        }
    };#switch -regexp $getwhat
    
}
proc Parse_TCPureBridge::GenProCommSetupFile {cfgfilename  stropt {compt_num ""} {mdeom_passwd ""}} {
    variable list_TCModem_Cmdset
    
    set wroutfd [open $cfgfilename w]
    set filecnt ""
    
    ################################################################################
    # TC modem INI File name
    # 1,115200,N,8,1|1234
    #
    # TC modem rest cfg file
    # wan adsl reset ^M;
    #^M;^M;^M;^M;
    ################################################################################
    if { [string tolower $stropt] == "inifile"} {
        append filecnt $compt_num "," "115200," "N,8,1|" $mdeom_passwd
        puts $wroutfd $filecnt
        close $wroutfd
        
        return true
    } elseif {[string tolower $stropt] == "zynos"} {
        append filecnt "24 ^M;"
        puts $wroutfd $filecnt
        if [info exists filecnt] { unset filecnt}
        append filecnt "8 ^M" ";" "24 ^M" ";" "8 ^M" ";"
        
        puts $wroutfd $filecnt
        close $wroutfd
        
        return true
    }
    
    switch -regexp  [string tolower $stropt] {
        {cfgfile} {
            append filecnt "wan adsl reset ^M;"
        }
        {wan_adsl_fw} {
            append filecnt [lindex $list_TCModem_Cmdset 0] " ^M;"
        }
        {wan_adsl_status} {
            append filecnt [lindex $list_TCModem_Cmdset 1] " ^M;"
        }
        {wan_adsl_chandata} {
            append filecnt [lindex $list_TCModem_Cmdset 2] " ^M;"
        }
        {wan_adsl_perfdata} {
            append filecnt [lindex $list_TCModem_Cmdset 4] " ^M;"
        }
        {wan_adsl_linedata_near} {
            append filecnt [lindex $list_TCModem_Cmdset 6] " ^M;"
        }
        {wan_adsl_linedata_far} {
            append filecnt [lindex $list_TCModem_Cmdset 7] " ^M;"
        }
        {wan_adsl_opmode} {
            append filecnt [lindex $list_TCModem_Cmdset 8] " ^M;"
        }
        {sys_version} {
            append filecnt [lindex $list_TCModem_Cmdset 9] " ^M;"
        }
    };#switch -regext [string tolower $stropt]
    
    puts $wroutfd $filecnt
    if [info exists filecnt] { unset filecnt}
    append filecnt " ^M" ";" " ^M" ";" " ^M" ";"
    
    puts $wroutfd $filecnt
    close $wroutfd
    
    return true
}
proc Parse_TCPureBridge::_GetInfo_ProComm {tcmodem_inifile tcmodem_cfgfile \
            tcmodem_dump strop_opt} {
    variable COMPt_Num
    
    Parse_TCPureBridge::GenProCommSetupFile $tcmodem_inifile "inifile" $COMPt_Num "1234"
    Parse_TCPureBridge::GenProCommSetupFile $tcmodem_cfgfile $strop_opt "" ""
    
    RunProComm $tcmodem_inifile $tcmodem_cfgfile $tcmodem_dump
    
    return true
}
proc Parse_TCPureBridge::_GetInfo_DMT-Ver {tcmodem_inifile tcmodem_cfgfile tcmodem_dump} {
    variable verbose
    variable logfile
    variable list_TCModem_Cmdset
    variable list_TCModem_Token
    
    set strCmd [lindex $list_TCModem_Cmdset 0];#"wan adsl fw"
    set strToken [lindex $list_TCModem_Token 0]
    
    _GetInfo_ProComm $tcmodem_inifile $tcmodem_cfgfile $tcmodem_dump "wan_adsl_fw"
    
    set strmsg "not null";#not empty string
    set strmsg [_GetInfo $strmsg $tcmodem_dump $strToken]
    Func_INI::PntMsg $strmsg
    
    if {$verbose == "on"} {
        set strlogmsg "$strCmd = $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
}

proc Parse_TCPureBridge::_GetInfo_adsl-status {tcmodem_inifile tcmodem_cfgfile tcmodem_dump} {
    variable verbose
    variable logfile
    variable list_TCModem_Cmdset
    variable list_TCModem_Token
    
    set strCmd [lindex $list_TCModem_Cmdset 1];#set strCmd "wan adsl status"
    set strToken [lindex $list_TCModem_Token 1]
    
    _GetInfo_ProComm $tcmodem_inifile $tcmodem_cfgfile $tcmodem_dump "wan_adsl_status"
    
    set strmsg "not null";#not empty string
    set strmsg [_GetInfo $strmsg $tcmodem_dump $strToken]
    if {$verbose == "on"} {
        set strlogmsg "Current modem status is $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
}
proc Parse_TCPureBridge::_GetInfo_chandata-fast {tcmodem_inifile tcmodem_cfgfile tcmodem_dump} {
    variable verbose
    variable logfile
    variable list_TCModem_Cmdset
    variable list_TCModem_Token
    
    # set strCmd [lindex $list_TCModem_Cmdset 2];#set strCmd "wan adsl chandata"
    set strToken [lindex $list_TCModem_Token 2]
    
    _GetInfo_ProComm $tcmodem_inifile $tcmodem_cfgfile $tcmodem_dump "wan_adsl_chandata"
    
    set strmsg "not null";#not empty string
    set strmsg [_GetInfo $strmsg $tcmodem_dump $strToken]
    if {$verbose == "on"} {
        set strlogmsg "Channel NearEnd; FarEnd datarate in fase mode= $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
}
proc Parse_TCPureBridge::_GetInfo_chandata-interleave {tcmodem_inifile tcmodem_cfgfile tcmodem_dump} {
    variable verbose
    variable logfile
    variable list_TCModem_Token
    
    ##set strCmd "wan adsl chandata"
    set strToken [lindex $list_TCModem_Token 3]
    
    ##Cause Parse_TCPureBridge::_GetInfo_chandata-fast already invoke
    # _GetInfo_ProComm $tcmodem_inifile $tcmodem_cfgfile $tcmodem_dump "wan_adsl_chandata"
    
    set strmsg "not null";#not empty string
    set strmsg [_GetInfo $strmsg $tcmodem_dump $strToken]
    if {$verbose == "on"} {
        set strlogmsg "Channel NearEnd; FarEnd datarate in interleaved mode= $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
}

proc Parse_TCPureBridge::_GetInfo_err-fast {tcmodem_inifile tcmodem_cfgfile tcmodem_dump} {
    variable verbose
    variable logfile
    variable list_TCModem_Token
    
    ##set strCmd "wan adsl perfdata"
    set strToken [lindex $list_TCModem_Token 4]
    
    _GetInfo_ProComm $tcmodem_inifile $tcmodem_cfgfile $tcmodem_dump "wan_adsl_perfdata"
    
    set strmsg "not null";#not empty string
    set strmsg [_GetInfo $strmsg $tcmodem_dump $strToken]
    if {$verbose == "on"} {
        set strlogmsg "Error NearEnd/FEC,CRC,HEC FarEnd/FEC,CRC,HEC in fast mode= $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
}
proc Parse_TCPureBridge::_GetInfo_err-interleave {tcmodem_inifile tcmodem_cfgfile tcmodem_dump} {
    variable verbose
    variable logfile
    variable list_TCModem_Token
    
    ##set strCmd "wan adsl perfdata"
    set strToken [lindex $list_TCModem_Token 5]
    
    ##Cause Parse_TCPureBridge::_GetInfo_err-fast already invoke
    # _GetInfo_ProComm $tcmodem_inifile $tcmodem_cfgfile $tcmodem_dump "wan_adsl_perfdata"
    
    set strmsg "not null";#not empty string
    set strmsg [_GetInfo $strmsg $tcmodem_dump $strToken]
    if {$verbose == "on"} {
        set strlogmsg "Error NearEnd/FEC,CRC,HEC FarEnd/FEC,CRC,HEC in interleaved mode= $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
}
proc Parse_TCPureBridge::_GetInfo_NM-near {tcmodem_inifile tcmodem_cfgfile tcmodem_dump} {
    variable verbose
    variable logfile
    variable list_TCModem_Token
    
    # set strCmd "wan adsl linedata near"
    set strToken [lindex $list_TCModem_Token 6]
    
    _GetInfo_ProComm $tcmodem_inifile $tcmodem_cfgfile $tcmodem_dump "wan_adsl_linedata_near"
    
    set strmsg "not null";#not empty string
    set strmsg [_GetInfo $strmsg $tcmodem_dump $strToken]
    if {$verbose == "on"} {
        set strlogmsg "Noise margin in near end= $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
}
proc Parse_TCPureBridge::_GetInfo_NM-far {tcmodem_inifile tcmodem_cfgfile tcmodem_dump} {
    variable verbose
    variable logfile
    variable list_TCModem_Token
    
    # set strCmd "wan adsl linedata far"
    set strToken [lindex $list_TCModem_Token 7]
    
    _GetInfo_ProComm $tcmodem_inifile $tcmodem_cfgfile $tcmodem_dump "wan_adsl_linedata_far"
    
    set strmsg "not null";#not empty string
    set strmsg [_GetInfo $strmsg $tcmodem_dump $strToken]
    if {$verbose == "on"} {
        set strlogmsg "Noise margin in far end= $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
}

proc Parse_TCPureBridge::_GetInfo_adsl-OpMode {tcmodem_inifile tcmodem_cfgfile tcmodem_dump} {
    
}
proc Parse_TCPureBridge::_GetInfo_RAS-Ver {tcmodem_inifile tcmodem_cfgfile tcmodem_dump} {
    variable verbose
    variable logfile
    variable list_TCModem_Token
    
    # set strCmd "sys version"
    set strToken [lindex $list_TCModem_Token 9]
    
    _GetInfo_ProComm $tcmodem_inifile $tcmodem_cfgfile $tcmodem_dump "sys_version"
    
    set strmsg "not null";#not empty string
    set strmsg [_GetInfo $strmsg $tcmodem_dump $strToken]
    Func_INI::PntMsg $strmsg
    
    if {$verbose == "on"} {
        set strlogmsg "Current RAS version is $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
}
proc Parse_TCPureBridge::ZyNOS_GetInfo_chandata {tcmodem_inifile tcmodem_cfgfile tcmodem_dump} {
    variable verbose
    variable logfile
    variable list_TCModem_Cmdset
    variable list_TCModem_Token
    
    # set strCmd [lindex $list_TCModem_Cmdset 2];#set strCmd "wan adsl chandata"
    set strToken "chandata zynos";#[lindex $list_TCModem_Token 2]
    
    _GetInfo_ProComm $tcmodem_inifile $tcmodem_cfgfile $tcmodem_dump "wan_adsl_chandata"
    
    set strmsg "not null";#not empty string
    set strmsg [_GetInfo $strmsg $tcmodem_dump $strToken]
    if {$verbose == "on"} {
        set strlogmsg "Channel NearEnd; FarEnd datarate in fase mode= $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
    
}
proc Parse_TCPureBridge::ZyNOS_GetInfo_adsl-OpMode {tcmodem_inifile tcmodem_cfgfile tcmodem_dump} {
    variable verbose
    variable logfile
    variable list_TCModem_Token
    
    # set strCmd "wan adsl opmode"
    set strToken "standard";#[lindex $list_TCModem_Token 8]
    
    ## _GetInfo_ProComm $tcmodem_inifile $tcmodem_cfgfile $tcmodem_dump "wan_adsl_opmode"
    
    set strmsg "not null";#not empty string
    set strmsg [_GetInfo $strmsg $tcmodem_dump $strToken]
    if {$verbose == "on"} {
        set strlogmsg "Current modem opmode is $strmsg"
        Func_INI::PntMsg $strlogmsg
        Log "info" $logfile [list $strlogmsg]
    }
    
    return $strmsg
    
}
proc Parse_TCPureBridge::ZyNOS_DumpInfo {testcaseid logfname maxretry timeout \
            {csvfname ""} stradsldiag_opt stradslreset_opt rowval} {
    
    set log_path [file join   ".." "Log"]
    if {$csvfname == ""} { set csvfname [file join $log_path "chandata_err.csv"]}
    
    
    ###Waiting 60 second error second
    if {[string tolower $stradslreset_opt] == "enable"} {
        set strmsg "wan adsl reset"
        insertLogLine info $strmsg
        Log "info" $logfile [list $strmsg]
        
        Parse_TCModem::WanRest $tcmodem_inifile $tcmodem_cfgfile $tcmodem_dump
    };#if {[string tolower $stradslreset_opt] == "enable"}
    
    set tcmodem_inifile  "tcmodem.ini"
    set tcmodem_cfgfile "tcmodemCfg.ini"
    set tcmodem_dump    "tcmodem.log"
    set str_adslstatus [Parse_TCPureBridge::_GetInfo_adsl-status $tcmodem_inifile \
            $tcmodem_cfgfile $tcmodem_dump]
    set retrytimes 0
    
    while {$str_adslstatus != "up"} {
        incr retrytimes
        ##Check retry times to break
        if {$retrytimes > $maxretry} { break }
        
        set timeout_duration [expr $timeout * $retrytimes]
        set strmsg "Waiting $timeout_duration seconds that modem links up to dump info. \
                Retry=$retrytimes times; Maxtimes=$maxretry"
        # Func_INI::PntMsg "Waiting $timeout seconds that modem links up"
        insertLogLine info $strmsg
        after [expr $timeout * 1000]; update
        set str_adslstatus [Parse_TCPureBridge::_GetInfo_adsl-status $tcmodem_inifile \
                $tcmodem_cfgfile $tcmodem_dump]
    };#while {$str_adslstatus != "up"}
    
    set looplen "$Func_CSV::LoopLen,$Func_CSV::BTLen"
    set time_tag [clock format [clock seconds] -format "%H:%M:%S %m%d%y"]
    append log_info $testcaseid "," $looplen "," $time_tag "," $Func_CSV::TestProfile ","
    
    if {$str_adslstatus == "up"} {
        
        set str_NM_near  [Parse_TCPureBridge::_GetInfo_NM-near $tcmodem_inifile \
                $tcmodem_cfgfile $tcmodem_dump]
        set str_NM_far   [Parse_TCPureBridge::_GetInfo_NM-far $tcmodem_inifile \
                $tcmodem_cfgfile $tcmodem_dump]
        set str_err_fast [Parse_TCPureBridge::_GetInfo_err-fast $tcmodem_inifile \
                $tcmodem_cfgfile $tcmodem_dump]
        set str_err_interleave [Parse_TCPureBridge::_GetInfo_err-interleave $tcmodem_inifile \
                $tcmodem_cfgfile $tcmodem_dump]
        ###################################################
        ##P660HW-T7> w ad ch
        ##DSL standard: ADSL2PLUS
        ##near-end bit rate: 17364 kbps
        ##far-end bit rate: 1128 kbps
        ##
        ##No fast or interleaved mode  
        ####################################################        
        set str_chandata [Parse_TCPureBridge::ZyNOS_GetInfo_chandata $tcmodem_inifile \
                $tcmodem_cfgfile $tcmodem_dump]
        set str_chandata_zero "0,0," ;
        #############################################################
        ##Base on DSLAM profile to set fast or interleaved datarate
        #############################################################
        set dslamprofile [lindex $rowval 2]
        switch -regexp [string tolower $dslamprofile] {
            {fast} {
                set str_chandata_fast        $str_chandata
                set str_chandata_interleave  $str_chandata_zero
            }
            {_f_} {
                set str_chandata_fast        $str_chandata
                set str_chandata_interleave    $str_chandata_zero
            }
            {interleave} {
                set str_chandata_fast          $str_chandata_zero
                set str_chandata_interleave    $str_chandata
            }
            {_i_} {
                set list_dut_datarate [split $str_chandata_interleave ,]
            }
            {^(a2|b2|au)} {
                set chandata_fast_ds          [lindex [split $str_chandata_fast ,] 0]
                set chandata_fast_us          [lindex [split $str_chandata_fast ,] 1]
                set chandata_interleave_ds    [lindex [split $str_chandata_interleave ,] 0]
                set chandata_interleave_us    [lindex [split $str_chandata_interleave ,] 1]
                set chandata_ds                [expr $chandata_fast_ds + $chandata_interleave_ds]
                set chandata_us                [expr $chandata_fast_us + $chandata_interleave_us]
                set list_dut_datarate         [list $chandata_ds $chandata_us]
            }
        };#switch -regexp [string tolower $Func_CSV::TestProfile]
        
        set str_opmode_adsl [Parse_TCPureBridge::ZyNOS_GetInfo_adsl-OpMode $tcmodem_inifile \
                $tcmodem_cfgfile $tcmodem_dump]
        
        if [info exists loghead] { unset loghead }
        if [info exists log] {unset log}
        
        ##For reduce raw csv file to csv2xls log, check csvfname if exist
        ####################################################################
        if {[file exists $csvfname] == 0} {
            set loghead_info "TestCaseID, LoopLength, BTLength, TimeStamp, TestPorfile,"
            set loghead_fast "NearEnd-datarate,FarEnd-datarate,NearEndFEC,NearEndCRC,NearEndHEC,FarEndFEC,FarEndCRC,FarEndHEC_FastMode"
            set loghead_interleave "NearEnd-datarate,FarEnd-datarate,NearEndFEC,NearEndCRC,NearEndHEC,FarEndFEC,FarEndCRC,FarEndHEC_InterleavedMode"
            set loghead_NM "NearEnd_NM,FarEnd_NM"
            set loghead_other "ADSL_OpMode,"
            
            append loghead $loghead_info $loghead_fast "," $loghead_interleave $loghead_NM \
                    $loghead_other
        };#if {[file exists $csvfname] == 0}
        
        append log_up $log_info \
                $str_chandata_fast $str_err_fast $str_chandata_interleave \
                $str_err_interleave $str_NM_near $str_NM_far \
                $str_opmode_adsl
        
        ##############################
        ##Check test result
        #
        # ##ID,table,testprofile,looptype,looplength,taplength,noise,
        #    0   1         2
        # noise_Ch1,noise_Ch2,noise_Ch3,noise_Ch4,microgain_Ch1,microgain_Ch2,
        # microgain_Ch3,microgain_Ch4,EnWhiteNoise_Ch1,EnWhiteNoise_Ch2,EnWhiteNoise_Ch3,
        # EnWhiteNoise_Ch4,Std_DS_DataRate,Std_US_DataRate,Std_DS_NM,Std_US_NM
        #
        ##############################
        set dslamprofile [lindex $rowval 2]
        switch -regexp [string tolower $dslamprofile] {
            {fast} {
                set list_dut_datarate [split $str_chandata_fast ,]
            }
            {_f_} {
                set list_dut_datarate [split $str_chandata_fast ,]
            }
            {_f[0-9]db} {
                set list_dut_datarate [split $str_chandata_fast ,]
            }
            {_i[0-9]db} {
                set list_dut_datarate [split $str_chandata_interleave ,]
            }
            {interleave} {
                set list_dut_datarate [split $str_chandata_interleave ,]
            }
            {_(l|i|i1\/2)_} {
                set list_dut_datarate [split $str_chandata_interleave ,]
            }
        };#switch -regexp [string tolower $Func_CSV::TestProfile]
        
        set strresult [MiscCommon::Chk_TestResult $list_dut_datarate \
                $str_NM_near $str_NM_far $rowval]
        set std_ds_datarate [lindex $rowval end-3]
        set std_us_datarate [lindex $rowval end-2]
        set std_ds_NM        [lindex $rowval end-1]
        set std_us_NM        [lindex $rowval end]
        
        append log_up "," $strresult "," $std_ds_datarate \
                "," $std_us_datarate "," $std_ds_NM "," $std_us_NM
        
        # show log header on widget
        insertLogLine critical $loghead
        Parse_TCModem::DUT_InfoFile  $logfname $csvfname $loghead $log_up
        
        ##Not log out ADSL Diag info, cause usting ProComm via COM port get info
        #########################################################################
        
        return $log_up
    } else  {
        append log_down $log_info "False"
        DUT_InfoFile  $logfname $csvfname "False" $log_down
        
        return $log_down
    };#if {$str_adslstatus == "up"}
            
}
proc Parse_TCPureBridge::ZyNOS_Init {tcmodem_inifile tcmodem_cfgfile tcmodem_dump} {
    variable verbose
    variable logfile
    
    _GetInfo_ProComm $tcmodem_inifile $tcmodem_cfgfile $tcmodem_dump "zynos"
    
    return true
}
proc Parse_TCPureBridge::DUT_Init {tcmodem_inifile tcmodem_cfgfile tcmodem_dump} {
    
    set dmt_ver [_GetInfo_DMT-Ver $tcmodem_inifile $tcmodem_cfgfile $tcmodem_dump]
    set ras_ver [_GetInfo_RAS-Ver $tcmodem_inifile $tcmodem_cfgfile $tcmodem_dump]
    
    set strmsg "DMTVer= $dmt_ver; RASVer= $ras_ver"
    return $strmsg
}
proc Parse_TCPureBridge::DUT_DumpInfo_PureBridge {testcaseid logfname maxretry timeout \
            {csvfname ""} stradsldiag_opt stradslreset_opt rowval} {
    
    set log_path [file join   ".." "Log"]
    if {$csvfname == ""} { set csvfname [file join $log_path "chandata_err.csv"]}
    set tcmodem_inifile  "tcmodem.ini"
    set tcmodem_cfgfile "tcmodemCfg.ini"
    set tcmodem_dump    "tcmodem.log"
    
    ##First time to dump TC modem
    ##############################
    if {[string tolower $stradslreset_opt] == "enable"} {
        set strmsg "wan adsl reset"
        insertLogLine info $strmsg
        Log "info" $logfname [list $strmsg]
        
        Parse_TCModem::WanRest $tcmodem_inifile $tcmodem_cfgfile $tcmodem_dump
    }
    
    set str_adslstatus [Parse_TCPureBridge::_GetInfo_adsl-status $tcmodem_inifile \
            $tcmodem_cfgfile $tcmodem_dump]
    set retrytimes 0
    
    while {$str_adslstatus != "up"} {
        incr retrytimes
        ##Check retry times to break
        if {$retrytimes > $maxretry} { break }
                
        set timeout_duration [expr $timeout * $retrytimes]
        set strmsg "Waiting $timeout_duration seconds that modem links up to dump info. \
                    Retry=$retrytimes times; Maxtimes=$maxretry"
        # Func_INI::PntMsg "Waiting $timeout seconds that modem links up"
        insertLogLine info $strmsg
        after [expr $timeout * 1000]; update
        set str_adslstatus [Parse_TCPureBridge::_GetInfo_adsl-status $tcmodem_inifile \
                    $tcmodem_cfgfile $tcmodem_dump]
    };#while {$str_adslstatus != "up"}
    
    set looplen "$Func_CSV::LoopLen,$Func_CSV::BTLen"
    set time_tag [clock format [clock seconds] -format "%H:%M:%S %m%d%y"]
    append log_info $testcaseid "," $looplen "," $time_tag "," $Func_CSV::TestProfile ","
    
    if {$str_adslstatus == "up"} {
        
        set str_NM_near  [Parse_TCPureBridge::_GetInfo_NM-near $tcmodem_inifile \
                $tcmodem_cfgfile $tcmodem_dump]
        set str_NM_far   [Parse_TCPureBridge::_GetInfo_NM-far $tcmodem_inifile \
                $tcmodem_cfgfile $tcmodem_dump]
        set str_err_fast [Parse_TCPureBridge::_GetInfo_err-fast $tcmodem_inifile \
                $tcmodem_cfgfile $tcmodem_dump]
        set str_err_interleave [Parse_TCPureBridge::_GetInfo_err-interleave $tcmodem_inifile \
                $tcmodem_cfgfile $tcmodem_dump]
        set str_chandata_fast [Parse_TCPureBridge::_GetInfo_chandata-fast $tcmodem_inifile \
                $tcmodem_cfgfile $tcmodem_dump]
        set str_chandata_interleave [Parse_TCPureBridge::_GetInfo_chandata-interleave $tcmodem_inifile \
                $tcmodem_cfgfile $tcmodem_dump]
        set str_opmode_adsl [Parse_TCPureBridge::_GetInfo_adsl-OpMode $tcmodem_inifile \
                $tcmodem_cfgfile $tcmodem_dump]
        
        if [info exists loghead] { unset loghead }
        if [info exists log] {unset log}
        
        ##For reduce raw csv file to csv2xls log, check csvfname if exist
        ####################################################################
        if {[file exists $csvfname] == 0} {
            set loghead_info "TestCaseID, LoopLength, BTLength, TimeStamp, TestPorfile,"
            set loghead_fast "NearEnd-datarate,FarEnd-datarate,NearEndFEC,NearEndCRC,NearEndHEC,FarEndFEC,FarEndCRC,FarEndHEC_FastMode"
            set loghead_interleave "NearEnd-datarate,FarEnd-datarate,NearEndFEC,NearEndCRC,NearEndHEC,FarEndFEC,FarEndCRC,FarEndHEC_InterleavedMode"
            set loghead_NM "NearEnd_NM,FarEnd_NM"
            set loghead_other "ADSL_OpMode,"
            
            append loghead $loghead_info $loghead_fast "," $loghead_interleave $loghead_NM \
                    $loghead_other
        };#if {[file exists $csvfname] == 0}
        
        append log_up $log_info \
                $str_chandata_fast $str_err_fast $str_chandata_interleave \
                $str_err_interleave $str_NM_near $str_NM_far \
                $str_opmode_adsl
        
        ##############################
        ##Check test result
        #
        # ##ID,table,testprofile,looptype,looplength,taplength,noise,
        #    0   1         2
        # noise_Ch1,noise_Ch2,noise_Ch3,noise_Ch4,microgain_Ch1,microgain_Ch2,
        # microgain_Ch3,microgain_Ch4,EnWhiteNoise_Ch1,EnWhiteNoise_Ch2,EnWhiteNoise_Ch3,
        # EnWhiteNoise_Ch4,Std_DS_DataRate,Std_US_DataRate,Std_DS_NM,Std_US_NM
        #
        ##############################
        set dslamprofile [lindex $rowval 2]
        switch -regexp [string tolower $dslamprofile] {
            {fast} {
                set list_dut_datarate [split $str_chandata_fast ,]
            }
            {_f_} {
                set list_dut_datarate [split $str_chandata_fast ,]
            }
            {_f[0-9]db} {
                set list_dut_datarate [split $str_chandata_fast ,]
            }
            {_i[0-9]db} {
                set list_dut_datarate [split $str_chandata_interleave ,]
            }
            {interleave} {
                set list_dut_datarate [split $str_chandata_interleave ,]
            }
            {_i_} {
                set list_dut_datarate [split $str_chandata_interleave ,]
            }
            {^(a2|b2|au)} {
                set chandata_fast_ds          [lindex [split $str_chandata_fast ,] 0]
                set chandata_fast_us          [lindex [split $str_chandata_fast ,] 1]
                set chandata_interleave_ds    [lindex [split $str_chandata_interleave ,] 0]
                set chandata_interleave_us    [lindex [split $str_chandata_interleave ,] 1]
                set chandata_ds                [expr $chandata_fast_ds + $chandata_interleave_ds]
                set chandata_us                [expr $chandata_fast_us + $chandata_interleave_us]
                set list_dut_datarate         [list $chandata_ds $chandata_us]
            }
        };#switch -regexp [string tolower $Func_CSV::TestProfile]
        
        set strresult [MiscCommon::Chk_TestResult $list_dut_datarate \
                $str_NM_near $str_NM_far $rowval]
        set std_ds_datarate [lindex $rowval end-3]
        set std_us_datarate [lindex $rowval end-2]
        set std_ds_NM        [lindex $rowval end-1]
        set std_us_NM        [lindex $rowval end]
        
        append log_up "," $strresult "," $std_ds_datarate \
                "," $std_us_datarate "," $std_ds_NM "," $std_us_NM
        
        # show log header on widget
        insertLogLine critical $loghead
        Parse_TCModem::DUT_InfoFile  $logfname $csvfname $loghead $log_up
        
        ##Not log out ADSL Diag info, cause usting ProComm via COM port get info
        #########################################################################
        
        return $log_up
    } else  {
        append log_down $log_info "False"
        DUT_InfoFile  $logfname $csvfname "False" $log_down
        
        return $log_down
    };#if {$str_adslstatus == "up"}
}
################################################################################
# Ttk MessageBox
# wrapper of ttk::dialog for using it like tk_messageBox
################################################################################
namespace eval Message {} {;#<<<
    variable retval {}
}

proc Message::msgset {btn} {
    variable retval $btn
}

# return selected button name
proc Message::show {args} {
    variable retval
    if [winfo exists .ttkmessage] {
        destroy .ttkmessage
    }
    eval ttk::dialog .ttkmessage $args -command {Message::msgset}
    wm withdraw .ttkmessage
    lower .ttkmessage .
    update
    if {[tk windowingsystem] eq "x11"} {
        ::Util::moveCenter .ttkmessage
    } else {
        ::Util::moveCenter .ttkmessage \
                [list [winfo reqwidth .ttkmessage] [winfo reqheight .ttkmessage]]
    }
    
    Util::ngrab set .ttkmessage
    wm transient .ttkmessage .
    wm deiconify .ttkmessage
    focus -force .ttkmessage
    tkwait window .ttkmessage
    Util::ngrab release
    return $retval
}
################################################################################
# -icon
# Specifies an icon to display. Must be one of the following: error, info, question or warning.
#
# -type
# Specifies a set of buttons to be displayed. The following values are possible:
# abortretryignore
# Displays three buttons whose symbolic names are abort, retry and ignore.
# ok
#     Displays one button whose symbolic name is ok.
# okcancel
#             Displays two buttons whose symbolic names are ok and cancel.
# retrycancel
#              Displays two buttons whose symbolic names are retry and cancel.
# yesno
#      Displays two buttons whose symbolic names are yes and no.
# yesnocancel
#             Displays three buttons whose symbolic names are yes, no and cancel.
# user
#     Displays buttons of -buttons option.
################################################################################
proc Message::popup {list_args} {
    set icon_opt    [lindex $list_args 0]
    set appname    [lindex $list_args 1]
    set version    [lindex $list_args 2]
    set strmsg    [lindex $list_args 3]
    set strdetail    [lindex $list_args 4]
    
    show -buttons ok -icon $icon_opt -title "$appname  Version:$version\n" \
                    -message $strmsg -detail $strdetail -type okcancel
}
proc Message::popup_buttopt {list_args } {
    set icon_opt    [lindex $list_args 0]
    set appname    [lindex $list_args 1]
    set version    [lindex $list_args 2]
    set strmsg    [lindex $list_args 3]
    set strdetail    [lindex $list_args 4]
    set str_buttopt   [lindex $list_args 5]
    #abortretryignore; ok; okcancel;retrycancel;yesno;yesnocancel;
    set retval [show -buttons $str_buttopt -icon $icon_opt -title "$appname  Version:$version\n" \
            -message $strmsg -detail $strdetail -type yesnocancel -default yes]
            
    return $retval
}
namespace eval Util {
    variable grabStack [list]
}
proc Util::moveCenter {widget {size {}}} {
    set sw [winfo vrootwidth $widget]
    set sh [winfo vrootheight $widget]
    if {[llength $size] == 0} {
        update idletask
        set w [winfo width $widget]
        set h [winfo height $widget]
    } else {
        set w [lindex $size 0]
        set h [lindex $size 1]
    }
    wm geometry $widget +[expr {($sw-$w)/2}]+[expr {($sh-$h)/2}]
}
# Nested grab command
# ngrab set .w   : stack window and grab it.
# ngrab release  : release a top of stack window.
proc Util::ngrab {command {w {}}} {
    variable grabStack
    switch -exact -- $command {
        set {
            if {$w eq {}} { error "wrong args : ngrab command window" }
            grab set $w
            focus -force $w
            lappend grabStack $w
        }
        release {
            set cur [lindex $grabStack end]
            set grabStack [lrange $grabStack 0 "end-1"]
            if {[llength $grabStack] == 0} {
                grab release $cur
                return
            }
            grab set [lindex $grabStack end]
        }
    }
}
namespace eval Statusbar {;#<<<
    variable var
    array set var {
        encoding {}
        version {}
        time {}
        row {}
    }
}
proc Statusbar::Statusbar {} {
    ttk::frame .statusbar
    ttk::label .statusbar.enc -textvariable [namespace current]::var(encoding)
    ttk::label .statusbar.ver -textvariable [namespace current]::var(version)
    ttk::label .statusbar.time -textvariable [namespace current]::var(time)
    ttk::label .statusbar.row -textvariable [namespace current]::var(row)
    ttk::label .statusbar.dummy
    foreach w [winfo children .statusbar] {
        $w configure -takefocus 0 -relief flat -anchor w -padding 0
    }
    foreach n {0 1 2 3} {
        ttk::separator .statusbar.sep$n -orient vertical
    }
    pack .statusbar.ver .statusbar.sep0 .statusbar.enc .statusbar.sep1 \
            -fill y -side left -pady 2 -padx 1
    
    pack .statusbar.time .statusbar.sep2 .statusbar.row .statusbar.sep3 \
            -fill y -side right -pady 2 -padx 1
    
    return .statusbar
}

proc Statusbar::clear {} {
    variable var
    set var(encoding) "Encoding : unknown"
    set var(version) "Version : unknown"
    set var(time) "Time : 0 msec"
    set var(row) "Rows : 0 lines"
}

proc Statusbar::update {} {
    variable var
    set var(encoding) "Encoding : A"
    set var(version) "Version : B"
    set var(time) "Time : C"
    set var(row) "Rows : D"
}

;#>>>
namespace eval Func_TCConsole {
    variable verbose off
    variable Agent_IPAddr "127.0.0.1"
    variable socketport_client 9901
    variable socketport_agent 9902;#default telnet port
}

proc Func_TCConsole::SendCmd {IP_agent strcmd} {
    variable socketport_agent
    
    catch [ set s [socket $IP_agent $socketport_agent] ]
    while {![info exist s]} {
        set x 0
        after 1000 {set x 1}
        vwait x
        catch [ set s [socket $IP_agent $socketport_agent] ]
    }
    fconfigure $s -buffering line
    puts $s $strcmd
    close $s
    
    return true
}
proc Func_TCConsole::getvalue {listcnt backnum} {
    set value [lindex $listcnt [expr [llength $listcnt] - $backnum]]
    return $value
}
proc Func_TCConsole::parsefile {strtoken stropt} {
    variable verbose
    
    foreach InFileName [list "sock.temp"] {
        set InputFileName [file tail  $InFileName]
        if {$verbose == "on"} {
            Func_INI::PntMsg "Input File is $InputFileName"
        }
        set rdinfd [open $InputFileName]
        set token $strtoken
        set str_temp ""
        
        while { [eof $rdinfd] != 1 } {
            gets $rdinfd line
            set test [string length $line]
            set token_loc [string first $token $line]
            
            
            
            if { ($test > 0) && ($token_loc > 0)} {
                set listtemp [split $line ": "]
                if {$verbose == "on"} {
                    Func_INI::PntMsg "$test; $token; $token_loc"
                }
                
                switch -regexp [string tolower $stropt] {
                    {dmt ver} {
                        ##DMT FwVer {} 3.5.24.0_A_TC3085 HwVer {} T14F7_1.0
                        set retstr [lindex $listtemp 3]
                        
                        if {$verbose == "on"} {
                            Func_INI::PntMsg "DMT Ver= $retstr"
                        };#if {$verbose == "on"}
                    }
                    {adsl status} {
                        set retstr [Func_TCConsole::getvalue $listtemp 1]
                    }
                    {chandata (fast|interleaved)} {
                        set retmsg [Func_TCConsole::getvalue $listtemp 2]
                        lappend retstr $retmsg
                        if {$verbose == "on"} {
                            Func_INI::PntMsg "chan data -$strtoken= $retstr"
                        };#if {$verbose == "on"}
                    }
                    
                    {err (fast|interleaved)} {
                        set retmsg [Func_TCConsole::getvalue $listtemp 1]
                        lappend retstr $retmsg
                        if {$verbose == "on"} {
                            Func_INI::PntMsg "Error -$strtoken= $retstr"
                        };#if {$verbose == "on"}
                    }
                };#switch -regexp [string tolower $stropt]
                
            } else  {
                set resstr ""
            };#if { ($test > 0) && ($token_loc > 0)}
            
        };#while { [eof $rdinfd] != 1 }
        
    };#foreach InFileName [list "sock.temp"]
    
    return $retstr
}
proc Func_TCConsole::_GetInfo_DMT-Ver {socktempfname} {
    variable verbose
    
    ###Send Cmd to TCConsole
    set ret_strmsg [Func_TCConsole::SendCmd_DMT-Ver]
    ###Wait 5 sec
    set time 5
    Func_TCConsole::Dbg_Durtion $time
    
    ###Kill TCConsole_PQA.exe
    set ret_strmsg [Func_TCConsole::KillProcess]
    after 500;
    ###
    set ret_strmsg [Func_TCConsole::CPCaptureFname $socktempfname]
    after 500;
    
    ##DMT FwVer: 3.5.24.0_A_TC3085 HwVer: T14F7_1.0
    set strtoken "HwVer"
    set stropt "dmt ver"
    
    set retmsg [Func_TCConsole::parsefile $strtoken $stropt]
    if {$retmsg != ""} {
        Func_TCConsole::SendCmd_DeleteFile "capture.txt"
    }
    
    return $retmsg
}
proc Func_TCConsole::_GetInfo_error {socktempfname} {
    variable verbose
    
    ###Send Cmd to TCConsole
    set ret_strmsg [Func_TCConsole::SendCmd_error]
    ###Wait 5 sec
    set time 5
    Func_TCConsole::Dbg_Durtion $time
    ###Kill TCConsole_PQA.exe
    set ret_strmsg [Func_TCConsole::KillProcess]
    after 500;
    ###
    set ret_strmsg [Func_TCConsole::CPCaptureFname $socktempfname]
    after 500;
    
    set strtoken "fast"
    set stropt "err fast"
    set list_error_fast [Func_TCConsole::parsefile $strtoken $stropt]
    
    set strtoken "interleaved"
    set stropt "err interleaved"
    set list_error_interleave [Func_TCConsole::parsefile $strtoken $stropt]
    
    lappend list_error $list_error_fast $list_error_interleave
    
    if {$list_error != {}} {
        Func_TCConsole::SendCmd_DeleteFile "capture.txt"
    }
    
    return $list_error
}
proc Func_TCConsole::_GetInfo_chandata {socktempfname} {
    variable verbose
    
    ###Send Cmd to TCConsole
    set ret_strmsg [Func_TCConsole::SendCmd_chandata]
    ###Wait 5 sec
    set time 5
    Func_TCConsole::Dbg_Durtion $time
    ###Kill TCConsole_PQA.exe
    set ret_strmsg [Func_TCConsole::KillProcess]
    after 500;
    ###
    set ret_strmsg [Func_TCConsole::CPCaptureFname $socktempfname]
    after 500;
    
    set strtoken "fast"
    set stropt "chandata fast"
    set list_chan_fast [Func_TCConsole::parsefile $strtoken $stropt]
    
    set strtoken "interleaved"
    set stropt "chandata interleaved"
    set list_chan_interleave [Func_TCConsole::parsefile $strtoken $stropt]
    
    lappend list_chan $list_chan_fast $list_chan_interleave
    
    if {$list_chan != {}} {
        Func_TCConsole::SendCmd_DeleteFile "capture.txt"
    }
    
    return $list_chan 
}
proc Func_TCConsole::_GetInfo_adsl-status {socktempfname} {
    variable verbose
    
    ###Send Cmd to TCConsole
    set ret_strmsg [Func_TCConsole::SendCmd_adsl-status]
    ###Wait 5 sec
    set time 5
    Func_TCConsole::Dbg_Durtion $time
    ###Kill TCConsole_PQA.exe
    set ret_strmsg [Func_TCConsole::KillProcess]
    after 500;
    ###
    set ret_strmsg [Func_TCConsole::CPCaptureFname $socktempfname]
    after 500;
    
    set strtoken "modem status"
    set stropt "adsl status"
    
    set retmsg [Func_TCConsole::parsefile $strtoken $stropt]
    
    if {$retmsg != ""} {
        Func_TCConsole::SendCmd_DeleteFile "capture.txt"
    }
    
    return $retmsg
}
proc Func_TCConsole::SendCmd_error {} {
    variable Agent_IPAddr
    
    set cmd "tcconsole_pqa.exe adsl_error.ini"
    set retval [SendCmd $Agent_IPAddr $cmd]
    
    if {$retval == "true"} {
        return $cmd
    }
    return fales
}
proc Func_TCConsole::SendCmd_chandata {} {
    variable Agent_IPAddr
    
    set cmd "tcconsole_pqa.exe adsl_datarate.ini"
    set retval [SendCmd $Agent_IPAddr $cmd]
    
    if {$retval == "true"} {
        return $cmd
    }
    return fales
}
proc Func_TCConsole::SendCmd_adsl-status {} {
    variable Agent_IPAddr
    
    set cmd "tcconsole_pqa.exe adsl_status.ini"
    set retval [SendCmd $Agent_IPAddr $cmd]
    
    if {$retval == "true"} {
        return $cmd
    }
    return fales
}
proc Func_TCConsole::SendCmd_DeleteFile {filename} {
    variable Agent_IPAddr
    
    set cmd "Delete $filename"
    set retval [SendCmd $Agent_IPAddr $cmd]
    
    if {$retval == "true"} {
        return $cmd
    }
    return fales
}
proc Func_TCConsole::SendCmd_DMT-Ver {} {
    variable Agent_IPAddr
    
    set cmd "tcconsole_pqa.exe adsl_fwver.ini"
    set retval [SendCmd $Agent_IPAddr $cmd]
    
    if {$retval == "true"} {
        return $cmd
    }
    return fales
}
proc Func_TCConsole::DegOn {} {
    variable Agent_IPAddr
    
    set cmd "tcconsole_pqa.exe dbg_on.ini"
    set retval [SendCmd $Agent_IPAddr $cmd]
    
    if {$retval == "true"} {
        return $cmd
    }
    return fales
}
proc Func_TCConsole::CPCaptureFname {strsocktemp} {
    variable Agent_IPAddr
    
    set cmd "Cp capture.txt $strsocktemp"
    set retval [SendCmd $Agent_IPAddr $cmd]
    
    if {$retval == "true"} {
        return $cmd
    }
    return fales
}
proc Func_TCConsole::RenameCaptureFname {} {
    variable Agent_IPAddr
    
    set cmd "Rename capture.txt"
    set retval [SendCmd $Agent_IPAddr $cmd]
    
    if {$retval == "true"} {
        return $cmd
    }
    return fales
}
proc Func_TCConsole::KillProcess {} {
    # set command "pv.exe -k -f tcconsole_pqa.exe"
    # set strmsg "Execute $command"
    # ShowMsgOut0 strmsg
    set currpath [file normalize [file dirname [info script]]]
    set path_lib [file join $currpath "../xDSL_lib"]
    set path_pv [file join $path_lib "pv.exe"]
    
    catch {exec $path_pv "-k" "-f" "tcconsole_pqa.exe"} screen
    update
    return $screen
}

proc Func_TCConsole::ProcommExit {inifile capfname} {
    set currpath [file normalize [file dirname [info script]]]
    global env
    
    set tcconsole_file "tcconsoleCfg.ini"
    # set tcconsole_wasfile "test_tcconsole.was"
    # set tcconsole_was [file join $currpath $tcconsole_wasfile]
    set tcconsole_waxfile "test_tcconsole.wax"
    set tcconsole_wax [file join $currpath $tcconsole_waxfile]
    
    ##################################################
    # ustin VARIABLES env
    ###env(ProgramFiles)           = D:\Program Files
    ##################################################
    set str_progdir ""
    foreach {index value} [array get env] {
        # puts "index: $index value: $value"
        foreach temp $index {
            switch -regexp $temp  {
                {^ProgramFiles$} {
                    set str_progdir $value
                    break;
                }
            }
        };#foreach temp $index
    };#foreach {index value} [array get env]
    # puts "str_progdir= $str_progdir"
    
    set path_procomm [file join $str_progdir "Symantec" "Procomm Plus" "PROGRAMS"]
    set path_pw5 [file join $path_procomm "pw5.exe"]
    exec $path_pw5 $tcconsole_wax $inifile $tcconsole_file $capfname
}
proc Func_TCConsole::ShutdownPC {} {
    # set command "pv.exe -k -f tcconsole_pqa.exe"
    # set strmsg "Execute $command"
    # ShowMsgOut0 strmsg
    catch {exec pv.exe "-k" "-f" "tcconsole_pqa.exe"} screen
    update
}
proc Func_TCConsole::Dbg_Durtion {sec} {
    after [expr $sec * 1000];update
    return ture
}

proc TCConsole_Client_Listen {port} {
    global conn
    
    set conn(main) [socket -server TCConsole_Client_ConnAccept $port]
}

proc TCConsole_Client_ConnAccept {sock addr port} {
    global conn TCConsole_Client_line
    
    set conn(addr,$sock) [list $addr $port]
    fconfigure $sock -buffering line
    fileevent $sock readable [list TCConsole_Client_GetReply $sock]
}

proc TCConsole_Client_GetReply {sock} {
    global conn line
    
    if {[eof $sock] || [catch {gets $sock line}]} {
        close $sock
    } else {
        if {[string match "Reply*" $line]} {
            set current_value [lindex $line 1]
            set got_reply 1
        };#if {[string match "Reply*" $line]}
    }
   ;#if {[eof $sock] || [catch {gets $sock line}]}
    
    return "Get Info---$line\n"
}

namespace eval MiscCommon {
    variable verbose "off"
    variable logfile "../log/MiscCommon.log"
    variable list_TCCPEsInfo {{00084	3162L2_3085_A} {00097	3162L2_3085_B}
        {00098	3162L_3085_B} {00099	3162L2_3084_A}
        {00101	3162L_3084_A} {00102	3162L_3084_A}
        {00103	3162L_3084_A} {00106	3162L_3084_A}
        {00115	3162L2_3085_A} {00116	3162L2F_3085_A}
        {00117	3162L2_3085_A} {00149	3162L2_3085_A}
        {00150	3162L2_3085_A} {00180	3162L2F_3085_A}
        {00183	3162L_3084_B} {00194	3162L2F_3085_B}
        {00195	3162L2F_3085_B} {00198	3162L2F_3085_A}
        {01000	3162L_3084_A} {01001	3162L_3084_A}
        {01002	3162L2_3085_B} {01003 }
        {01004	3162L_3084_A} {01010	3162L_3085_A}
        {01011	3162L_3085_A} {01012	3162L_3085_A}
        {01013	3162L_3085_B} {01014 }
        {01015	3162L_3084_A} {01016	3162L2_3085_A}
        {01017	3162L2F_3085_A} {01018	3162L2_3084_B}
        {01019	3162L_3084_A} {01020	3162L2_3084_A}
        {01022	3162L2_3085_B} {01035	3162L2_3085_B}}
}
################################################################################
# CSV flow file
# ID,table,testprofile,looptype,looplength,taplength,noise,noise_Ch1,noise_Ch2,noise_Ch3,
# noise_Ch4,microgain_Ch1,microgain_Ch2,microgain_Ch3,microgain_Ch4,EnWhiteNoise_Ch1,
# EnWhiteNoise_Ch2,EnWhiteNoise_Ch3,EnWhiteNoise_Ch4,
# Std_DS_DataRate,Std_US_DataRate,Std_DS_NM,Std_US_NM
#    end-3           end-2          end-1        end
# DUT dump file
# TestCaseID, LoopLength, BTLength, TimeStamp, TestPorfile,NearEnd-datarate,FarEnd-datarate,
# NearEndFEC,NearEndCRC,NearEndHEC,FarEndFEC,FarEndCRC,FarEndHEC_FastMode,NearEnd-datarate,
# FarEnd-datarate,NearEndFEC,NearEndCRC,NearEndHEC,FarEndFEC,FarEndCRC,
# FarEndHEC_InterleavedMode,NearEnd_NM,FarEnd_NM,ADSL_OpMode
################################################################################
proc MiscCommon::Chk_TestResult {list_dut_datarate str_NM_near str_NM_far rowval} {
    variable verbose
    variable logfile
    
    set dut_ds_datarate [lindex $list_dut_datarate 0]
    set dut_us_datarate [lindex $list_dut_datarate 1]
    
    set std_ds_datarate [lindex $rowval end-3]
    set std_us_datarate [lindex $rowval end-2]
    set std_ds_NM        [lindex $rowval end-1]
    set std_us_NM        [lindex $rowval end]
    
    if {$verbose == "on"} {
        append strmsg "dut_ds_datarate=" $dut_ds_datarate "," \
                "dut_us_datarate=" $dut_us_datarate "," \
                "std_ds_datarate=" $std_ds_datarate "," \
                "std_us_datarate=" $std_us_datarate "," \
                "std_ds_NM=" $std_ds_NM "," "std_us_NM=" $std_us_NM
        puts $strmsg
        Log::LogOut $strmsg $logfile
    }
    if {($dut_us_datarate < $std_us_datarate) || ($dut_ds_datarate < $std_ds_datarate)} {
        return "fail"
    }
    
    return "pass"
}
################################################################################
# if current taable name is differnet with previous one then logout multi logfile
################################################################################
proc MiscCommon::LogTableNameMulitCPEsDSLAMs {strprev_tablename strtablename \
            list_pathlogfile} {
    variable verbose
    set list_pathlogfile [split $list_pathlogfile ]
    if {$strprev_tablename != $strtablename} {
        foreach temp $list_pathlogfile {
            set strmsg "Current Table of Test plan= $strtablename."
            set csvfname "$temp.csv"
            
            Log::LogOut $strmsg $temp
            Log::LogOut $strmsg $csvfname
            
            if {$verbose == "on"} {
                set strmsg "In MiscCommon::LogTableName. $strmsg  $temp $csvfname \
                       length:[llength $list_pathlogfile] $list_pathlogfile"
                puts $strmsg
                Log::LogOut $strmsg $temp
            };#if {$verbose == "on"}
            
        };#foreach temp $list_pathlogfile
        
        return "changed"
    };#if {$strprev_tablename != $strtablename}
    
    return true
}
################################################################################
# if current taable name is differnet with previous one then logout
################################################################################

proc MiscCommon::LogTableName {strprev_tablename strtablename logfile csv_logfile} {
    if {$strprev_tablename != $strtablename} {
        set strmsg "Current Table of Test plan= $strtablename."
        set log_path [file join   ".." "Log"]
        set csvfname [file join $log_path $csv_logfile]
        
        Log::LogOut $strmsg $logfile
        Log::LogOut $strmsg $csvfname
        
        return "changed"
    }
    return true
}
proc MiscCommon::GetTCCPEChipsets {strDeviceID} {
    variable list_TCCPEsInfo
    
    #Third party device or pure bridge TC modem
    ###########################################
    switch -regexp [string tolower $strDeviceID] {
        {[a-z][a-z][a-z][a-z]d$} {
            return $strDeviceID
        }
        {[0-9][0-9][0-9][0-9][0-9]pb$} {
            return $strDeviceID
        }
    };#switch -regexp [string tolower $strDeviceID]
    
    foreach temp $list_TCCPEsInfo {
        set str_devicid [lindex $temp 0]
        set str_tcmoden_model [lindex $temp 1]
        
        if {$str_devicid == $strDeviceID} {
            return $str_tcmoden_model
        };#if {$str_devicid == $strDeviceID}
        
    };#foreach temp $list_DSLAMinfo
    
    return false
}
proc MiscCommon::GetCPEFwVer {retrytimes cpe_comptnum dut_id dut_ip \
            bd_ptnum modempasswd} {
    global list_sock_multicpes
    global sock_desc_modem
    
    ##Check DUT modem connection
    set tcmodem_inifile  "tcmodem.ini"
    set tcmodem_cfgfile "tcmodemCfg.ini"
    set tcmodem_dump "tcmodem.log"
    
    for {set i 0} {$i < $retrytimes} {incr i} {
        switch -regexp $dut_id {
            {([A-Z][A-Z][A-Z][A-Z]d|[0-9][0-9][0-9][0-9][0-9]b)$} {
                set dmtras_ver "3rd party device"
            }
            {[0-9][0-9][0-9][0-9][0-9]pb$} {
                set Parse_TCPureBridge::COMPt_Num    $cpe_comptnum
                set dmtras_ver [Parse_TCPureBridge::DUT_Init $tcmodem_inifile \
                        $tcmodem_cfgfile $tcmodem_dump]
            }
            {zynos$} {
                set Parse_TCPureBridge::COMPt_Num    $cpe_comptnum
                Parse_TCPureBridge::ZyNOS_Init $tcmodem_inifile $tcmodem_cfgfile \
                        $tcmodem_dump
                set dmtras_ver [Parse_TCPureBridge::DUT_Init $tcmodem_inifile \
                        $tcmodem_cfgfile $tcmodem_dump]
            }
            default {
                set retlist [Func_Socket::Get_all_devids_sockch $dut_id $dut_ip $bd_ptnum]
                lappend list_sock_multicpes $retlist
                set sock_desc_modem [lindex $retlist end-1]
                
                set dmtras_ver [Parse_TCModem::Get_Router_ver-ras_dmt \
                        $sock_desc_modem $modempasswd]
            }
        };#switch -regexp $dut_id
        
    };#for {set i 0} {$i < $retrytimes} {incr i}
    
    return $dmtras_ver
}
proc MiscCommon::Log_MultiDSLAMsFWVer {str_args \
            list_cpe_testconds_log_dslaminfo_relaybdptnum} {
    
    set list_args [split $str_args ";"]
    set modempasswd [lindex $list_args 1]
    
    ##Check DUT modem connection
    set retrytimes 1
    
    ################################################################################
    # Check multi DSLAMs variables
    # element of list_cpe_testconds_log_dslaminfo_relaybdptnum
    # 00008 192.168.5.8 4 40 off ../Log/TR067100_BRCM-A_04112007
    #   0        1      2  3  4               5 
    # ../Log/TR067100_BRCM-A_DeviceID00008_04112007 00076 11 1
    #                 end-3                        end-2 end-1 end
    ################################################################################
    set temp [lindex $list_cpe_testconds_log_dslaminfo_relaybdptnum end]
    set dut_id    [lindex $temp 0]
    set dut_ip    [lindex $temp 1]
    set bd_ptnum  [lindex $temp end]
    set cpe_comptnum 1
    set dmtras_ver [MiscCommon::GetCPEFwVer $retrytimes $cpe_comptnum \
                $dut_id $dut_ip $bd_ptnum $modempasswd]
    
    foreach temp [lsort -index end $list_cpe_testconds_log_dslaminfo_relaybdptnum] {
        set logfile   [lindex $temp end-3]
        set csv_logfname [file tail $logfile]
        
        ##For reduce raw csv file to csv2xls log, check csvfname if exist
        ####################################################################
        set log_path [file join   ".." "Log"]
        set csvfname [file join $log_path "$csv_logfname.csv"]
        if {[file exist $csvfname]} {
            break;
        };#if {[file exist $csvfname]}
        
        ###################################################
        # Check multi CPEs variables
        #Log out Firmware version
        #############################################################
        MiscCommon::LogFWVer $dmtras_ver $logfile "$csv_logfname.csv"
        
        if [info exist str_deviceid] {unset str_deviceid}
        append str_deviceid "DeviceID=" $dut_id
        MiscCommon::LogFWVer $str_deviceid $logfile "$csv_logfname.csv"
    };#foreach temp [lsort -index end $list_cpe_testconds_log_dslaminfo_relaybdptnum]
    
    return true
}
proc MiscCommon::Log_MultiCPEsFWVer {str_args \
            list_multicpes_testconds_logfile_availflag_ptnum} {
    variable verbose
    
    global list_sock_multicpes
    global sock_desc_modem
    
    set list_args [split $str_args ";"]
    set modempasswd [lindex $list_args 1]
    
    ##Check DUT modem connection
    set retrytimes 1
    set tcmodem_inifile  "tcmodem.ini"
    set tcmodem_cfgfile "tcmodemCfg.ini"
    set tcmodem_dump "tcmodem.log"
    
    ################################################################################
    # Check multi CPEs variables
    #
    ################################################################################
    foreach temp [lsort -index end $list_multicpes_testconds_logfile_availflag_ptnum] {
        set passflag [lindex $temp end-1]
        set dut_id    [lindex $temp 0]
        set dut_ip    [lindex $temp 1]
        set bd_ptnum  [lindex $temp end]
        set logfile   [lindex $temp end-2]
        set csv_logfname [file tail $logfile]
        
        set log_path [file join   ".." "Log"]
        set csvfname [file join $log_path "$csv_logfname.csv"]
        ##Jun252007 remark by philip
        ####################################################################
        # if {[file exist $csvfname]} {
            # break;
        # };#if {[file exist $csvfname]}
        
        if {$verbose == "on"} {
            puts "temp= $temp"
            append strmsg "In proc MiscCommon::Log_MultiCPEsFWVer." \
                "passflag=" $passflag ";"\
                "dut_id=" $dut_id ";" "dut_ip=" $dut_id  ";"\
                "bd_ptnum=" $bd_ptnum  ";" "logfile=" $logfile  ";"\
                "csv_logfname=" $csv_logfname
            puts $strmsg
            Log::LogOut $strmsg $MiscCommon::logfile
        }
        
        if {[string tolower $passflag] == "pass"} {
            
            for {set i 0} {$i < $retrytimes} {incr i} {
                switch -regexp $dut_id {
                    {([A-Z][A-Z][A-Z][A-Z]d|[0-9][0-9][0-9][0-9][0-9]b)$} {
                        set dmtras_ver "3rd party device"
                    }
                    {[0-9][0-9][0-9][0-9][0-9]pb$} {
                        set Parse_TCPureBridge::COMPt_Num    $bd_ptnum
                        set dmtras_ver [Parse_TCPureBridge::DUT_Init $tcmodem_inifile \
                                $tcmodem_cfgfile $tcmodem_dump]
                    }
                    {zynos$} {
                        set Parse_TCPureBridge::COMPt_Num    $bd_ptnum
                        Parse_TCPureBridge::ZyNOS_Init $tcmodem_inifile $tcmodem_cfgfile \
                                $tcmodem_dump
                        set dmtras_ver [Parse_TCPureBridge::DUT_Init $tcmodem_inifile \
                                $tcmodem_cfgfile $tcmodem_dump]
                    }
                    default {
                        set retlist [Func_Socket::Get_all_devids_sockch $dut_id $dut_ip $bd_ptnum]
                        lappend list_sock_multicpes $retlist
                        set sock_desc_modem [lindex $retlist end-1]
                        
                        if {$verbose == "on"} {
                            if [info exists strmsg] {
                                unset strmsg
                            }
                            append strmsg "In proc MiscCommon::Log_MultiCPEsFWVer." \
                                    "list_sock_multicpes= $list_sock_multicpes."
                            puts $strmsg
                            Log::LogOut $strmsg $MiscCommon::logfile
                        }
                        
                        set dmtras_ver [Parse_TCModem::Get_Router_ver-ras_dmt \
                                $sock_desc_modem $modempasswd]
                    }
                };#switch -regexp $dut_id
                
            };#for {set i 0} {$i < $retrytimes} {incr i}
            
            ###################################################
            # Check multi CPEs variables
            #Log out Firmware version
            #############################################################
            puts "In proc MiscCommon::Log_MultiCPEsFWVer."
            puts "dmtras_ver= $dmtras_ver."
            MiscCommon::LogFWVer $dmtras_ver $logfile "$csv_logfname.csv"
            
            if [info exist str_deviceid] {unset str_deviceid}
            append str_deviceid "DeviceID=" $dut_id
            MiscCommon::LogFWVer $str_deviceid $logfile "$csv_logfname.csv"
            
        };#if {[string tolower $passflag] == "pass"}
    };#foreach temp $list_multicpes_logfile_availflag_ptnum
    
    return true
}
proc MiscCommon::main_cycleone_adslopt {adslreset_iniopt} {
    switch -regexp [string tolower $adslreset_iniopt]  {
        {on} {
            set adsldiag_opt "enable"
            set adslrest_opt "enable"
        }
        {off} {
            set adsldiag_opt "disable"
            set adslrest_opt "disable"
        }
        default {
            set adsldiag_opt "disable"
            set adslrest_opt "disable"
        }
    };#switch -regexp [string tolower $adsldiag_iniopt]
    
    set list_ret_opt [list $adsldiag_opt $adslrest_opt]
    return $list_ret_opt
}
proc MiscCommon::LogFWVer {strfw_ver logfile csv_logfile} {
    set log_path [file join   ".." "Log"]
    set csvfname [file join $log_path $csv_logfile]
    
    ##For reduce raw csv file to csv2xls log, check csvfname if exist
    ####################################################################
    if {[file exist $csvfname]} {
        return true;
    };#if {[file exist $csvfname]}
    
    Log::LogOut $strfw_ver $logfile
    Log::LogOut $strfw_ver $csvfname
    
    return true
}
proc MiscCommon::LogLoopBTLen {strloopbt logfile csv_logfile} {
    set log_path [file join   ".." "Log"]
    set csvfname [file join $log_path $csv_logfile]
    
    Log::LogOut $strloopbt $logfile
    # Log::LogOut $strloopbt $csvfname
    
    return true
}
proc MiscCommon::LogVariableTitle  {csv_logfile} {
    set loghead_info "TestCaseID, LoopLength, BTLength, TimeStamp, TestPorfile,"
    set loghead_fast "NearEnd-datarate,FarEnd-datarate,NearEndFEC,NearEndCRC,NearEndHEC,FarEndFEC,FarEndCRC,FarEndHEC_FastMode"
    set loghead_interleave "NearEnd-datarate,FarEnd-datarate,NearEndFEC,NearEndCRC,NearEndHEC,FarEndFEC,FarEndCRC,FarEndHEC_InterleavedMode"
    set loghead_NM "NearEnd_NM,FarEnd_NM,"
    # set loghead_DMTSW "TCM,MOS,CG,ModCG,ROS,"
    # set lodghead_MinPCB_DS "MinPCB_DS-CO, MinPCB_DS-Remote,"
    set loghead_other "ADSL_OpMode"
    
    # append loghead $loghead_info $loghead_fast "," $loghead_interleave "," $loghead_NM \
            # $loghead_DMTSW $lodghead_MinPCB_DS $loghead_other
    
    append loghead $loghead_info $loghead_fast "," $loghead_interleave "," $loghead_NM \
            $loghead_other
    Log::LogOut $loghead $csv_logfile
    
    return true
}
proc MiscCommon::ShowLogMultiDSLAMsFailTestCases  {list_fail_testcaseid_devid_bdptnum \
            list_cpe_testconds_log_dslaminfo_relaybdptnum} {
    if {[llength $list_fail_testcaseid_devid_bdptnum] > 0} {
        ################################################################################
        # Check multi DSLAMs variables
        # element of list_cpe_testconds_log_dslaminfo_relaybdptnum
        # 00008 192.168.5.8 4 40 off ../Log/TR067100_BRCM-A_04112007
        #   0        1      2  3  4               5
        # ../Log/TR067100_BRCM-A_DeviceID00008_04112007 00076 11 1
        #                 end-3                        end-2 end-1 end
        ################################################################################
        foreach dslam_ptnum [lsort -index end $list_cpe_testconds_log_dslaminfo_relaybdptnum] {
            set relaybd_ptnum [lindex $dslam_ptnum end]
            set strdutid [lindex $dslam_ptnum 0]
            set testbed_logfile   [lindex $dslam_ptnum 5];##
            set testbed_csv_logfname [file tail $testbed_logfile];##
            set dslam_id    [lindex $dslam_ptnum end-2]
            
            if [info exist str_failtestcases] {unset str_failtestcases}
            
            foreach temp $list_fail_testcaseid_devid_bdptnum {
                set fail_testcase [lindex $temp 0]
                set dut_id   [lindex $temp end-1]
                set bd_ptnum  [lindex $temp end]
                
                if {$relaybd_ptnum == $bd_ptnum} {
                    append str_failtestcases $fail_testcase ","
                };#if {$relaybd_ptnum == $bd_ptnum}
                
            };#foreach temp $list_fail_testcaseid_devid_bdptnum
            
            ##Check str_failtestcases if exist
            ##################################
            if [info exist str_failtestcases] {
                if [info exist strmsg] {unset strmsg}
                append strmsg "Fail testcases:" $str_failtestcases "  DeviceID:" $strdutid \
                        "  DSLAM_ID:" $dslam_id "  RelayBD_Ptnum:" $relaybd_ptnum "    "
            } else  {
                append strmsg "All TestcaseID Pass." DeviceID:" $strdutid \
                        "  RelayBD_Ptnum:" $relaybd_ptnum "    "
            };#if [info exist str_failtestcases]
            
            insertLogLine critical $strmsg
            Log "info" $testbed_logfile [list $strmsg]
            
        };#foreach dslam_ptnum [lsort -index end $list_cpe_testconds_log_dslaminfo_relaybdptnum]
    } else  {
        return bypass
    };#if {[llength $list_fail_testcaseid_devid_bdptnum] > 0}
    
    return true
    
}
proc MiscCommon::ShowLogMultiCPEsFailTestCases {list_fail_testcaseid_devid_bdptnum \
            list_multicpes_testconds_logfile_availflag_ptnum} {
    
    if {[llength $list_fail_testcaseid_devid_bdptnum] > 0} {
        foreach cpe_ptnum [lsort -index end $list_multicpes_testconds_logfile_availflag_ptnum] {
            set relaybd_ptnum [lindex $cpe_ptnum end]
            set strdutid [lindex $cpe_ptnum 0]
            set testbed_logfile   [lindex $cpe_ptnum end-3];##
            set testbed_csv_logfname [file tail $testbed_logfile];##
            
            if [info exist str_failtestcases] {unset str_failtestcases}
            
            foreach temp $list_fail_testcaseid_devid_bdptnum {
                
                set fail_testcase [lindex $temp 0]
                set dut_id   [lindex $temp end-1]
                set bd_ptnum  [lindex $temp end]
                
                if {$relaybd_ptnum == $bd_ptnum} {
                    append str_failtestcases $fail_testcase ","
                };#if {$relaybd_ptnum == $bd_ptnum}
                
            };#foreach temp $list_fail_testcaseid_devid_bdptnum
            
            ##Check str_failtestcases if exist
            ##################################
            if [info exist str_failtestcases] {
                if [info exist strmsg] {unset strmsg}
                append strmsg "Fail testcases:" $str_failtestcases "  DeviceID:" $strdutid \
                        "  RelayBD_Ptnum:" $relaybd_ptnum "    "
            } else  {
                append strmsg "All TestcaseID Pass." DeviceID:" $strdutid \
                        "  RelayBD_Ptnum:" $relaybd_ptnum "    "
            };#if [info exist str_failtestcases]
            
            insertLogLine critical $strmsg
            Log "info" $testbed_logfile [list $strmsg]
            
        };#foreach cpe_ptnum [lsort -index end $list_multicpes_testconds_logfile_availflag_ptnum]
        
        # puts "list_fail_testcaseid_devid_bdptnum= $list_fail_testcaseid_devid_bdptnum"
    } else  {
        
        return bypass
    };#if {[llength $list_fail_testcaseid_devid_bdptnum] > 0}
    
    return true
}
proc MiscCommon::ShowMultiDSLAMsonWidget {list_multidslams_ptnum_relaybdptnum} {
    set list_rtumsg {}
    foreach listemp [lsort -index end $list_multidslams_ptnum_relaybdptnum] {
        set dslam_id [lindex $listemp 0]
        set dslam_ptnum [lindex $listemp 1]
        set relaybd_ptnum [lindex $listemp end]
        set dslm_model [DSLAM_Common::_GetDSLAMModel $dslam_id]
        
        if [info exist strmsg] {unset strmsg}
        append strmsg "RelayBD_Pt_Num=$relaybd_ptnum; DSLAM_Model=$dslm_model; \
                DSLAM_ID=$dslam_id; DSLAM_PortNum=$dslam_ptnum;\n"
        lappend list_rtumsg $strmsg
        
    };#foreach listemp [lsort -index end $list_multidslams_ptnum_relaybdptnum]
    
    set ::pref(appname) "MultiDSLAMs"; set ::VERSION "RC6"
    set strmsg "Check which Port number in RelayBD is available?"
    foreach strtemp $list_rtumsg {
        set retstr [Message::popup_buttopt [list  "question" $::pref(appname) $::VERSION \
                $strmsg "$strtemp" "yes no cancel"]]
        if {($retstr == "no") || ($retstr == "cancel")} {
            exit
        }
    };#foreach strtemp $list_rtumsg
    
    return true
}
proc MiscCommon::ShowMultiCPEsonWidget {list_multicpes_testconds_logfile_availflag_ptnum} {
    set list_rtumsg {}
    foreach listemp [lsort -index end $list_multicpes_testconds_logfile_availflag_ptnum] {
        set dut_id [lindex $listemp 0]
        set dut_ip [lindex $listemp 1]
        set retryshowtime_times [lindex $listemp 2]
        set stability [lindex $listemp 3]
        set adslreset_opt [lindex $listemp 4]
        set result [lindex $listemp end-1]
        set pt_num [lindex $listemp end]
        set str_tcmodem_model [MiscCommon::GetTCCPEChipsets $dut_id]
        
        if [info exist strmsg] {unset strmsg}
        append strmsg "RelayBD_Pt_Num=$pt_num; ID=$dut_id; ModemModel=$str_tcmodem_model; \
                IP=$dut_ip; WaitShowtime_Times=$retryshowtime_times; \
                AfterShowtime_Duration=$stability; ADSLReset_Opt=$adslreset_opt\n"
        lappend list_rtumsg $strmsg
        # puts $strmsg
    };#foreach listemp [lsort -index end $list_multicpes_testconds_logfile_availflag_ptnum]
    
    set strmsg "Check which Port number in RelayBD is available?"
    foreach strtemp $list_rtumsg {
        set retstr [Message::popup_buttopt [list  "question" $::pref(appname) $::VERSION \
                $strmsg "$strtemp" "yes no cancel"]]
        if {($retstr == "no") || ($retstr == "cancel")} {
            exit
        }
    };#foreach strtemp $list_rtumsg
    
    return true
}
proc MiscCommon::ShowSwitchRelayBD {list_mulitdslam} {
    set strdutid [lindex $list_mulitdslam 0]
    set dut_ip    [lindex $list_mulitdslam 1]
    set bd_ptnum  [lindex $list_mulitdslam end]
    set logfile   [lindex $list_mulitdslam end-3]
    set dslam_id  [lindex $list_mulitdslam end-2]
    
    if {[string length $dslam_id] == 5} {
        ##MultiDSLAMs case
        set dslm_model [DSLAM_Common::_GetDSLAMModel $dslam_id]
    } else  {
        ##MultiCPEs case
        ##../Log/UR2v7_BRCM-B_DeviceID00116_03262007
        #############################################
        set strtemp [file tail $dslam_id]
        set list_dslam_id [split $strtemp "_"]
        set dslm_model [lindex $list_dslam_id 1]
    };#if {[string length $dslam_id] == 5}
    
    set strmsg [MiscCommon::RelayBDSwitch $bd_ptnum]
    puts "$strmsg form Relay Board."
    set strmsg "Switch to Relay Board Port$bd_ptnum. DSLAM_Model=$dslm_model. \
            DeviceID $strdutid. IP_Address $dut_ip"
    
    insertLogLine critical $strmsg
    Log "info" $logfile [list $strmsg]
}
proc MiscCommon::RelayBDSwitch {pt_num} {
    set path_lib [file join ".." "xDSL_Lib"]
    set path_relaybd [file join $path_lib "xDSL_RelayBD.exe"]
    catch {exec $path_relaybd $pt_num} retsmg
    
    return $retsmg
}
namespace eval Test_NoiseMargin {
    variable verbose "off"
    variable logfile "../log/Test_NoiseMargin.log"
}
proc Test_NoiseMargin::CalcBER_Exp {str_dumpinfo str_last_dumpinfo std_ds_BER_coff test_time logfile} {
    ################################################################################
    # strdumpinfo as below:
    # "TestCaseID, LoopLength, BTLength, TimeStamp, TestPorfile,"
    #       0          1           2         3            4
    # "NearEnd-datarate,FarEnd-datarate,NearEndFEC,NearEndCRC,NearEndHEC,FarEndFEC,FarEndCRC,FarEndHEC_FastMode"
    #       5                   6            7        8            9        10        11        12
    # "NearEnd-datarate,FarEnd-datarate,NearEndFEC,NearEndCRC,NearEndHEC,FarEndFEC,FarEndCRC,FarEndHEC_InterleavedMode"
    #       13                  14           15        16          17        18        19        20
    ################################################################################
    if {[string length $str_dumpinfo] < 0} {
        return fail
    }
    set list_strmsg [split $str_dumpinfo ,]
    set neardatarate_fast [lindex $list_strmsg 5]
    set neardatarate_interleave [lindex $list_strmsg 13]
    set list_str_lastmsg [split $str_last_dumpinfo ,]
    
    set strmsg "neardatarate_fast=$neardatarate_fast; \
            neardatarate_interleave=$neardatarate_interleave"
    insertLogLine info $strmsg;
    Log::LogList_level "info" $logfile [list $strmsg]
    
    ##Check if fast or interleave
    if {($neardatarate_fast > 0) && ($neardatarate_interleave == 0)} {
        set path 15
        set crc_last [lindex $list_str_lastmsg 8]
        set crc [lindex $list_strmsg 8]
        
        set datarate $neardatarate_fast
    } elseif {($neardatarate_fast == 0) && ($neardatarate_interleave > 0)} {
        set path 40
        set crc_last [lindex $list_str_lastmsg 16]
        set crc [lindex $list_strmsg 16]
        set datarate $neardatarate_interleave
    }
    set crc_diff [expr $crc - $crc_last]
    set strmsg "path=$path; crc_last=$crc_last; crc=$crc; \
            crc_diff=$crc_diff; datarate=$datarate"
    insertLogLine info $strmsg;
    Log::LogList_level "info" $logfile [list $strmsg]
    
    set testtime_sec [expr $test_time * 60]
    set a [expr $path * $crc_diff]
    set b [expr $datarate * $testtime_sec]
    set c [expr [expr exp(7)]* $a]
    set d [expr $std_ds_BER_coff * $b]
    set diff [expr $c - $d]
    set strmsg "a=$a; b=$b; c=$c; d=$d; diff=$diff"
    Log::LogList_level "info" $logfile [list $strmsg]
    
    if {$diff < 0} {
        set str_result "Pass"
    } else  {
        set str_result "Fail"
    }
    set strmsg "Result=$str_result; diff_value=$diff"
    insertLogLine info $strmsg;
    Log::LogList_level "info" $logfile [list $strmsg]
    
    return $str_result
}
proc Test_NoiseMargin::Timer {maxretry timeout_interval strinfo logfile csv_logfname} {
    set retrytimes 0
    set strmsg "maxretry=$maxretry; timeout_interval=$timeout_interval; $strinfo"
    Log::LogList_level "info" $logfile [list $strmsg]
    if {$csv_logfname != " " } {
        Log::LogList_level "info" "$csv_logfname.csv" [list $strmsg]
    }
    
    while {1} {
        incr retrytimes
        ##Check retry times to break
        if {$retrytimes > $maxretry} { break }
        
        set timeout_duration [expr $timeout_interval * $retrytimes]
        set strmsg "Waiting $timeout_duration seconds. $strinfo. MaxRetry=$maxretry; \
                Retrytimes=$retrytimes."
        
        insertLogLine info $strmsg
        Log::LogList_level "info" $logfile [list $strmsg]
        # Log::LogList_level "info" "$csv_logfname.csv" [list $strmsg]
        
        after [expr $timeout_interval * 1000]; update
        
    };#while {1}
    
    return true
}
proc Test_NoiseMargin::MicroGainIncr {str_passfail minval logfile csv_logfname rowval \
    list_dutid str_current_testcaseid waituptimes timeout list_DSLAMInfo} {
    
    set microgain_value 0
    #If modem still links up
    ####################################
    while {$str_passfail == "pass"} {
        incr microgain_value
        ##Check microgain_value value great than minval
        if {$microgain_value > $minval} { break }
        
        set strmsg "microgain_value= $microgain_value."
        insertLogLine critical $strmsg
        Log::LogList_level "info" $logfile [list $strmsg]
        Log::LogList_level "info" $csv_logfname [list $strmsg]
        
        set list_rtumsg [NoiseCommon::_SetMicroGainStep $microgain_value $rowval $logfile]
        Log::LogList_level "info" $logfile $list_rtumsg
        
        #############################################
        #After increase 1dB noise power to wait 60 sec
        ##############################################
        set maxretry 2;set timeout_interval 30;set strinfo "after increase 1dB power on R side."
        Test_NoiseMargin::Timer $maxretry $timeout_interval $strinfo $logfile $csv_logfname
        
        set adsldiag_opt "disable";set adslrest_opt "disable"
        
        #Dump DUT Modem data by 3rd or TC modem
        ##########################################
        set strmsg_DumpInfo_last [DUT_DumpInfo_Sel $list_dutid $str_current_testcaseid \
                $logfile $waituptimes $timeout $csv_logfname $rowval \
                $adsldiag_opt $adslrest_opt $list_DSLAMInfo]
        
        set str_passfail [Parse_TCModem::_GetInfo_DumpInfo_PassFail $strmsg_DumpInfo_last]
        
    };#while {$str_passfail == "pass"}
    
    return $str_passfail
}
########################################################
#Waiting test case duration.
#Calculate BER to judge pass or fail.
########################################################
proc Test_NoiseMargin::BERDuration {rowval logfile csv_logfname \
    str_DUTID str_current_testcaseid waituptimes timeout list_DSLAMInfo} {
    
    ################################################################################
    # rowvalue list format as below:
    # ID,table,testprofile,looptype,looplength,taplength,noise,noise_Ch1,noise_Ch2,noise_Ch3,noise_Ch4,
    #  0   1       2         3            4        5        6    7        8            9          10
    # microgain_Ch1,microgain_Ch2,microgain_Ch3,microgain_Ch4,
    #        11            12            13            14
    # EnWhiteNoise_Ch1,EnWhiteNoise_Ch2,EnWhiteNoise_Ch3,EnWhiteNoise_Ch4
    #         15                16            17            18
    # Modemprofile,margin_testtime,Std_DS_BER_Coff,Std_US_BER_Coff
    #         19        20            21
    ##################################################################################
    
    set Std_DS_BER_Coff [lindex $rowval 21];#(???e-7)
    set duration [lindex $rowval 20] ;
    set strmsg "Std_DS_BER_Coff=$Std_DS_BER_Coff; duration(min)=$duration"
    insertLogLine info $strmsg; Log "info" $logfile [list $strmsg]
    
    # set duration 9;#testing purpose
    set duration_sec [expr $duration * 60]
    # set strmsg "duration(min)=$duration"
    # insertLogLine info $strmsg; Log "info" $logfile [list $strmsg]
    
    set timeout_interval 30;set strinfo "Waiting BER duration."
    set maxretry [expr $duration_sec / $timeout_interval]
    Test_NoiseMargin::Timer $maxretry $timeout_interval $strinfo $logfile $csv_logfname
    
    set strmsg "Complete BER duration."
    insertLogLine info $strmsg
    Log::LogList_level "info" $logfile [list $strmsg]
    
    set Func_Socket::_verbose on;#debug purpose
    set Parse_TCModem::verbose on;#debug purpose
    
    set adsldiag_opt "disable";set adslrest_opt "disable"
    #Dump DUT Modem data by 3rd or TC modem
    ##########################################
    set strmsg_DumpInfo [DUT_DumpInfo_Sel $str_DUTID $str_current_testcaseid \
            $logfile $waituptimes $timeout $csv_logfname $rowval \
            $adsldiag_opt $adslrest_opt $list_DSLAMInfo]
        
                
        return $strmsg_DumpInfo
}