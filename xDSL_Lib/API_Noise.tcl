# ChangeLog
# By Philip Shen @PQA TrendChip Corp.
# ---------------------------------------------------------------------
#May02-2006 Initialize Noise control.
#           Initialize namespace eval dls5204; from dls5204_telnet_command.tcl
#May03-2006 Modify dls5204::handle_report from "CMD_ACCEPTED" to "CMD_SUCCEEDED"
#           Verify API_Noise.tcl using test_noise.tcl.
#           Add proc PingTest.
#May05-2006 Add timeout mechanism to check setup procedure
#           Verify API_Noise.tcl using test_noise.tcl.
#May06-2006 Add proc Checksetting to check setting status.
#May09-2006 Verify both proc _NoiseGen_StepLog and _NoiseGen using test_noise.tcl
#Jul07-2006 Add insertLogLine to show info to GUI in porc _NoiseGen
# Sep14-2006 Add dls550::SetNoiseGain and dls550::SetMicroGain
# Sep22-2006 Add dls5204::SetMicroGain, dls5204::GetMACAddress, dls5204::GetSWVer,
#            dls5204::GetChannelFileName
# Sep27-2006 Add dls5204::_NoiseGen_TDWhiteNoise for DLS5500
# Sep30-2006 Divide _td and _xtk two categorey In NoiseCommon::_SendCmd
# Oct01-2006 Add porc dls5204::SetInjectors
# Oct12-2006 Verify 5dbm issue on 5500 by FAE Kevin
# Oct19-2006 Add proc dls5500::Run_EnDisXtkWhiteNoise to combine WhiteNoise to FB Noise
# Oct25-2006 Modify dls5204::_NoiseGen_XtkWhiteNoise
# Oct27-2006 Add GetChannelFileName in both proc dls5500::Run_EnDisXtkWhiteNoise and
#            proc dls5500::Run_EnDisTDWhiteNoise to tune procedure
# Jan13-2007 modify AWGN_xtk path for dls5204 and dls5500 in proc dls5204::LoadXtkWhiteNoise
###################################################################################

namespace eval dls5204 {
    set currpath [file dirname [info script]]
    source "$currpath/API_Misc.tcl"
    
    # Socket descriptor
    variable _sock_desc ""
    
    # Server Response (Must be global for vwait)
    variable line ""
    
    # time out
    variable _timeout 60000
    # for debugging
    variable _verbose off
    variable logfile "../log/noise.log"
    
    # for check setting
    variable _retrytimes  8
    #noise file of prvious test case
    variable csv_nosiefname ""
}

# ]-- Private Functions --[ #
################################################################################
# Procedure : write_socket
# Purpose   : Sends 'command' to the 5204
# Exits if a connection is not established
################################################################################
proc dls5204::write_socket { command } {
    variable _sock_desc
    variable _verbose
    variable logfile
    set snd "Sending: "
    
    if {[eof $_sock_desc]} {
        puts "Error: A connection has not been established!!"
    }
    if { $_verbose == "on" } {
        set msg "$_sock_desc $snd $command"
        puts $msg
        Log::tofile "info" "noise.log" $msg
    }
    puts $_sock_desc $command
}

################################################################################
# Procedure : read_socket
# Purpose   : Reads response from 5204
# Sets 'line' to reponse if successful, otherwise
# sets 'line' to "Timeout" if a timeout occured while
# waiting for a response.
# Returns a parsed 5204 response or 'timed out' on failure.
################################################################################
proc dls5204::read_socket { } {
    
    #variable sock descriptor and line
    variable _sock_desc
    variable line
    variable _verbose
    variable logfile
    variable _timeout
    
    # set the timer
    # Remark by Philip
    set time_id [after $_timeout {set line -1}]
    
    # when socket becomes readable
    set dls5204::line ""
    fileevent $_sock_desc readable {
        # if { $dls5204::_verbose == "on" } {
            # append msg "$dls5204::_sock_desc" "\n" "$dls5204::line"
            # puts -nonewline $msg
            # Log::tofile "info" $logfile $msg
        # }

        # Disable fileevent until set again
        #####################################
        #5/4/2006 Remark by Philip  
        fileevent $dls5204::_sock_desc readable ""
        ###################################### 
        # poll until read line or timed out
        
        while { $dls5204::line == "" } {
            set dls5204::line [read $dls5204::_sock_desc]
            update
        }

    };#fileevent $_sock_desc readable
    
    # wait for variable line to change
    # vwait ::line
    vwait line
    
    if { $line == -1 } {
        # Timeout Occured
        set line "Timeout"
        set result "Connection timed out."
    } else {
        # recieved message. disable timer
        # Remark by Philip
        after cancel $time_id
        if { $_verbose == "on" } {
            set msg "Recieved: $line"
            puts -nonewline $msg
            Log::tofile "info" $logfile $msg
            update
        }
        set result [dls5204::parse_line]
    }
    return $result
}
################################################################################
# Procedure : IsConnected
# Purpose   : checks if already connected
# Returns   : 1 connected - 0 not connected
################################################################################
proc dls5204::IsConnected { } {
    variable _sock_desc
    
    if { $_sock_desc == "" } {
        return 0
    }
    return 1
}
################################################################################
# Procedure : parse_line
# Purpose   : Parse the response form the 5204
################################################################################
proc dls5204::parse_line {} {
    variable line
    variable _verbose
    variable logfile
    
    puts $line
    if { $_verbose == "on" } {
        set msg "In parse_line. $line"
        puts -nonewline $msg
        Log::tofile "info" $logfile $msg
        update
    }
    
    set result ""
    
    if {[regexp {^!STX:([A-Z]+)\(([A-Z_]+)\):VAL\(([A-Z0-9_]+)\)} $line -> command type response_code] == 1} {
        if { $dls5204::_verbose == "on" } {
            puts "command: $command; type: $type; response_code: $response_code"
        }
        # found a match
        switch -- $command {
            "SET" {
                if { $type == "REPORT" } {
                    set result [dls5204::handle_report $response_code]
                } else {
                    dls5204::perror "parse_line" "Cannot parse $type response"
                }
            }
            
            "TRAP" {
                if { $type == "ERROR" } {
                    set result [dls5204::handle_error $response_code]
                } else {
                    dls5204::perror "parse_line" "Cannot parse $type response"
                }
            }
            
            "REPLY" {
                if { $type == "M_SELECTED_OUTPUT" } {
                    set result [dls5204::handle_reply $response_code]
                } else {
                    dls5204::perror "parse_line" "Cannot parse $type response"
                }
                #   set result [handle_reply $response_code]
            }
            default {
                return "parse_line: Unknown case."
            }
        };#switch -- $command
        if { $_verbose == "on" } {
            set msg "In parse_line. dls5204::handle_reply result: $result"
            puts $msg
            Log::tofile "info" $logfile $msg

        }
        return $result
        
    } else {
        # Don't know what we have. This SHOULD never happen.
        #May042006 Remark by Philip
        # dls5204::perror "ERROR" "Cannot parse response."
        return false
        
    };#if {[regexp {^!STX:([A-Z]+)\(([A-Z_]+)\):VAL\(([A-Z0-9_]+)\)} $line -> command type response_code] == 1}
}
################################################################################
# Procedure : handle_report
# Purpose   : Display errors regarding REPORT messages
# Returns   : Translation of error code
################################################################################
proc dls5204::handle_report { report_type } {
    # 5/3/2006 modify from "CMD_ACCEPTED" to "CMD_SUCCEEDED"
    variable _verbose
    variable logfile
    
    if { $_verbose == "on" } {
        set msg "In handle_report. Input argument: $report_type"
        puts $msg
        Log::tofile "info" $logfile $msg
    }
    
    switch -- $report_type {
        "CMD_SUCCEEDED" {
            return "Successful."
        }
        
        "LEVEL_TOO_HIGH" {
            return "Power level of combined crosstalk exceeds maximum output power level."
        }
        
        "PACKAGE_MANAGER_ERROR" {
            return "Package manager error."
        }
        
        "CREST_FACTOR_UNATTAINED" {
            return "The resulting crest factor is less than 5"
        }
        
        "SET_XTALK_GAIN_BLOCKED" {
            return "Attempted to set crosstalk gain while crosstalk is muted. OR The resulting crest factor is less than 5."
        }
        
        "SET_RFI_GAIN_BLOCKED" {
            return "Attempted to set RFI gain while crosstalk is muted. OR RFI has not been licensed."
        }
        
        "RFI_INVALID_LICENSE" {
            return "Invalid RFI license."
        }
        
        "LICENSE_SERVER_ERROR" {
            return "Invalid RFI license."
        }
        
        "LICENSE_SERVER_LOST" {
            return "Connection to license server was lost."
        }
        
        "FILE_ACCESS_ERROR" {
            return "The file does not exist"
        }
        
        default {
            return "Unknown error type: $report_type"
        }
    };#switch $report_type
}
################################################################################
# Procedure : handle_error
# Purpose   : Display errors regarding ERROR messages
# Returns   : Translation of error code
################################################################################
proc dls5204::handle_error { error_type } {
    
    switch -- $error_type {
        
        "FAILURE" {
            return "Unspecified error."
        }
        
        "BAD_PARAMETER_ID" {
            return "Invalid task or parameter identifer in message. OR. A command is previous to this is required."
        }
        
        "BUSY" {
            return "Busy operating on a pervious command."
        }
        
        "CMD_OVERFLOW" {
            return "Remote control access is disabled. Cannot process command."
        }
        
        default {
            return "Unknown error type: $error_type"
        }
    };#switch $error_type
    
}
################################################################################
# Procedure : handle_reply
# Purpose   : Converts relpy code to channel number
# Returns   : Translation of reply code "
################################################################################
proc dls5204::handle_reply { reply_type } {
    
    switch -- $reply_type {
        
        "OUTPUT_1" {
            return "Channel_1"
        }
        
        "OUTPUT_2" {
            return "Channel_2"
        }
        
        "OUTPUT_3" {
            return "Channel_3"
        }
        
        "OUTPUT_4" {
            return "Channel_4"
        }
        
        default {
            return "Unknown error type: $error_type"
        }
    };#switch $reply_type
}
################################################################################
# Procedure : perror
# Purpose   : Report Errors to console
################################################################################
proc dls5204::perror { command errormsg } {
    # Display Error messages
    error "$command: $errormsg"
}

# ]-- Public Functions --[ #
################################################################################
# Procedure : IsConnected
# Purpose   : checks if already connected
# Returns   : 1 connected - 0 not connected
################################################################################
proc dls5204::Connect { ip port } {
    variable _sock_desc
    set list_rtu {}
    
    # Check for valid args
    if { $ip != "" && $port != "" } {
        # check if already connected
        if { [dls5204::IsConnected] } {
            dls5204::perror "Dls_5204_Connect" "Already Connected"
        }
        
        set _sock_desc [socket $ip $port]
        if {[eof $_sock_desc]} {
            dls5204::perror "Dls_5204_Connect:" "Cannot establish connection to $ip on port $port."
        } else {
            fconfigure $_sock_desc -blocking 0 -buffering line -translation auto
            puts "Dls_5204_Connect: Sucessful."
            return $_sock_desc
        }
    };#if { $ip != "" && $port != "" }
    
    lappend list_rtu "Dls_5204_Connect" "Invalid amount of parameters."
    dls5204::perror "Dls_5204_Connect" "Invalid amount of parameters."
    return $list_rtu
}
###############################################################################
# Procedure : Dls_5204_SetMicroGain
# Purpose   : Adapt from DLS5500 v1.0
################################################################################
proc dls5204::SetMicroGain {gain_value} {
    variable _verbose
    variable logfile
    
    set list_rtu {}
    
    # check if  connected
    if { ![dls5204::IsConnected] } {
        dls5204::perror "Dls_5204_SetMicroGain" "Not connected to unit."
    }
    
    # check for non null parameters
    if { $gain_value == "" } {
        lappend list_rtu "Dls_5204_SetMicroGain" "Invalid parameters."
        dls5204::perror "Dls_5204_SetMicroGain" "Invalid parameters."
        return $list_rtu
    }
    
    dls5204::write_socket "!STX:SET(M_MICRO_GAIN):VAL($gain_value);ETX!"
    
    ################################################
    #May092006 By Philip
    #Make sure Catch return msg from Noise Generator
    ################################################
    set result [dls5204::CheckSetting]
    
    if {$_verbose == "on"} {
        set msg "In dls5204::SetMicroGain result=$result."
        puts $msg
        Log::tofile "info" $logfile $msg
    }
    
    switch -regexp [string tolower $result]  {
        {^successful} {
            lappend list_rtu "Dls_5204_SetMicroGain:$gain_value Command successful. Set to $gain_value dBm."
            puts $list_rtu
        }
        default {
            lappend list_rtu "Dls_5204SetMicroGain" $result
            dls5204::perror "Dls_5204_SetMicroGain" $result
        }
    };#switch -regexp [string tolower $result]
    
    return $list_rtu
}
################################################################################
# Procedure : Dls_5204_SetGain
# Purpose   : Sets the gain on the 5204
################################################################################
proc dls5204::SetGain { gain_type gain_value } {
    set list_rtu {}
    
    # check if  connected
    if { ![dls5204::IsConnected] } {
        dls5204::perror "Dls_5204_SetGain" "Not connected to unit."
    }
    
    # check for non null parameters
    if { $gain_type == "" || $gain_value == "" } {
        lappend list_rtu "Dls_5204_SetGain" "Invalid parameters."
        dls5204::perror "Dls_5204_SetGain" "Invalid parameters."
        return $list_rtu
    }
    
    switch -- [string toupper $gain_type] {
        
        "XTALK" {
            dls5204::write_socket "!STX:SET(M_XTALK_GAIN):VAL($gain_value);ETX!"
        }
        
        "RFI" {
            dls5204::write_socket "!STX:SET(M_RFI_GAIN):VAL($gain_value);ETX!"
        }
        
        default {
            dls5204::perror "Dls_5204_SetGain" "Invalid parameters."
        }
    };#switch [string toupper $gain_type]
    
    ################################################
    #May092006 By Philip
    #Make sure Catch return msg from Noise Generator
    ################################################
    set result [dls5204::CheckSetting]
    
    if  { $result == "Successful." } {
        lappend list_rtu "Dls_5204_SetGain: Command successful. $gain_type set to $gain_value dBm."
        puts $list_rtu
    } else {
        lappend list_rtu "Dls_5204_SetGain" $result
        dls5204::perror "Dls_5204_SetGain" $result
    };#if  { $result == "Successful." }
    
    return $list_rtu
}
################################################################################
# Procedure : Dls_5204_GetSelectedChannel
# Purpose   : Determine what output is selected
# Returns   : Channel_1 ... Channel_4 depending on the current selcted output
################################################################################
proc dls5204::GetSelectedChannel { } {
    # check if  connected
    if { ![dls5204::IsConnected] } {
        dls5204::perror "Dls_5204_GetSelectedChannel" "Not connected to unit."
    }
    
    dls5204::write_socket "!STX:GET(M_SELECTED_OUTPUT);ETX!"
    
    set result [dls5204::read_socket]

    #get result returns 2 responses.. get the second one.
    set result2 [dls5204::read_socket]
    ################################################
    #May042006 By Philip
    #Make sure Catch return msg from Noise Generator
    ################################################
    while {$result2 == "false"} {
        set x 0
        after 500 {set x 1}
        vwait x
        set result2 [dls5204::read_socket]
    }
    
    # check for a valid result
    if  { $result == "Channel_1" || $result == "Channel_2" || \
          $result == "Channel_3" || $result == "Channel_4" } {
        puts "Dls_5204_GetSelectedOutput: Current selected output is $result."
        return $result
    } else {
        puts "--> $result is result"
        dls5204::perror "Dls_5204_GetSeelectedOutput" "$result"
    }
}
proc dls5204::GetMACAddress { } {
    # check if  connected
    if { ![dls5204::IsConnected] } {
        dls5204::perror "Dls_5204_GetMACAddress" "Not connected to unit."
    }
    
    dls5204::write_socket "!STX:GET(M_MAC_ADDRESS);ETX!"
    set result [dls5204::read_socket]
    
    #get result returns 2 responses.. get the second one.
    set result2 [dls5204::read_socket]
    ################################################
    #May042006 By Philip
    #Make sure Catch return msg from Noise Generator
    ################################################
    while {$result2 == "false"} {
        set x 0
        after 500 {set x 1}
        vwait x
        set result2 [dls5204::read_socket]
    }
    
    # check for a valid result
    if  { $result1 != " "  } {
        puts "Dls_5204_GetMACAddress: Current selected output is $result."
        return $result
    } else {
        puts "--> $result is result"
        dls5204::perror "Dls_5204_GetMACAddress" "$result"
    }
}
proc dls5204::GetChannelFileName {channel} {
    set list_rtu {}
    
    # check if  connected
    if { ![dls5204::IsConnected] } {
        dls5204::perror "Dls_5204_GetChannelFileName" "Not connected to unit."
    }
    
    # check for valid channel
    set tmp_channel [string toupper $channel]
    set str_sckcmd ""
    set channel ""
    switch -- $tmp_channel {
        
        "CHANNEL_1" {
            set channel 1
        }
        "CHANNEL_2" {
            set channel 2
        }
        "CHANNEL_3" {
            set channel 3
        }
        "CHANNEL_4" {
            set channel 4
        }
        default {
            dls5204::perror "Dls_5204_GetChannelFileName" "$channel is an invalid channel"
        }
    };#switch $tmp_channel
    set str_sckcmd "!STX:GET(M_FILE_NAMES):VAL(OUTPUT_$channel);ETX!"
    # puts $str_sckcmd
    dls5204::write_socket $str_sckcmd
    
    ################################################
    #May092006 By Philip
    #Make sure Catch return msg from Noise Generator
    ################################################
    set result [dls5204::CheckSetting]
    
    # check result
    if  { $result == "Successful." } {
        lappend list_rtu "Dls_5204_GetChannelFileName: Command successful."
        puts $list_rtu
    } elseif {$result == "Bypass"} {
        lappend list_rtu "Dls_5204_GetChannelFileName: Getting successful. But can't get reply msg."
        puts $list_rtu
    } else {
        lappend list_rtu "Dls_5204_GetChannelFileName" $result
        dls5204::perror "Dls_5204_GetChannelFileName" $result
    }
    return $list_rtu
}
proc dls5204::GetSWVer {} {
    variable _verbose
    variable logfile
    
    # check if  connected
    if { ![dls5204::IsConnected] } {
        dls5204::perror "Dls_5204_GetSWVer" "Not connected to unit."
    }
    
    dls5204::write_socket "!STX:GET(M_SOFTWARE_VERSION);ETX!"
    
    ################################################
    #May092006 By Philip
    #Make sure Catch return msg from Noise Generator
    ################################################
    set result [dls5204::read_socket]
    
    if {$_verbose == "on"} {
        set msg "In dls5204::GetSWVer result=$result."
        puts $msg
        Log::tofile "info" $logfile $msg
    }
    
    switch -regexp [string tolower $result]  {
        {^successful} {
            lappend list_rtu "Dls_5204_GetSWVern: Command successful. result= $result."
            puts $list_rtu
        }
        default {
            lappend list_rtu "Dls_5204GetSWVer" $result
            dls5204::perror "Dls_5204_GetSWVer" $result
        }
    };#switch -regexp [string tolower $result]
    
    return $list_rtu
}
################################################################################
# Procedure : Dls_5204_SetSelectedChannel
# Purpose   : Select the current channel to operate on.
################################################################################
proc dls5204::SetSelectedChannel { channel } {
    set list_rtu {}
    
    # check if  connected
    if { ![dls5204::IsConnected] } {
        dls5204::perror "Dls_5204_SetSelectedChannel" "Not connected to unit."
    }
    
    # check for valid channel
    set tmp_channel [string toupper $channel]
    set str_sckcmd ""
    set channel ""
    switch -- $tmp_channel {
        
        "CHANNEL_1" {
            set str_sckcmd "!STX:SET(M_SELECT_OUTPUT):VAL(OUTPUT_1);ETX!"
            set channel 1
        }
        
        "CHANNEL_2" {
            set str_sckcmd "!STX:SET(M_SELECT_OUTPUT):VAL(OUTPUT_2);ETX!"
            set channel 2
        }
        
        "CHANNEL_3" {
            set str_sckcmd "!STX:SET(M_SELECT_OUTPUT):VAL(OUTPUT_3);ETX!"
            set channel 3
        }
        
        "CHANNEL_4" {
            set str_sckcmd "!STX:SET(M_SELECT_OUTPUT):VAL(OUTPUT_4);ETX!"
            set channel 4
        }
        
        default {
            dls5204::perror "Dls_5204_SetSelectedOutput" "$channel is an invalid channel"
        }
    };#switch $tmp_channel
    # puts $str_sckcmd
    dls5204::write_socket $str_sckcmd
    
    ################################################
    #May092006 By Philip
    #Make sure Catch return msg from Noise Generator
    ################################################
    set result [dls5204::CheckSetting]
    
    # check result
    if  { $result == "Successful." } {
        lappend list_rtu "Dls_5204_SetSelectedOutput:$channel Command successful. Now using channel #$channel."
        puts $list_rtu
    } elseif {$result == "Bypass"} {
        lappend list_rtu "Dls_5204_SetSelectedOutput:$channel Setting successful. But can't get reply msg."
        puts $list_rtu
    } else {
        lappend list_rtu "Dls_5204_SetSelectedOutput" $result
        dls5204::perror "Dls_5204_SetSelectedOutput" $result
    }
    return $list_rtu
}
################################################################################
# Procedure : Dls_5204_EnableOutput
# Purpose   : Select the current channel to operate on.
################################################################################
proc dls5204::EnableOutput { channel channel_status } {
    set list_rtu {}
    
    # check if  connected
    if { ![dls5204::IsConnected] } {
        perror "Dls_5204_EnableOutput" "Not connected to unit."
    }
    
    # check for valid channel
    set tmp_channel [string toupper $channel]
    switch -- $tmp_channel {
        
        "CHANNEL_1" {
            set channel "OUTPUT_1"
            set chan_num 1
        }
        
        "CHANNEL_2" {
            set channel "OUTPUT_2"
            set chan_num 2
        }
        
        "CHANNEL_3" {
            set channel "OUTPUT_3"
            set chan_num 3
        }
        
        "CHANNEL_4" {
            set channel "OUTPUT_4"
            set chan_num 4
        }
        
        default {
            perror "Dls_5204_EnableOutput" "$channel is an invalid channel"
        }
    };#switch $tmp_channel
    
    # Check for a valid status
    set tmp_chan_status [string toupper $channel_status]
    if { $tmp_chan_status != "ON" && $tmp_chan_status != "OFF" } {
        lappend list_rtu "Dls_5204_EnableOutput" "Invalid status. Either \"ON\" or \"OFF\"."
        dls5204::perror "Dls_5204_EnableOutput" "Invalid status. Either \"ON\" or \"OFF\"."
        return $list_rtu
    }
    
    # send command
    dls5204::write_socket "!STX:SET(M_ENABLE_OUTPUT):VAL($channel:$tmp_chan_status);ETX!"
    
    ################################################
    #May092006 By Philip
    #Make sure Catch return msg from Noise Generator    
    ################################################
    set result [dls5204::CheckSetting]
    
    # check result
    if  { $result == "Successful." } {
        lappend list_rtu "Dls_5204_EnableOutput: Command successful. Channel $chan_num is now $tmp_chan_status."
        puts $list_rtu
    } elseif {$result == "Bypass"} {
        lappend list_rtu "Dls_5204_EnableOutput: Setting successful. But can't get reply msg."
        puts $list_rtu
    } else {
        lappend list_rtu "Dls_5204_EnableOutput" $result
        dls5204::perror "Dls_5204_EnableOutput" $result
    }
    
    return $list_rtu
}
################################################################################
# Procedure : Dls_5204_GenerateSample
# Purpose   : Select the current channel to operate on.
################################################################################
proc dls5204::GenerateSample { } {
    set list_rtu {}
    
    # check if  connected
    if { ![dls5204::IsConnected] } {
        dls5204::perror "Dls_5204_GenerateSample" "Not connected to unit."
    }
    
    dls5204::write_socket "!STX:SET(M_GENERATE_SAMPLE);ETX!"
    
    ################################################
    #May092006 By Philip
    #Make sure Catch return msg from Noise Generator
    ################################################
    set result [dls5204::CheckSetting]
    
    # check result
    if  { $result == "Successful." } {
        lappend list_rtu "Dls_5204_GenerateSample: Command successful."
        puts $list_rtu
    } elseif {$result == "Bypass" } {
        lappend list_rtu "Dls_5204_GenerateSample: Setting successful.  But can't get reply msg."
        puts $list_rtu
    } else {
        lappend list_rtu "Dls_5204_GenerateSample" $result
        dls5204::perror "Dls_5204_GenerateSample" $result
    }
    
    return $list_rtu
}
################################################################################
# Procedure : Dls_5204_LoadNoiseFile
# Purpose   : Selects a noise file to load.
################################################################################
proc dls5204::LoadNoiseFile { noise_type file_name } {
    set list_rtu {}
    
    # check if  connected
    if { ![dls5204::IsConnected] } {
        dls5204::perror "Dls_5204_LoadNoiseFile" "Not connected to unit."
    }
    
    # check file type
    set n_type [string toupper $noise_type]
    
    switch -- $n_type {
        
        "XTALK" {
            set n_type "M_LOAD_XTALK_FILE"
        }
        
        "RFI" {
            set n_type "M_LOAD_RFI_FILE"
        }
        {TDM} {
            set n_type "M_LOAD_FILE"
        }
        default {
            dls5204::perror "Dls_5204_LoadNoiseFile" "Invalid noise type \"XTALK\" or \"RFI\"."
        }
    };#switch $n_type
    
    # check for non null file name
    if { $file_name == "" } {
        lappend list_rtu "Dls_5204_LoadNoiseFile" "Invalid noise file."
        dls5204::perror "Dls_5204_LoadNoiseFile" "Invalid noise file."
        return $list_rtu
    }
    
    # send command
    dls5204::write_socket "!STX:SET($n_type):VAL($file_name);ETX!"
    
    ################################################
    #May092006 By Philip
    #Make sure Catch return msg from Noise Generator
    ################################################
    set result [dls5204::CheckSetting]

    if  { $result == "Successful." } {
        lappend list_rtu "Dls_5204_LoadNoiseFile: Command successful."
        puts $list_rtu
    } elseif {$result == "Bypass"} {
        lappend list_rtu "Dls_5204_LoadNoiseFile: Setting successful.  But can't get reply msg."
        puts $list_rtu
    } else {
        lappend list_rtu "Dls_5204_LoadNoiseFile" $result
        dls5204::perror "Dls_5204_LoadNoiseFile" $result
    }
    
    return $list_rtu
}
################################################################################
# Procedure : Dls_5204_Disconnect
# Purpose   : Disconnects the current connection.
################################################################################
proc dls5204::Disconnect { } {
    variable _sock_desc
    set list_rtu {}
    
    
    # check if  connected
    if { ![dls5204::IsConnected] } {
        lappend list_rtu "Dls_5204_Disconnect" "Not connected to unit."
        dls5204::perror "Dls_5204_Disconnect" "Not connected to unit."
    }
    
    if [info exists _sock_desc] {
        # close socket
        close $_sock_desc
        # unset _sock_desc

        lappend list_rtu "Dls_5204_Disconnect: Command successful."
        puts $list_rtu
    }
    
    return $list_rtu
}
################################################################################
# Procedure : Dls_5204_LoadOutput
# Purpose   : Loads the interpolated noise to the tality card ram.
# Precond   : Channel *MUST* be disabled prior to loading
################################################################################
proc dls5204::LoadOutput { } {
    set list_rtu {}
    
    # check if  connected
    if { ![dls5204::IsConnected] } {
        dls5204::perror "Dls_5204_LoadOutput" "Not connected to unit."
    }
    
    dls5204::write_socket "!STX:SET(M_LOAD_OUTPUT);ETX!"
    
    ################################################
    #May092006 By Philip
    #Make sure Catch return msg from Noise Generator
    ################################################
    set result [dls5204::CheckSetting]
    
    if  { $result == "Successful." } {
        lappend list_rtu "Dls_5204_LoadOutput: Command successful."
        puts $list_rtu
    } elseif {$result == "Bypass"} {
        lappend list_rtu "Dls_5204_LoadOutput: Setting successful.  But can't get reply msg."
        puts $list_rtu
    } else {
        lappend list_rtu "Dls_5204_LoadOutput" $result
        dls5204::perror "Dls_5204_LoadOutput" $result
    }
    return $list_rtu
}
################################################################################
# Procedure : Dls_5204_SetCrestFactor
# Purpose   : Sets or unsets the Crest factor
################################################################################
proc dls5204::SetCrestFactor { status } {
    set list_rtu {}
    
    # check if  connected
    if { ![dls5204::IsConnected] } {
        dls5204::perror "Dls_5204_SetCrestFactor" "Not connected to unit."
    }
    
    set status [string toupper $status]
    
    if { $status != "ON"  && $status != "OFF" } {
        lappend list_rtu "Dls_5204_SetCrestFactor" "Invalid status. Either \"ON\" or \"OFF\"."
        dls5204::perror "Dls_5204_SetCrestFactor" "Invalid status. Either \"ON\" or \"OFF\"."
        return $list_rtu
    }
    
    dls5204::write_socket "!STX:SET(M_CREST_FACTOR):VAL($status);ETX!"
    
    ################################################
    #May042006 By Philip
    #Make sure Catch return msg from Noise Generator
    ################################################
    set result [dls5204::CheckSetting]
    
    if  { $result == "Successful." } {
        lappend list_rtu "Dls_5204_SetCrestFactor:$status Command successful. Crest factor >= 5 is : $status."
        puts $list_rtu
    } elseif {$result == "Bypass"} {
        lappend list_rtu "Dls_5204_SetCrestFactor:$status Setting successful. But can't get reply msg."
        puts $list_rtu
    } else {
        lappend list_rtu "Dls_5204_SetCrestFactor" $result
        dls5204::perror "Dls_5204_SetCrestFactor" $result
    }
    
    return $list_rtu
}
################################################################################
# Procedure : Dls_5204_SetNumberOfSamples
# Purpose   : Sets or unsets the Crest factor
################################################################################
proc dls5204::SetNumberOfSamples { samples } {
    set list_rtu {}
    
    # check if  connected
    if { ![dls5204::IsConnected] } {
        dls5204::perror "Dls_5204_SetNumberOfSamples" "Not connected to unit."
    }
    
    # check for non null
    if { $samples == "" } {
        lappend list_rtu "Dls_5204_SetNumberOfSamples" "Invalid parameter."
        dls5204::perror "Dls_5204_SetNumberOfSamples" "Invalid parameter."
        return $list_rtu
    }
    
    dls5204::write_socket "!STX:SET(M_NUMBER_SAMPLES):VAL($samples);ETX!"
    
    ################################################
    #May092006 By Philip
    #Make sure Catch return msg from Noise Generator
    ################################################
    set result [dls5204::CheckSetting]
    
    if  { $result == "Successful." } {
        lappend list_rtu "Dls_5204_SetNumberOfSamples:$samples Command successful. Number of samples used: $samples."
        puts $list_rtu
    } elseif {$result == "Bypass"} {
        lappend list_rtu "Dls_5204_SetNumberOfSamples:$samples Setting successful. But can't get reply msg."
        puts $list_rtu
    } else {
        lappend list_rtu "Dls_5204_SetNumberOfSamples" $result
        dls5204::perror "Dls_5204_SetNumberOfSamples" $result
    }
    return $list_rtu
}
################################################################################
# Procedure : Dls_5204_SelectNoise
# Purpose   : Selects which noise(s) will be used to create the sample
################################################################################
proc dls5204::SelectNoise { noise_type noise_status } {
    set list_rtu {}
    
    # check if  connected
    if { ![dls5204::IsConnected] } {
        dls5204::perror "Dls_5204_SelectNoise" "Not connected to unit."
    }
    
    # check file type
    set n_type [string toupper $noise_type]
    switch -- $n_type {
        "XTALK" {
            set n_type "M_XTALK_MUTE"
        }
        
        "RFI" {
            set n_type "M_RFI_MUTE"
        }
        
        default {
            lappend list_rtu "Dls_5204_SelectNoise" "Invalid noise type \"XTALK\" or \"RFI\"."
            dls5204::perror "Dls_5204_SelectNoise" "Invalid noise type \"XTALK\" or \"RFI\"."
            return $list_rtu
        }
    };#switch $n_type
    
    set n_status [string toupper $noise_status]
    if { $n_status != "ON"  && $n_status != "OFF" } {
        lappend list_rtu "Dls_5204_SelectNoise" "Invalid status. Either \"ON\" or \"OFF\"."
        dls5204::perror "Dls_5204_SelectNoise" "Invalid status. Either \"ON\" or \"OFF\"."
        return $list_rtu
    }
    
    # switch noise
    if { $n_status == "ON" } {
        set real_n_status "OFF"
    } else {
        set real_n_status "ON"
    }
    dls5204::write_socket "!STX:SET($n_type):VAL($real_n_status);ETX!"
    

    #################################################
    #May092006 By Philip
    #Make sure Catch return msg from Noise Generator
    #################################################
    set result [dls5204::CheckSetting]
    
    if  { $result == "Successful." } {
        lappend list_rtu "Dls_5204_SelectNoise:$noise_status Command successful. $noise_type is $n_status."
        puts $list_rtu
    } elseif {$result == "Bypass"} {
        lappend list_rtu "Dls_5204_SelectNoise:$noise_status Setting successful. But can't ger reply msg."
        puts $list_rtu
    } else {
        lappend list_rtu "Dls_5204_SelectNoise" $result
        dls5204::perror "Dls_5204_SelectNoise" $result
    }
    return $list_rtu
}
################################################################################
# Procedure : PingTest
# Purpose   : Before connecting, check host if exists.
################################################################################
proc dls5204::PingTest {host} {
    catch {exec ping.exe $host -n 1} pingmsg
    set result [string first "Reply" $pingmsg]
    
    if { $result == -1} {
        puts "Ping $host no Reply \n"; update
        return false
    } else {
        puts "Ping $host OK \n"; update
        return true
    }
}

################################################################################
# 
################################################################################
proc dls5204::SetResetChannel {} {
    set list_rtu {}
    
    # check if  connected
    if { ![dls5204::IsConnected] } {
        dls5204::perror "Dls_5204_SetGain" "Not connected to unit."
    }
    
    set str_sckcmd "!STX:SET(M_RESET_CHANNEL);ETX!"
    dls5204::write_socket $str_sckcmd
    
    ################################################
    #May092006 By Philip
    #Make sure Catch return msg from Noise Generator
    ################################################
    set result [dls5204::CheckSetting]
    
    if  { $result == "Successful." } {
        lappend list_rtu "Dls_5204_SetResetChannel: Command successful."
        puts $list_rtu
    } elseif {$result == "Bypass"} {
        lappend list_rtu "Dls_5204_SetResetChannel: Setting successful.  But can't get reply msg."
        puts $list_rtu
    } else {
        lappend list_rtu "Dls_5204_SetGain" $result
        dls5204::perror "Dls_5204_SetGain" $result
    };#if  { $result == "Successful." }
    
    return $list_rtu
}
proc dls5204::SetInjectors {cmdopt comportnum} {
    set list_rtu {}
    
    # check if  connected
    if { ![dls5204::IsConnected] } {
        dls5204::perror "Dls_5204_SetInjectors" "Not connected to unit."
    }
    
    switch -regexp [string tolower $cmdopt] {
        {connect} {
            # check for non null
            if { $comportnum == "" } {
                lappend list_rtu "Dls_5204_SetTDWhiteNoise COM Port Number." "Invalid parameter."
                dls5204::perror "Dls_5204_SetTDWhiteNoise COM Port Number." "Invalid parameter."
                return $list_rtu
            }
            
            dls5204::write_socket "!STX:SET(M_INJ_CONNECT):VAL($comportnum);ETX!"
        }
        {disconnect} {
            dls5204::write_socket "!STX:SET(M_INJ_DISCONNECT):VAL($comportnum);ETX!"
        }
    };#switch -regexp [string tolower $cmdopt]
    
    
    ################################################
    #May092006 By Philip
    #Make sure Catch return msg from Noise Generator
    ################################################
    set result [dls5204::CheckSetting]
    
    switch -regexp [string tolower $cmdopt] {
        {connect} {
            if  { $result == "Successful." } {
                lappend list_rtu "Dls_5204_SetInjectors:$comportnum Command successful."
                puts $list_rtu
            } elseif {$result == "Bypass"} {
                lappend list_rtu "Dls_5204_SetInjectors:$comportnum Setting successful.  But can't get reply msg."
                puts $list_rtu
            } else {
                lappend list_rtu "Dls_5204_SetInjectors" $result
                dls5204::perror "Dls_5204_SetInjectors" $result
            };#if  { $result == "Successful." }
        }
        {disconnect} {
            if  { $result == "Successful." } {
                lappend list_rtu "Dls_5204_SetInjectors:Disconnect Command successful."
                puts $list_rtu
            } elseif {$result == "Bypass"} {
                lappend list_rtu "Dls_5204_SetInjectors:Disconnect Setting successful.  But can't get reply msg."
                puts $list_rtu
            } else {
                lappend list_rtu "Dls_5204_SetInjectors:Disconnect" $result
                dls5204::perror "Dls_5204_SetInjectors:Disconnect" $result
            };#if  { $result == "Successful." }
        }
    };#switch -regexp [string tolower $cmdopt]
    
    return $list_rtu
}
proc dls5204::LoadXtkWhiteNoise {flag whitenoise_opt} {
    set list_rtu {}
    set str_prochead "Dls_5204_SetXtkWhiteNoise"
    
    # check if  connected
    if { ![dls5204::IsConnected] } {
        dls5204::perror $str_prochead "Not connected to unit."
    }
    
    # check for non null
    if { $flag == "" } {
        lappend list_rtu $str_prochead "Invalid parameter."
        dls5204::perror $str_prochead "Invalid parameter."
        return $list_rtu
    }
    
    set status ""
    switch -regexp [string tolower $flag] {
        {enable} {
            set pathDLS5204 "C:\\Program Files\\Spirent Communications\\DLS 5204\\NoiseFiles\\"
            set pathxtkWhiteNoise ""; set xtkWhiteNoise ""
            # DLS5500 path TR-048_(Apr_2002)
            # DLS5204 path TR 048 (Apr 2002)
            ############################################ 
            # set path_pureWhiteNoise "DSL Forum\\TR-048_(Apr_2002)\\8-6-1_Bit_Swap_Tests\\Downstream_At_ATU-R\\"
            set path_pureWhiteNoise "DSL Forum\\WT-100\\8-1\\Upstream_At_ATU-C\\"
            set pureWhiteNoise    "White_Noise_xtk.enc"
            
            set path_ARCORWhiteNoise "ETSI\\ADSL_Over_ISDN_EC-ModFD_5B30v2-3-1\\White_Noise\\"
            set ARCORWhiteNoise    "White_Noise(EL100)_xtk.enc"
            
            set path_wt100_fbWhiteNoise "ETSI\\ADSL2PlusFB_5B30v2-3-1\\White_Noise\\"
            set wt100_fbWhiteNoise      "White_Noise(EL135)_xtk.enc"
            
            switch -regexp [string tolower $whitenoise_opt] {
                {arcor} {
                    set pathxtkWhiteNoise $path_ARCORWhiteNoise
                    set xtkWhiteNoise    $ARCORWhiteNoise
                }
                {wt100_fb} {
                    set pathxtkWhiteNoise    $path_wt100_fbWhiteNoise
                    set xtkWhiteNoise        $wt100_fbWhiteNoise
                }
                {pure} {
                    set pathxtkWhiteNoise $path_pureWhiteNoise
                    set xtkWhiteNoise    $pureWhiteNoise
                }
            };#switch -regexp [string tolwer $whitenoiseopt]
            
            append strpathWhiteNoise $pathDLS5204 $pathxtkWhiteNoise $xtkWhiteNoise
            
            set strtemp [dls5204::LoadNoiseFile TDM $strpathWhiteNoise]
            lappend list_rtu $strtemp
        }
        {disable} {
            return "LoadXtkWhiteNoise Option $flag"
        }
    };#switch -regexp [string tolower $flag]
    
    return $list_rtu
}
################################################################################
# WT100 for DLS5500 Time domain noise file 
################################################################################
proc dls5204::SetTDWhiteNoise {flag} {
    set list_rtu {}
    
    # check if  connected
    if { ![dls5204::IsConnected] } {
        dls5204::perror "Dls_5204_SetTDWhiteNoise" "Not connected to unit."
    }
    
    # check for non null
    if { $flag == "" } {
        lappend list_rtu "Dls_5204_SetTDWhiteNoise" "Invalid parameter."
        dls5204::perror "Dls_5204_SetTDWhiteNoise" "Invalid parameter."
        return $list_rtu
    }
    
    set status ""
    switch -regexp [string tolower $flag] {
        {enable} {
            set status "ON"
        }
        {disable} {
            set status "OFF"
        }
    };#switch -regexp [string tolower $flag]
    
    dls5204::write_socket "!STX:SET(M_TD_WHITENOISE):VAL($status);ETX!"
    
    ################################################
    #May092006 By Philip
    #Make sure Catch return msg from Noise Generator
    ################################################
    set result [dls5204::CheckSetting]
    
    if  { $result == "Successful." } {
        lappend list_rtu "Dls_5204_SetTDWhiteNoise:$status Command successful."
        puts $list_rtu
    } elseif {$result == "Bypass"} {
        lappend list_rtu "Dls_5204_SetTDWhiteNoise:$status Setting successful.  But can't get reply msg."
        puts $list_rtu
    } else {
        lappend list_rtu "Dls_5204_SetTDWhiteNoise" $result
        dls5204::perror "Dls_5204_SetTDWhiteNoise" $result
    };#if  { $result == "Successful." }
    
    return $list_rtu
}
proc dls5204::_NoiseGen_XtkWhiteNoise {strchannelnum strnoisefile strendiswhitenoise GUIflag} {
    set strtemp [dls5204::SetSelectedChannel $strchannelnum]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    set    strtemp [SetResetChannel]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    set strtemp [dls5204::SelectNoise XTALK ON]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    # ##lappend list_rtumsg [dls5204::SelectNoise RFI OFF]
    set strtemp [dls5204::LoadNoiseFile XTALK $strnoisefile]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    # after [expr 1000 * 2]
    
    ###################################################
    ###FDD_AI_ModFB_C_ETSI_Lp1_2750m_xtk.enc,FDD_AI_ModFB_R_ETSI_Lp1_2750m_xtk.enc
    ###ETSI-ADSL2PlusFB-B_C_Lp1_0500m_xtk.enc, ETSI-ADSL2PlusFB-B_R_Lp1_0500m_xtk.enc
    ###################################################
    switch -regexp $strnoisefile {
        {FDD_AI_ModFB_(C|R)_ETSI_Lp1_2750m_xtk.enc} {
            set whitenoise_opt "arcor"
        }
        {ETSI-ADSL2PlusFB-B_(C|R)_Lp1_[0-9][0-9][0-9][0-9]m_xtk.enc} {
            set whitenoise_opt "wt100_fb"
        }
        default {
            set whitenoise_opt "pure"
        }
    };#switch -regexp $strnoisefile
    
    #####Set XtkWhiteNoise for WT100 on DLS5500 or DLS5204
    set strtemp [dls5204::LoadXtkWhiteNoise $strendiswhitenoise $whitenoise_opt]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    set strtemp [dls5204::SetCrestFactor ON]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    set strtemp [dls5204::SetNumberOfSamples 131072]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    set strtemp [dls5204::GenerateSample]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    set strtemp [dls5204::LoadOutput]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    set strtemp [dls5204::EnableOutput $strchannelnum ON]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    set strtemp [dls5204::GetChannelFileName $strchannelnum]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    return $list_rtumsg
}
proc dls5204::_NoiseGen_TDWhiteNoise {comportnum strchannelnum strnoisefile strendiswhitenoise \
            GUIflag} {
        
   # set    strtemp [dls5204::SetInjectors "disconnect" $comportnum]
   # if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
   # lappend list_rtumsg $strtemp
        
    # set    strtemp [dls5204::SetInjectors "connect" $comportnum]
    # if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    # lappend list_rtumsg $strtemp
    
    set strtemp [dls5204::SetSelectedChannel $strchannelnum]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    set    strtemp [SetResetChannel]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    # ##lappend list_rtumsg [dls5204::SelectNoise RFI OFF]
    set strtemp [dls5204::LoadNoiseFile TDM $strnoisefile]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    # after [expr 1000 * 2]
    
    #####Set TDWhiteNoise for WT100 on DLS5500
    set strtemp [dls5204::SetTDWhiteNoise $strendiswhitenoise]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    set strtemp [dls5204::SetCrestFactor ON]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    # set strtemp [dls5204::SetNumberOfSamples 131072]
    # if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    # lappend list_rtumsg $strtemp
    
    set strtemp [dls5204::GenerateSample]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    set strtemp [dls5204::LoadOutput]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    set strtemp [dls5204::EnableOutput $strchannelnum ON]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    set strtemp [dls5204::GetChannelFileName $strchannelnum]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    return $list_rtumsg
}
################################################################################
# Jan04-2007 For Noise Marign test
################################################################################
proc dls5204::_NoiseMicroGain {strchannelnum microgain_value logfilename GUIflag} {
    set strtemp [dls5204::SetSelectedChannel $strchannelnum]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    ###Set MicroGain
    ###@namespace dls5500
    set strtemp [dls5204::SetMicroGain $microgain_value]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    return $list_rtumsg
}
################################################################################
# Jun27 Add proc SetResetChannel
# Jul05 Correct procedure is SetSelectedChannel first and SetResetChannel ending
################################################################################
proc dls5204::_NoiseGen {strchannelnum strnoisefile strnoise_compen \
            GUIflag} {
    
    set strtemp [dls5204::SetSelectedChannel $strchannelnum]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    set    strtemp [SetResetChannel]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    set strtemp [dls5204::SelectNoise XTALK ON]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    # ##lappend list_rtumsg [dls5204::SelectNoise RFI OFF]
    set strtemp [dls5204::LoadNoiseFile XTALK $strnoisefile]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    ;# <--------  Spirent Measured Insertion Loss
    # set strtemp [dls5204::SetGain XTALK $strnoise_compen]
    # if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    # lappend list_rtumsg $strtemp
    
    set strtemp [dls5204::SetCrestFactor ON]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    set strtemp [dls5204::SetNumberOfSamples 131072]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    set strtemp [dls5204::GenerateSample]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    # set strtemp [dls5204::EnableOutput $strchannelnum OFF]
    # if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    # lappend list_rtumsg $strtemp
    
    set strtemp [dls5204::LoadOutput]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    set strtemp [dls5204::EnableOutput $strchannelnum ON]
    if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    lappend list_rtumsg $strtemp
    
    # set strtemp [SetResetChannel]
    # if {$GUIflag == "on"} { insertLogLine info $strtemp};#if {$GUIflag == "on"}
    # lappend list_rtumsg $strtemp
    
    return $list_rtumsg
}

################################################################################
# 
################################################################################
proc dls5204::_NoiseGen_StepLog {strchannelnum strnoisefile \
            strnoise_compen logfilename} {
            
    set list_rtumsg [dls5204::SetSelectedChannel $strchannelnum]
    Log::tofile "info" $logfilename $list_rtumsg
    
    set list_rtumsg [dls5204::SelectNoise XTALK ON]
    Log::tofile "info" $logfilename $list_rtumsg
    
    ### set list_rtumsg [dls5204::SelectNoise RFI OFF]
    ### Log "info" $logfilename $list_rtumsg
    
    set list_rtumsg [dls5204::LoadNoiseFile XTALK $strnoisefile]
    Log::tofile "info" $logfilename $list_rtumsg
    ;# <--------  Spirent Measured Insertion Loss
    # set list_rtumsg [dls5204::SetGain XTALK $strnoise_compen]
    # Log::tofile "info" $logfilename $list_rtumsg
    
    set list_rtumsg [dls5204::SetCrestFactor ON]
    Log::tofile "info" $logfilename $list_rtumsg
    
    set list_rtumsg [dls5204::SetNumberOfSamples 131072]
    Log::tofile "info" $logfilename $list_rtumsg
    
    #Need more time to gernerate sample
    #    set dls5204::_timeout $longtimeout
    set list_rtumsg [dls5204::GenerateSample]
    Log::tofile "info" $logfilename $list_rtumsg
    
    # set list_rtumsg [dls5204::EnableOutput $strchannelnum OFF]
    # Log::tofile "info" $logfilename $list_rtumsg
    
    set list_rtumsg [dls5204::LoadOutput]
    Log::tofile "info" $logfilename $list_rtumsg
    
    set list_rtumsg [dls5204::EnableOutput $strchannelnum ON]
    return $list_rtumsg
            
}
################################################################################
# Procedure : CheckSetting
# Purpose   : Check setting if sucessful.
################################################################################
proc dls5204::CheckSetting {} {
    # get reply
    set result [dls5204::read_socket]
    
    set cnt 0
    while {$result == "false"} {
        set x 0; after 100 {set x 1}; vwait x
        set result [dls5204::read_socket]
        incr cnt
        if {$cnt > $dls5204::_retrytimes} {
            set result "Bypass"
            break
        }
    };#while {$result == "false"}
    
    return $result
}
################################################################################
# 
################################################################################
namespace eval dls5500 {} {
    # for debugging
    variable verbose off

    #noise file of prvious test case
    variable csv_nosiefname ""
    variable csv_nosiefname_id00184 ""
    variable list_CSVValue {}
}
################################################################################
# Procedure : Dls55500_SetNoiseGain
# Purpose   : Adapt from DLS5500 v1.0
################################################################################
proc dls5500::SetNoiseGain {gain_value } {
    set list_rtu {}
    
    # check if  connected
    if { ![dls5204::IsConnected] } {
        dls5204::perror "Dls_5500_SetNoiseGain" "Not connected to unit."
    }
    
    # check for non null parameters
    if { $gain_value == "" } {
        lappend list_rtu "Dls_5500_SetNoiseGain" "Invalid parameters."
        dls5204::perror "Dls_5500_SetNoiseGain" "Invalid parameters."
        return $list_rtu
    }
    
    dls5204::write_socket "!STX:SET(M_NOISE_GAIN):VAL($gain_value);ETX!"
    
    ################################################
    #May092006 By Philip
    #Make sure Catch return msg from Noise Generator
    ################################################
    set result [dls5204::CheckSetting]
    
    switch -regexp [string tolower $result]  {
        {succeeded$} {
            lappend list_rtu "Dls_5500_SetNoiseGain: Command successful. $gain_type set to $gain_value dBm."
            puts $list_rtu
        }
        default {
            lappend list_rtu "Dls_5500SetNoiseGain" $result
            dls5204::perror "Dls_5500_SetNoiseGain" $result
        }
    };#switch -regexp [string tolower $result]
    
    return $list_rtu
}
################################################################################
# Procedure : Dls_5204_SetMicroGain
# Purpose   : Adapt from DLS5500 v1.0
################################################################################
proc dls5500::SetMicroGain {gain_value } {
}

proc dls5500::Run {outputch noisefname noiseCompensation {logfile ""} {GUIflag ""}} {
    set chnum "Channel_$outputch"
    #puts "Setting white noise on $chnum"
    
    set list_rtumsg [dls5204::_NoiseGen $chnum $noisefname $noiseCompensation \
                    $GUIflag]
    if { $logfile != ""} {
        Log "info" $logfile $list_rtumsg
    };#if { $logfile != ""}
}
proc dls5500::Run_EnDisXtkWhiteNoise {outputch noisefname tdwhitenoise {logfile ""} {GUIflag ""}} {
    set chnum "Channel_$outputch"
    #puts "Setting white noise on $chnum"
    
    set list_rtumsg [dls5204::_NoiseGen_XtkWhiteNoise $chnum $noisefname $tdwhitenoise \
            $GUIflag]
    
    if { $logfile != ""} {
        Log "info" $logfile $list_rtumsg
    };#if { $logfile != ""}
    
    return $list_rtumsg
}
proc dls5500::Run_EnDisTDWhiteNoise {outputch noisefname tdwhitenoise {logfile ""} {GUIflag ""}} {
    set chnum "Channel_$outputch"
    #puts "Setting white noise on $chnum"
    set comportnum "2"
    
    set list_rtumsg [dls5204::_NoiseGen_TDWhiteNoise $comportnum $chnum $noisefname $tdwhitenoise \
            $GUIflag]

    if { $logfile != ""} {
        Log "info" $logfile $list_rtumsg
    };#if { $logfile != ""}
    
    return $list_rtumsg
}
proc dls5500::Log {level filename list_msg} {
    foreach temp $list_msg {
        set msg "$temp"
        Log::tofile $level $filename $msg
    }
}

namespace eval NoiseCommon {} {
    variable list_noisefile_ChNum
    variable verbose    "off"
    variable logfile    "../noisecommon.log"
}

################################################################################
# list row content as below
# {0037 A_1_4_2-AWGN AU_RA_I_30000k 26AWG 100 0 DLS5500  -na-    -na-    White_Noise_td.enc  White_Noise_td.enc}
#    0    1            2            3     4   5 6        7(ch1)   8(ch2)          9(ch3)        10(ch4)
################################################################################
proc NoiseCommon::inital {rowvalue} {
    variable list_noisefile_ChNum
    set list_noisefile_ChNum {}
    
    # puts "$list_value in proc NoiseCommon::inital"
    set startidx 7
    set list_prvious_noisefname {}
    lset list_prvious_noisefname [lrange $rowvalue $startidx [expr $startidx + 3]]
            
    lset list_noisefile_ChNum $list_prvious_noisefname
    return true
}
################################################################################
# Refer demo sample code diff2.tcl
################################################################################
proc NoiseCommon::DiffList {rowvalue logfile} {
    package require struct
    
    set startidx 7
    set list_current_noisepathfname {}
    
    lappend list_current_noisepathfname [lindex $rowvalue $startidx] \
            [lindex $rowvalue [expr $startidx + 1]] \
            [lindex $rowvalue [expr $startidx + 2]] \
            [lindex $rowvalue [expr $startidx + 3]]
    
    Log "info" $logfile [list $list_current_noisepathfname $NoiseCommon::list_noisefile_ChNum]
    
    set lines1 {}; set lines2 {}
    lset lines1 $list_current_noisepathfname
    lset lines2 $NoiseCommon::list_noisefile_ChNum
    
    ::struct::list assign [::struct::list longestCommonSubsequence $lines1 $lines2] x1 x2
    
    foreach chunk [::struct::list lcsInvertMerge2 $x1 $x2 [llength $lines1] [llength $lines2]] {
        Log "info" $logfile [list "===========================================" \
                $chunk "-------------------------------------------"]
        
        ::struct::list assign [lindex $chunk 1] b1 e1
        ::struct::list assign [lindex $chunk 2] b2 e2
        switch -exact -- [lindex $chunk 0] {
            changed {
                Log "info" $logfile [list "< [join [lrange $lines1 $b1 $e1]]" \
                        "---" "> [join [lrange $lines2 $b2 $e2]]"]
                Log "info" $logfile [list "==========================================="]
                return [lindex $chunk 0];#add Sep08-2006 by Philip
                
            }
            added   {
                Log "info" $logfile [list "> [join [lrange $lines2 $b2 $e2]]" ]
                Log "info" $logfile [list "==========================================="]
                return [lindex $chunk 0];#add Sep27-2006 by Philip
            }
            deleted {
                Log "info" $logfile [list "< [join [lrange $lines1 $b1 $e1]]" ]
                Log "info" $logfile [list "==========================================="]
                return [lindex $chunk 0];#add Sep27-2006 by Philip
            }
        };#switch -exact -- [lindex $chunk 0]
    };#foreach chunk [::struct::list lcsInvertMerge2 $x1 $x2 [llength $lines1] [llength $lines2]]
    
    Log "info" $logfile [list "==========================================="]
    
    if {($b1==$b2) && ($e1==$e2)} {
        return [lindex $chunk 0];#return "unchanged"
    }
    return true
}
proc NoiseCommon::_SetMicroGainStep {microgain_value rowvalue logfile} {
    
    set list_noisefname [lrange $rowvalue 7 10]
    set chnumidx 1
    foreach str_noisefname $list_noisefname {
        switch -regexp $str_noisefname {
            {_(xtk|td)} {
                incr chnumidx
                break
            }
            incr chnumidx
        };#switch -regexp $str_noisefname
    }
    
    set chnum "Channel_$chnumidx"
    puts "Noise Channel of CPE side is $chnum"
    
    set list_rtumsg [dls5204::_NoiseMicroGain $chnum $microgain_value $logfile "on"]
    
    return $list_rtumsg
}
proc NoiseCommon::_SendCmd {strnoise_diff rowvalue list_noisepathfname logfile} {
################################################################################
# rowvalue list format as below:
# ID,table,testprofile,looptype,looplength,taplength,noise,noise_Ch1,noise_Ch2,noise_Ch3,noise_Ch4,
#  0   1       2         3            4        5        6    7        8            9          10 
# microgain_Ch1,microgain_Ch2,microgain_Ch3,microgain_Ch4,
#        11            12            13            14            
# EnWhiteNoise_Ch1,EnWhiteNoise_Ch2,EnWhiteNoise_Ch3,EnWhiteNoise_Ch4
#         15                16            17            18
# ################################################################################
    if {$strnoise_diff == "unchanged"} {
        set strmsg "Previous and current noise profile are the same."
        
    } else  {
        Func_CSV::_List_NoisePathFname $rowvalue $list_noisepathfname
        set strmsg $Func_CSV::list_noisepathfname_ChNum
        
        set chnumidx 1;set Compen 0;#37.5
        foreach str_noisepathfname $Func_CSV::list_noisepathfname_ChNum {
            switch -regexp $str_noisepathfname {
                {_xtk} {
                    # dls5500::Run $chnumidx $str_noisepathfname \
                    # $Compen $logfile "on"
                    set enWhiteNoiseidx [expr 14 + $chnumidx]
                    set flag_WhiteNoise [lindex $rowvalue $enWhiteNoiseidx]
                    dls5500::Run_EnDisXtkWhiteNoise $chnumidx $str_noisepathfname \
                            $flag_WhiteNoise $logfile "on"
                }
                {_td} {
                    set enWhiteNoiseidx [expr 14 + $chnumidx]
                    set flag_WhiteNoise [lindex $rowvalue $enWhiteNoiseidx]
                    dls5500::Run_EnDisTDWhiteNoise $chnumidx $str_noisepathfname \
                            $flag_WhiteNoise $logfile "on"
                }
            };#switch -regexp $str_noisepathfname
            
            incr chnumidx
        };#foreach str_noisepathfname
    };#if {$retval == false}
    
    return $strmsg
}
