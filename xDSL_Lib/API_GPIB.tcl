# ChangeLog
# By Philip Shen @PQA TrendChip Corp.
# ---------------------------------------------------------------------
#Apr19-2006 Initialization and plan for remote control purpose.
#Apr26-2006 Add proc TC_GPIB::write_listcmd
#Apr28-2006 Add proc both TC_GPIB::DispGPIBMsg and TC_GPIB::DispMsg
#Oct19-2006 Add proc TC_GPIB::ChkGPIBStatus to check "*ESR?" "*OPC?"
#Oct20-2006 Modify proc TC_GPIB::write_listcmd to meet DLS410A utility porcedure
################################################################################

set currpath [file dirname [info script]]
lappend auto_path $currpath
package require gpib

namespace eval TC_GPIB {
    variable verbose off
    variable logfile "../log/gpib.log"
}

proc TC_GPIB::opendevice {{list_gpibaddr {}}} {
    #On, failed GPIB operations will throw an exception and return detailed information.
    gpib interface exceptions on
    
    ################################################################################
    # Build open device number list.
    ###############################################################################
    set list_opendevice [list ]
    foreach  idx_gpibaddr $list_gpibaddr {
        gpib open -address $idx_gpibaddr -timeout 5;#
    }
    foreach dev_num [gpib list] {
        # puts "Device Number=$dev_num"
        lappend list_opendevice $dev_num
    }
    ################################################################################
    #If no listen devices exist,
    # then clear and close all open devices then return false
    ################################################################################
    foreach idx $list_opendevice {
        # gpib select -device $idx
        catch {gpib write -device $idx -message "*IDN?"} errmsg
        ################################################################################
        #GPIB interface error - ENOL - No listeners on the GPIB
        ################################################################################
        if {([string first "ENOL" $errmsg] > 0) || \
                    ($errmsg == "Device not found")} {
            clear_close_opendevice $list_opendevice
            return false
        }
    }
    
    return $list_opendevice
}

proc TC_GPIB::close {{list_opendevice {}} } {
        
    if {$list_opendevice != {}} {
        foreach idx_opendevice $list_opendevice {
            # gpib select -device $idx_opendevice
            gpib close -device $idx_opendevice
        }
    } else  {
        return false
    }
    
    return true
}

proc TC_GPIB::clear {{list_opendevice {}}} {
    
    if {$list_opendevice != {}} {
        foreach idx_opendevice $list_opendevice {
            # gpib select -device $idx_opendevice
            gpib clear -device $idx_opendevice
        }
    } else  {
        return false
    }
    
    return true
}
################################################################################
# Purpose:
#         opendevice handle is just only one;Cmd input type is list
# Sep16-2006 Ask error msg each write GPIB Cmd
################################################################################
proc TC_GPIB::write_listcmd {{str_opendevice ""} {list_cmd {}} } {
    variable verbose
    variable logfile
        
    if {$str_opendevice != ""} {
        if {$list_cmd != {}} {
            # gpib select -device $str_opendevice
            
            foreach idx_listcmd $list_cmd {
                gpib select -device $str_opendevice
                gpib write -message $idx_listcmd
                
                if {$verbose == on} {
                    Log "info" $logfile [list "Select_Device=$str_opendevice" \
                                    "GPIN_Cmd=$idx_listcmd" ]
                };#if {$verbose == on}
                
                DispMsg [list "Select_Device=$str_opendevice"] [list "GPIN_Cmd=$idx_listcmd"]
                                
                ###Ask return error msg
                gpib write -message *ESR?
                ####Follow DLS410 utility
                # gpib serialpoll
                
                set str_msg [gpib read -mode ascii]
                if [info exist err_msg] {
                    unset err_msg
                }
                append err_msg "ErrorMsg" $str_msg
                DispMsg [list "Select_Device=$str_opendevice"] [list "ErrorMsg=$str_msg"]
                        
                if {$verbose == on} {
                    Log "info" $logfile [list "Select_Device=$str_opendevice"  \
                            "ErrorMsg=$str_msg"]
                };#if {$verbose == on}
                
                ###Waiting
                # gpib wait -event srq
                
                if {[string length $str_msg] == 0} {
                    return false
                };#if {$str_msg != "0"}
                                
                # TC_GPIB::ChkGPIBStatus $str_opendevice "*ESR?"
                # TC_GPIB::DispGPIBMsg [list $str_opendevice]
            };#foreach idx_listcmd $list_cmd
            
        } else  {
            return false
        };#if {$list_cmd != {}}
    } else  {
        return false
    };#if {$str_opendevice != ""}
    
    ###Ask Opc msg
    gpib write -message *OPC?
    
    ###Waiting
    # gpib wait -event srq
     
    ####Follow DLS410 utility
    gpib serialpoll
    
    # set str_msg [gpib read -mode ascii]
    # append opc_msg "OPCMsg" $str_msg
    # DispMsg [list "Select_Device=$str_opendevice"] [list "OPCMsg=$str_msg" ]
    
    # if {$verbose == on} {
        # Log "info" $logfile [list "Select_Device=$str_opendevice" \
                # "OPCMsg=$str_msg" ]
    # };#if {$verbose == on}
    
    # if {$str_msg != "0"} {
        # return false
    # };#if {$str_msg != "0"}
    
    after [expr 1000 * 2]
    return true
}

proc TC_GPIB::write {{list_opendevice {}} {str_cmd ""}} {

    if {$list_opendevice != {}} {
        foreach idx_opendevice $list_opendevice {
            if {$str_cmd != ""} {
                gpib select -device $idx_opendevice
                gpib write -message $str_cmd
            } else  {
                return false
            };#if {$str_cmd != ""}
        };#foreach idx_opendevice $list_opendevice
    } else {
        return false
    };#if {$list_opendevice != {}}
    
    return true
}

proc TC_GPIB::read {list_opendevice } {
    
    if {$list_opendevice != {}} {
        foreach idx_opendevice $list_opendevice {
            gpib select -device $idx_opendevice
            set str_rtu [gpib read -mode ascii]
            lappend list_rtumsg $str_rtu
        }
        
    } else {
        return false
    };#if {$list_opendevice != {}}
    
    return $list_rtumsg
}

proc TC_GPIB::clear_close_opendevice {{list_opendevice {}}} {
    if {$list_opendevice != {}} {
        foreach idx_opendevice $list_opendevice {
            gpib clear -device $idx_opendevice
            gpib close -device $idx_opendevice
        }
    };#if {$list_opendevice != {}}
}

proc TC_GPIB::clear_existed_device {} {
    
    set list_openeddevic [gpib list]
    
    if {$list_openeddevic != "No devices"} {
        set retval1 [clear $list_openeddevic]
        set retval2 [close $list_openeddevic]
        if {($retval1 && $retval2)} {
            return true
        } else  {
            return false
        }
    } else  {
        return true
    };#if {$list_openeddevic != "No devices"}
}

proc TC_GPIB::DispMsg {listdev listmsg} {
    foreach x  $listdev y $listmsg {
        puts "Device$x Msg is $y"
    }
}
proc TC_GPIB::ChkGPIBStatus {strdev stropt} {
    variable verbose
    variable logfile
    
    set x $strdev
    set list_rtumsg {}
    
    # TC_GPIB::write $x $StrTmp1
    TC_GPIB::write $x $stropt
    set msg [TC_GPIB::read $x]
    DispMsg [list "In TC_GPIB::ChkGPIBStatus; $x $stropt"] $msg
    lappend list_rtumsg "Device$x Msg:$msg"
        
    if {$verbose == on} {
        Log "info" $logfile [list "Select_Device=" $x "Return Msg=" $$msg]
    };#if {$verbose == on}
    
    return $list_rtumsg
}

proc TC_GPIB::DispGPIBMsg {listdev} {
    variable verbose
    variable logfile
    
    # set StrTmp1 ":SET :CHANNEL :BYPASS NO"
    set list_rtumsg {}
    
    foreach x $listdev {
        # TC_GPIB::write $x $StrTmp1
        TC_GPIB::write $x "*ESR?"
        set msg [TC_GPIB::read $x]
        DispMsg $x $msg
        lappend list_rtumsg "Device$x Msg:$msg"
        
        if {$verbose == on} {
            Log "info" $logfile [list "Select_Device=" $x \
                    "Return Msg=" $$msg]
        };#if {$verbose == on}
     }
    
    return $list_rtumsg
}
proc TC_GPIB::Log {level filename list_msg} {
    foreach temp $list_msg {
        set msg "$temp"
        Log::tofile $level $filename $msg
    }
}

########################################
# Define a simple custom logproc
##########################################
namespace eval Log {
    package require logger
}

# Define a simple custom logproc
proc Log::log_to_file {lvl logfiletxt} {
    # set logfile "mylog.log"
    set msg "\[[clock format [clock seconds] -format "%H:%M:%S %m%d%Y"]\] \
            [lindex $logfiletxt 1]"
    set f [open [lindex $logfiletxt 0] {WRONLY CREAT APPEND}] ;# instead of "a"
    fconfigure $f -encoding utf-8
    puts $f $msg
    close $f
}
################################################################################
#${log}::logproc level
# ${log}::logproc level command
# ${log}::logproc level argname body
# This command comes in three forms - the third, older one is deprecated
# and may be removed from future versions of the logger package.
# The current set version takes one argument, a command to be executed
# when the level is called. The callback command takes on argument,
# the text to be logged. If called only with a valid level logproc returns
# the name of the command currently registered as callback command.
# logproc specifies which command will perform the actual logging for a given level.
# The logger package ships with default commands for all log levels,
# but with logproc it is possible to replace them with custom code.
# This would let you send your logs over the network, to a database, or anything else.
################################################################################
proc Log::tofile {level strfilename strtext} {
    # Initialize the logger
    set log [logger::init global]
    
    # Install the logproc for all levels
    foreach lvl [logger::levels] {
        interp alias {} log_to_file_$lvl {} Log::log_to_file $lvl
        ${log}::logproc $lvl log_to_file_$lvl
    }
    
    ${log}::$level [list $strfilename $strtext]
    
    return true
}

proc Log::parray {a {pattern *}} {
    upvar 1 $a array
    if ![array exists array] {
        error "\"$a\" isn't an array"
    }
    set maxl 0
    foreach name [lsort [array names array $pattern]] {
        if {[string length $name] > $maxl} {
            set maxl [string length $name]
        }
    }
    set maxl [expr {$maxl + [string length $a] + 2}]
    foreach name [lsort [array names array $pattern]] {
        set nameString [format %s(%s) $a $name]
        puts stdout [format "%-*s = %s" $maxl $nameString $array($name)]
    }
}

proc Log::LogOut {str_out logfname} {
    
    set path_logfile [file join ".." "log" $logfname]
    # set clk [clock format [clock seconds] -format "%b%d%H%M%S%Y"]
    # append str_out;# "  ";append Str_Out $clk
    
    set rnewinfd [open $path_logfile a]
    puts $rnewinfd $str_out
    flush $rnewinfd;close $rnewinfd
}