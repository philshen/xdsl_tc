#-------------------------------------------------------------------------------------------------------------#
# API source code for xDSL Test                                
#                                                                              
#  Simple test written by Philip Shen  4/8/06                      
# Purpose: For xDSL auto test functionality:                   
# 
#
#------------------------------------------------------------------------------------------------------------#

#-------------------------------------------------------------------------------------------------------------#
# class Show_Outcomes
#                                                                              
# Purpose: Output execute results to screen
#
# Sep8/2005  Add logger library
#------------------------------------------------------------------------------------------------------------#
class Show_Outcomes
require 'logger'

  def initialize(str_out,logfile=nil)
    @str_out=str_out
    @str_time=Time.now
    @strtemp=String.new
    @log = Logger.new(STDERR)
    @logfile = logfile
    @log_file = Logger.new(@logfile)
  end
  
  def show_results()
    @strtemp<<@str_time.strftime(" %m/%d/%Y %H:%M:%S")
    @strtemp<<@str_out
    puts @strtemp
  end

  def log_screen()
    @log.level = Logger::INFO#DEBUG	,Default.
    @log.datetime_format = "%d%b%Y@%H:%M:%S"
    do_log_screen
  end
  
  def do_log_screen()
#  @log.debug('do_log1') { "debug" }
    @log.info { @str_out }
#  @log.warn('do_log3') { "warn" }
#  @log.error('do_log4') { "error" }
#  @log.fatal('do_log6') { "fatal" }
#  @log.unknown { @str_out }
  end

  def log_logfile()
    @log_file.level = Logger::INFO#DEBUG	,Default.
    @log_file.datetime_format = "%d%b%Y@%H:%M:%S"
    do_log_logfile
  end

  def do_log_logfile()
    @log_file.info { @str_out }
  end
#  def teardown
#    @str_out=nil
#    @str_time=nil
#    @strtemp=nil
#  end
  
end
#-------------------------------------------------------------------------------------------------------------#
# class Show_Outcomes
#                                                                              
# Purpose: Output execute results to screen    
#------------------------------------------------------------------------------------------------------------#
class Dump_File
  def initialize(str_outfile,strContent)
    @str_outfile=str_outfile
    @strContent=strContent
    #@strtemp=String.new
    @hFile=File.new(@str_outfile,'w')
  end
  
  def dump_results()
    #@strtemp<<@strCnt
    @hFile.write(@strContent)
    @hFile.close
  end

end

#-------------------------------------------------------------------------------------------------------------#
# class Dump_DUT_SysInfo
#                                                                              
# Purpose: Dump SysInfo.htm to httpinfo.txt
#------------------------------------------------------------------------------------------------------------#
class Dump_DUT_SysInfo
  def initialize(host, target,method = 'GET',username = 'admin', passwd = 'admin')
	@host=host
	@method=method
	@target=target
	@username=username
	@passwd=passwd
	@syscmd=String.new
  end
  def dump_sysinfo_file()
    @syscmd =	"httpauthpntEX.exe "
    @syscmd <<	@host
    @syscmd <<	' '
    @syscmd <<	@method
    @syscmd <<	' '
    @syscmd <<	@target
    @syscmd <<	' '
    @syscmd <<	'"'
    @syscmd <<	'"'
    @syscmd <<	' '
    @syscmd <<	@username
    @syscmd <<	' '
    @syscmd <<	@passwd
    begin
	 system(@syscmd)
    rescue
	return    
    end
    
    return
    #puts "#{@syscmd}"
  end
  
  def dump_sysinfo_Telco()
    @syscmd =	"httpauthpntEX.exe "
    @syscmd <<	@host
    @syscmd <<	' '
    @syscmd <<	@method
    @syscmd <<	' '
    @syscmd <<	@target+' '+"sysinfo"
    @syscmd <<	' '
    @syscmd <<	@username+' '+@passwd
    
    begin
	 system(@syscmd)
    rescue
      return    
    end
  end
  
  def teardown
    @host=nil
    @method=nil
    @target=nil
    @username=nil
    @passwd=nil
    @syscmd=nil
  end

end
#-------------------------------------------------------------------------------------------------------------#
# class Get_Text_DUTSysInfo
#                                                                              
# Purpose: Return spcific string to act as dirctory that stores results
#------------------------------------------------------------------------------------------------------------#
class Get_Text_DUTSysInfo
  def initialize(basename,srcfile)
	@line=String.new
	@basename=basename
	@srcfile=srcfile
	@httpinfo_file=File.join(@basename,@srcfile)
	@hFile=File.new(@httpinfo_file,'r')
	@str_mode=String.new
	@str_firmware=String.new
	@str_country=String.new
	@str_return=String.new
  end

  def filter_results
	return @line[(@line.index(':')-@line.length+1) .. -2] 
  end
  
  def get_results
	 @hFile.each_line {|@line| 
			if !(@line.empty?) then
				case @line
					when /^(m|M)ode*/
					@str_mode=filter_results()
					when /^(f|F)irm*/
					@str_firmware=filter_results()
					when /^(c|C)ountry*/
					@str_country=filter_results()
				end
			end
		}
	@str_return=@str_mode+'-'+@str_country+@str_firmware
	return @str_return[0..(@str_return.index(' ')-@str_return.length-1)]
  end	
  
end
#-------------------------------------------------------------------------------------------------------------#
# class MkDir_webpage
#                                                                              
# Purpose: Makek dirctory to stores results
#------------------------------------------------------------------------------------------------------------#
class MkDir_webpage
  def initialize(what)
	@what=what
  end
  def chk_existence
	begin
		Dir.entries(@what) 
	rescue SystemCallError
		Dir.mkdir(@what) 
		Show_Outcomes.new(" Make directory #{@what}.").show_results()	
	end
	
  end
end
#-------------------------------------------------------------------------------------------------------------#
# class Get_Webpage_href
#                                                                              
# Purpose: Return array that stores every href of each page from href.txt
#------------------------------------------------------------------------------------------------------------#
class Get_Webpage_href
  def initialize(pathname,srcfile)
	@line=String.new
	@pathname=pathname
	@srcfile=srcfile
	@href_file=File.join(@pathname,@srcfile)
	@hFile=File.new(@href_file,'r')
	@str_http=String.new
	@str_href="href="
	@arr_return=Array.new
  end

  def filter_results
	return @line[(0-@line.length+@str_href.length) .. -3] 
  end
  
  def get_results
	 @hFile.each_line {|@line| 
			if !(@line.empty?) then
				case @line
					when /(.asp|.htm),$/
					@str_http=filter_results()
					#puts "#{@str_http}+'\N'"
					@arr_return.push(@str_http)
          
          when /[a-zA-Z0-9](html)%2F*/
          @str_http=filter_results()
					#p "#{@str_http}+'\n' "
					@arr_return.push(@str_http)
          
				end
			end
		}
	return @arr_return
  end	
  
end
#-------------------------------------------------------------------------------------------------------------#
# class Dump_File_Basename
#                                                                              
# Purpose: Dump File with both Basename filename
#
# Sep92005 Add logger to file 
#------------------------------------------------------------------------------------------------------------#
class Dump_File_Basename
  def initialize(pathname,outfile,strContent,logfiledir=nil)
    @pathname=pathname
    @outfile=outfile
    @strContent=strContent
    @outfile_path=File.join(@pathname,@outfile)
    @dir_entries=Array.new
    @logfiledir=logfiledir
  end
  
  def chk_outfile_existence()
	  @dir_entries=Dir.entries(@pathname)
	  return @dir_entries.include?(@outfile) 
  end

  def Dump_outfile()
	if   !chk_outfile_existence() then
		begin
			@hFile=File.new(@outfile_path,'w')
			@hFile.write(@strContent)
		rescue SystemCallError
			@hFile.close
			File.delete(@outfile_path)
		end
		@hFile.close
    Show_Outcomes.new(" Dump page #{@outfile}.",@logfiledir).log_screen
    Show_Outcomes.new(" Dump page #{@outfile}.",@logfiledir).log_logfile
	end
  end
end
#-------------------------------------------------------------------------------------------------------------#
# class DoTest_Dir_File
#                                                                              
# Purpose: Dump File with both Basename filename
#------------------------------------------------------------------------------------------------------------#
class DoTest_Dir_File
  def initialize(arrSrc,arrDest)
    @arrSrc=arrSrc
    @arrDest=arrDest
    @arrRet=Array.new
  end
  
  def DoDifference
    return @arrRet=@arrDest-@arrSrc
  end
  
  def DoUnion
    return @arrRet=(@arrDest|@arrSrc)
  end

  def DoIntersection
    return @arrRet=(@arrDest&@arrSrc)  
  end
  
end
#-------------------------------------------------------------------------------------------------------------#
# class Get_Webpage_help
#                                                                              
# Purpose: Return array that stores text in each help page
#------------------------------------------------------------------------------------------------------------#
class Get_Webpage_help
  def initialize()
	@str_return=String.new
  end
  
  def get_results
	 @str_return=$ie.document().body.innerText.strip
	return @str_return
  end	

end

#-------------------------------------------------------------------------------------------------------------#
# class Check_DUT_httpd_alive
#                                                                              
# Purpose: Check DUT http server if alive
#------------------------------------------------------------------------------------------------------------#
class Check_DUT_httpd_alive
require 'ping'
require 'net/http'

  def initialize(host,logfiledir=nil)
    @host=host
    @logfiledir=logfiledir
  end
  
  def check_alive
    retvalue=Ping.pingecho(@host)
   
    if retvalue == true #then #if true
      #h = Net::HTTP.new(@host, 80)
      #resp, data = h.get('/', nil )
      begin
        h = Net::HTTP.new(@host, 80)
        resp, data = h.get('/', nil )
  
      rescue Errno::ECONNREFUSED
        Show_Outcomes.new("HTTP Srv isn't alive.").log_screen
        Show_Outcomes.new("HTTP Srv isn't alive",@logfiledir).log_logfile  
        return false
      end  
      # puts "resp = #{resp}; #{resp.class}"
      Show_Outcomes.new("Code = #{resp.code}.").log_screen
      Show_Outcomes.new("Message = #{resp.message}",@logfiledir).log_logfile
      #resp.each {|key, val| printf "%-14s = %-40.40s\n", key, val }
      # puts "data = #{data}; #{data.class}"    #puts data[0..55]
    else
      Show_Outcomes.new("Host can't reachable.").log_screen
      Show_Outcomes.new("Host can't reachable.",@logfiledir).log_logfile
      
      return false
    end
    
    return  resp.code
    
  end	

end

#-------------------------------------------------------------------------------------------------------------#
# class KeyBoard_Event
#                                                                              
# Purpose: Trigger Keyboard event effect
#------------------------------------------------------------------------------------------------------------#
class KeyBoard_Event
require 'Win32API'

  def initialize()
    @KEYEVENTF_KEYUP = 0x2
    @VK_PRIOR        = 0x21 #Page UP key
    @VK_NEXT        = 0x22  #Page DOWN key
    @SW_HIDE         = 0
    @SW_SHOW         = 5
    @SW_SHOWNORMAL   = 1
    @VK_CONTROL      = 0x11
    @VK_F4           = 0x73
    @VK_MENU         = 0x12
    @VK_RETURN       = 0x0D
    @VK_SHIFT        = 0x10
    @VK_SNAPSHOT     = 0x2C
    @VK_TAB         = 0x09  
    
    @keybd_event = Win32API.new("user32", "keybd_event", ['I','I','L','L'], 'V')
    @vkKeyScan = Win32API.new("user32", "VkKeyScan", ['I'], 'I')
  end
  
  def Page_Down()
  # Page DOWN key
    @keybd_event.Call(@VK_NEXT, 1, 0, 0)
    @keybd_event.Call(@VK_NEXT, 1, @KEYEVENTF_KEYUP, 0)
    sleep(1)
  end

  def Paste()
  # Ctrl + V  : Paste
    @keybd_event.Call(@VK_CONTROL, 1, 0, 0)
    @keybd_event.Call(@vkKeyScan.Call(?V), 1, 0, 0)
    @keybd_event.Call(@vkKeyScan.Call(?V), 1, @KEYEVENTF_KEYUP, 0)
    @keybd_event.Call(@VK_CONTROL, 1, @KEYEVENTF_KEYUP, 0)
  end

  def Save_As()
  # Alt F + A : Save As
    @keybd_event.Call(@VK_MENU, 1, 0, 0)
    @keybd_event.Call(@vkKeyScan.Call(?F), 1, 0, 0)
    @keybd_event.Call(@vkKeyScan.Call(?F), 1, @KEYEVENTF_KEYUP, 0)
    @keybd_event.Call(@VK_MENU, 1, @KEYEVENTF_KEYUP, 0)
    @keybd_event.Call(@vkKeyScan.Call(?A), 1, 0, 0)
    @keybd_event.Call(@vkKeyScan.Call(?A), 1, @KEYEVENTF_KEYUP, 0)
    sleep(0.5)
  end

  def Tab_Key()
    @keybd_event.Call(@VK_TAB, 1, 0, 0)
    @keybd_event.Call(@VK_TAB, 1, @KEYEVENTF_KEYUP, 0)
    sleep(0.5)
  end
  
  def Enter_Key()
    @keybd_event.Call(@VK_RETURN, 1, 0, 0)
    @keybd_event.Call(@VK_RETURN, 1, @KEYEVENTF_KEYUP, 0)
    sleep(0.5)
  end
  
  def Exit()
    @keybd_event.Call(@VK_MENU, 1, 0, 0)
    @keybd_event.Call(@VK_F4, 1, 0, 0)
    @keybd_event.Call(@VK_F4, 1, @KEYEVENTF_KEYUP, 0)
    @keybd_event.Call(@VK_MENU, 1,@KEYEVENTF_KEYUP, 0)
    sleep(1) 
  end

  def teardown()
    @keybd_event = nil
    @vkKeyScan = nil
  end
  
end

class Screen_Capture
require 'Win32API'

  KEYEVENTF_KEYUP = 0x2
  SW_HIDE         = 0
  SW_SHOW         = 5
  SW_SHOWNORMAL   = 1
  VK_CONTROL      = 0x11
  VK_F4           = 0x73
  VK_MENU         = 0x12
  VK_RETURN       = 0x0D
  VK_SHIFT        = 0x10
  VK_SNAPSHOT     = 0x2C
  VK_TAB      = 0x09
  GMEM_MOVEABLE = 0x0002
  CF_TEXT = 1

  def initialize(dirname,filename, active_window_only=false, save_as_bmp=false)
    @dirname=dirname
    @filename=filename
    @active_window_only=active_window_only
    @save_as_bmp=save_as_bmp
  end

  # this method saves the current window or whole screen as either a bitmap or a jpeg
  # It uses paint to save the file, so will barf if a duplicate filename is selected, or  the path doesnt exist etc
  #    * dirname         - string  -  the name of the directory to save.
  #    * filename        - string  -  the name of the file to save. If its not fully qualified the current directory is used
  #    * active_window   - boolean - if true, the whole screen is captured, if false,  just the active window is captured
  #    * save_as_bmp     - boolean - if true saves the file as a bitmap, saves it as a jpeg otherwise
  def capture()

    keybd_event = Win32API.new("user32", "keybd_event", ['I','I','L','L'], 'V')
    vkKeyScan = Win32API.new("user32", "VkKeyScan", ['I'], 'I')
    winExec = Win32API.new("kernel32", "WinExec", ['P','L'], 'L')
    openClipboard = Win32API.new('user32', 'OpenClipboard', ['L'], 'I')
    setClipboardData = Win32API.new('user32', 'SetClipboardData', ['I', 'I'], 'I')
    closeClipboard = Win32API.new('user32', 'CloseClipboard', [], 'I')
    globalAlloc = Win32API.new('kernel32', 'GlobalAlloc', ['I', 'I'], 'I')
    globalLock = Win32API.new('kernel32', 'GlobalLock', ['I'], 'I')
    globalUnlock = Win32API.new('kernel32', 'GlobalUnlock', ['I'], 'I')
    memcpy = Win32API.new('msvcrt', 'memcpy', ['I', 'P', 'I'], 'I')

    @dirname=@dirname.tr('/','\\')
    @filename=@filename.tr('/','\\')
    @filename = Dir.getwd.tr('/','\\')+'\\'+@dirname+'\\'+@filename

    if @active_window_only ==false
          keybd_event.Call(VK_SNAPSHOT,0,0,0)   # Print Screen
    else
          keybd_event.Call(VK_SNAPSHOT,1,0,0)   # Alt+Print Screen
    end 

    winExec.Call('mspaint.exe', SW_SHOW)
    sleep(1)
     
    # Ctrl + V  : Paste
    KeyBoard_Event.new.Paste()

    # Alt F + A : Save As
    KeyBoard_Event.new.Save_As()

    # copy filename to clipboard
    hmem = globalAlloc.Call(GMEM_MOVEABLE, @filename.length+1)
    mem = globalLock.Call(hmem)
    memcpy.Call(mem, @filename, @filename.length+1)
    globalUnlock.Call(hmem)
    openClipboard.Call(0)
    setClipboardData.Call(CF_TEXT, hmem) 
    closeClipboard.Call 
    sleep(1)
      
    # Ctrl + V  : Paste
    KeyBoard_Event.new.Paste()

    if @save_as_bmp == false
      # goto the combo box
      KeyBoard_Event.new.Tab_Key()

      # select the first entry with J
      keybd_event.Call(vkKeyScan.Call(?J), 1, 0, 0)
      keybd_event.Call(vkKeyScan.Call(?J), 1, KEYEVENTF_KEYUP, 0)
      sleep(0.5)
    end  

    # Enter key
    KeyBoard_Event.new.Enter_Key()
     
    # Alt + F4 : Exit
    KeyBoard_Event.new.Exit()

  end
    
end

#-------------------------------------------------------------------------------------------------------------#
# class CPE_TrendChip
#                                                                              
# Purpose: TrendChip moden usage thru telnet
#------------------------------------------------------------------------------------------------------------#
class CPE_TrendChip
require 'net/telnet'

  def initialize(retval,host,logfilepath)
    @RetVal=retval
    @password_prompt = /Password[: ]*\z/n

    @dut = Net::Telnet::new("Host" => host,
                        "Timeout" => 10,
                        "Prompt" => /[$%#>] \z/n,
                        "Output_log" => logfilepath)

  end
  
  def DUT_Login(password)
    case @RetVal

      when "200"

      when /40(1|2|3)/
        Show_Outcomes.new("Return Value =#{@RetVal}.").log_screen
        line=String.new
        line = @dut.waitfor(@password_prompt)
        line += @dut.cmd(password)
        Show_Outcomes.new("#{@line}").log_screen
      else
        p 'Unknow Login methode.'
    end
    
  end

  def DUT_Logout()
    line = @dut.close
    Show_Outcomes.new("#{@line}").log_screen
  end
  
  def DUT_CMD(cmd)
    line = @dut.cmd(cmd)
    Show_Outcomes.new("#{@line}").log_screen
  end
  
end
#-------------------------------------------------------------------------------------------------------------#
# class Login_DUT_Billion
#                                                                              
# Purpose: Using telnet to login DUT
#
# Jun5/2006  Initialize
#-------------------------------------------------------------------------------------------------------------#
class DUT_Billion
require 'net/telnet'

  def initialize(debug=nil)
    #@login_prompt = /enter for login\z/n
    @login_prompt = /home.gateway login[: ]*/
    @password_prompt = /Password[: ]*/
    @prompt = /home.gateway[>]*/
    @debugmode=debug
  end
  
  def Login(session,username,password)
    #p "#{line.object}"
    line = session.waitfor(@login_prompt)
    line = session.cmd(username)
    line = session.cmd(password)
    line = session.waitfor(@prompt)
    return true
  end
  
  def Chk_statusCnt(strCnt)
    strcnd1="attenuation downstream:"
    strcnd2="adsl doesn't sync up!"
    ret1=false;ret2=false
    
    ret1=strCnt.downcase.include? strcnd1
    ret2=strCnt.downcase.include? strcnd2
    
    if ret1==true
      retmsg="up"
    elsif ret1==true
      retmsg="down"
    else
      retmsg="unknown"
    end
    
    return retmsg
  end
  
  def Chk_ADSL_status(host,username,password,logfnamedir)
    if @debugmode.to_s == "on"
      dut_session = Net::Telnet::new("Host" => host,
                                  "Timeout" => 10,
                                  "Prompt" => / [:|>|$|%|#]*/,
                                  "Output_log" => "log.txt")
    else
      dut_session = Net::Telnet::new("Host" => host,
                                  "Timeout" => 10,
                                  "Prompt" => / [:|>|$|%|#]*/)
    end
    
    Login(dut_session,username,password)
    
    cmd="wan adsl linedata"
    line = dut_session.cmd(cmd)
    #line = dut_session.waitfor(/\!$/)
    line = dut_session.waitfor(/[:|>|$|%|#|!]/)
    
    Show_Outcomes.new(line).log_screen
    Show_Outcomes.new(line,logfnamedir).log_logfile
    strmsg=Chk_statusCnt(line)
    
    Logout(dut_session)
    return strmsg
  end
  

  
  def Generate_CTLX(hash_inifile,logfnamedir)
    host=""
    username=""
    password=""

    #-----------------------------------------------
    #Get username ,password and array dut IP Address
    #-----------------------------------------------
    arr_dut_ip=[]
    hash_inifile.each { |key, value|
      #print "  #{key}=#{value} #{key.class}\n"
      case key.to_s
        when /Billion_username/
              username=value
        when /Billion_password/
              password=value
        when /Billion_0103[0-9]/
              arr_dut_ip.push(value)
      end
    }

    arr_dut_ip.each {|host|
      ArpTable_Clear()
      Show_Outcomes.new(host).log_screen
      
      while Check_DUT_httpd_alive.new(host,logfnamedir).check_alive != false
        
        #cntflag=0
        #while cntflag < 10
          str_retval=Chk_ADSL_status(host,username,password,logfnamedir)
          
          Show_Outcomes.new(str_retval).log_screen
          Show_Outcomes.new(str_retval,logfnamedir).log_logfile
          
          #if str_retval.to_s == "up"
          #  cntflag=10
          #else
          #  cntflag++
          #end
          
        #end
        
    
        Reboot(host,username,password)
      end

      ArpTable_Clear()
    }
    
  end

  def Reboot(host,username,password)
    if @debugmode.to_s == "on"
      dut_session = Net::Telnet::new("Host" => host,
                                  "Timeout" => 10,
                                  "Prompt" => / [:|>|$|%|#]*/,
                                  "Output_log" => "log.txt")
    else
      dut_session = Net::Telnet::new("Host" => host,
                                  "Timeout" => 10,
                                  "Prompt" => / [:|>|$|%|#]*/)
    end
    
    Login(dut_session,username,password)
    
    cmd="sys reboot"
    line = dut_session.cmd(cmd)
    #line = dut_session.waitfor(/\!$/)
    line = dut_session.waitfor(/[:|>|$|%|#|!]/)
    
    Show_Outcomes.new(line).log_screen
    
    Logout(dut_session)
    return true
  end
  
  def Logout(session)
    session.close
  end
  
  def ArpTable_Clear
     cmd="arp -d"
     retval=system(cmd)
     
     return retval
  end
end

#-------------------------------------------------------------------------------------------------------------#
# class TestEnv_Configure
#                                                                             
# Purpose: Configure task.properties under folder tr69_device_test_suite/bin
#          Add method Read_TestEnv_ini for input argument from inifile.
#------------------------------------------------------------------------------------------------------------#
class TestEnv_Config

  def Read_TestEnv_ini(what)
    require 'API_inifile'
  
    if FileTest.exist?(what) == false
      Show_Outcomes.new("Pls check #{what} if exists!").log_screen
      return 1
    end
  
    rtu_hash = {}
    dut_number=""
    oIni = IniFile.new(what)
  
    oIni.sections { |s|
      #print "[#{s}]\n"
      oIni[s].each { |key, value|
        #print "  #{key} = #{value}\n"
        case key
        when /^Billion_01031$/
          rtu_hash[:Billion_01031]=value
        when /^Billion_01032$/
          rtu_hash[:Billion_01032]=value
        when /^Billion_01033$/
          rtu_hash[:Billion_01033]=value
        when /^Billion_01034$/
          rtu_hash[:Billion_01034]=value
        when /^Billion_01035$/
          rtu_hash[:Billion_01035]=value
        when /^Billion_01036$/
          rtu_hash[:Billion_01036]=value
        when /^IP_01037$/
          rtu_hash[:Billion_01037]=value
        when /^IP_01023$/
          rtu_hash[:Billion_01023]=value
        when /^Billion_username$/
          rtu_hash[:Billion_username]=value
        when /^Billion_password$/
          rtu_hash[:Billion_password]=value
          
        when /^DUT_Len$/
          dut_number=value
        end
      }
    }
    
    element=dut_number.to_i+2;#add username an passwd
    #p "#{element}" 
    if (rtu_hash.length != element)
      Show_Outcomes.new("Pls check #{what} content! It should have #{element} variables.").log_screen
      return 1
    else
      return rtu_hash
    end
    
  end
  
end