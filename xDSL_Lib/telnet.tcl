####################################################################
#                         ScriptCenter
#                 Telnet Automation Library
#
# Copyright (c) 1993-2000, Spirent Communications of Calabasas, Inc.
# Copyright (c) 1993-1999, Netcom Systems, Inc.
####################################################################
if {[info exists __TELNET_TCL__] == 0} then {
set __TELNET_TCL__ 1
package provide libcmds 1.0

namespace eval SCTELNET {
   variable MaxTimeout 30
   variable Debug 0
   variable LoginSet
   variable IsOpen 0
   variable Session
   variable Prompt
   variable LogFile
   variable LogFileName default.log
   variable ReturnBuffer ""
   variable ResponseDelay 500
}


#################################################
#  Wait for response
#################################################
proc _EX_Telnet:WaitFor {str {verbose 0}} {
   if {$SCTELNET::Debug > 0} {set verbose 1}
   set timer 0
   set ch "this is an empty string this is an empty string this is a you know"
   while {[string compare $ch $str] != 0} {
      set instr [read $SCTELNET::Session]
      set ch ""
      if {$verbose > 0} {
         set strList [split $instr \n]
         for {set i 0} {$i < [llength $strList]} {incr i} {
            _EX_TelnetLog [lindex $strList $i]
            append SCTELNET::ReturnBuffer "[lindex $strList $i]\n"
         }
      }      
      if {[string length $instr] > 0} {         
         set instr [string trimright $instr]         
         set ch [string range $instr [expr [string length $instr] - [string length $str]] [string length $instr]]
         #Prompt for xterm
         if [regexp "Terminal" $instr] {
            #set timer 1
            _EX_TelnetLog "Setting to xterm"
            puts $SCTELNET::Session "xterm"            
         }
      }
      incr timer
      if {$timer > $SCTELNET::MaxTimeout} {
         _EX_TelnetLog "Timeout exceeded waiting for $str"
         return 0
      }
      after 1000
   }
   #_EX_TelnetLog $ch
   return 1
}


###############################################
# Negotiate with the VTA
###############################################
proc _EX_Telnet:NegotiateTerm {negs} {
   for {set k 0} {$k < $negs} {incr k} {      
      set timer 0
      set i ""
      while {$i == ""} {
         #Read until you can't read no more
         set i [read $SCTELNET::Session]         
         if {$SCTELNET::Debug > 0} {_EX_TelnetLog "***i is ${i}***"}
         
         #Did we go too far
         if {[regexp "LOGIN" [string toupper $i]] || \
             [regexp "NAME"  [string toupper $i]]} {
             set SCTELNET::LoginSet 1
             return
         }
         
         after 1000
         incr timer
         puts -nonewline "."
         flush stdout
         if {$timer > $SCTELNET::MaxTimeout} {
            _EX_TelnetLog "\nT.O.on_NegRead $k"
            return
         }
      }
      puts ""
      
      #Parse it
      set str ""
      for {set j 0} {$j < [string length $i]} {incr j 3} {        
         set opt [string range $i $j [expr $j + 3]]
         #Prepare response: 255 <response> <option>        
         set respond 252

         #Is this a VTA negotiation?
         set firstByteStr [string range $opt 0 0]
         set chkByte [string range $opt 1 1]
         set doerByte [string range $opt 2 2]
         if {$firstByteStr == [binary format c 255]} {
            #Set correct response
            if {$chkByte == [binary format c 251]} {set respond 254}            

            if {$SCTELNET::Debug} {
               set firstByte 0
               set secondByte 0
               set thirdByte 0
               binary scan $firstByteStr c firstByte
               binary scan $chkByte c secondByte
               binary scan $doerByte c thirdByte         
               puts "VTA Rcv: [expr ($firstByte + 0x100 ) % 0x100] [expr ($secondByte + 0x100 ) % 0x100] [expr ($thirdByte + 0x100 ) % 0x100]"
            }
            #Prepare response
            append str "[binary format cc 255 $respond]$doerByte"      
         } else {            
            if {$SCTELNET::Debug} {
               set firstByte 0
               set secondByte 0
               set thirdByte 0
               binary scan $firstByteStr c firstByte
               binary scan $chkByte c secondByte
               binary scan $doerByte c thirdByte         
               puts "Returning VTA Rcv: [expr ($firstByte + 0x100 ) % 0x100] [expr ($secondByte + 0x100 ) % 0x100] [expr ($thirdByte + 0x100 ) % 0x100]"
            }
            #Clear Buffer
            puts $SCTELNET::Session ""
            return
         }         
      }
      #Send response
      puts -nonewline $SCTELNET::Session $str
      after $SCTELNET::ResponseDelay
   }
}


###################################################
# Telnet Open
###################################################
proc EX_TelnetOpen {server user pass args} {

   #Set defaults
   set prompt "\~\]\$"
   set commands {}
   set telnetPort "telnet"
   set SCTELNET::MaxTimeout 10 
   set passPrompt "Password:"
   set SCTELNET::LoginSet 0
   set SCTELNET::LogFileName "sctelnet.log"
   set SCTELNET::Prompt "\>"

  
   #Parse args
   for {set i 0} {$i < [llength $args]} {incr i} {
      #Prompt
      set f [lsearch -exact $args "-prompt"]
      if {$f > -1} {set SCTELNET::Prompt [lindex $args [expr $f + 1]] }

      #Timeout
      set f [lsearch -exact $args "-timeout"]
      if {$f > -1} {set SCTELNET::MaxTimeout [lindex $args [expr $f + 1]] }     

      #PassString
      set f [lsearch -exact $args "-passwordPrompt"]
      if {$f > -1} {set passPrompt [lindex $args [expr $f + 1]] } 

      #LogFile
      set f [lsearch -exact $args "-logFile"]
      if {$f > -1} {set SCTELNET::LogFileName [lindex $args [expr $f + 1]] } 

      #debug
      set f [lsearch -exact $args "-debug"]
      if {$f > -1} {set SCTELNET::Debug [lindex $args [expr $f + 1]] }

      #responseDelay in milliseconds
      set f [lsearch -exact $args "-responseDelay"]
      if {$f > -1} {set SCTELNET::ResponseDelay [lindex $args [expr $f + 1]] }

      #telnetPort
      set f [lsearch -exact $args "-port"]
      if {$f > -1} {set telnetPort [lindex $args [expr $f + 1]] }

   }

   #Open log
   puts "Logging to: $SCTELNET::LogFileName"
   if [catch {set SCTELNET::LogFile [open $SCTELNET::LogFileName a]} iErr] {
      puts "Could not open log file $SCTELNET::LogFileName\n$iErr\n"
   }
   

   #Connect
   _EX_TelnetWriteLogHeader $server
   _EX_TelnetLog "Making connection to $server"
   if {$SCTELNET::IsOpen} {
      EX_TelnetClose      
   }
   if [catch {set SCTELNET::Session [socket $server $telnetPort]} iErr] {
      _EX_TelnetLog "Could not open socket to port $telnetPort"
      _EX_TelnetLog "Error: $iErr"
      return
   }   
   fconfigure $SCTELNET::Session -blocking 0
   fconfigure $SCTELNET::Session -buffering none
   fconfigure $SCTELNET::Session -translation binary
   
   #Negotiate terminal
   _EX_TelnetLog "Negotiating telnet terminal"
   _EX_Telnet:NegotiateTerm 5

   fconfigure $SCTELNET::Session -translation auto

   #Wait for Login
   #puts $SCTELNET::Session "" ;#Clear buffer
   if {$SCTELNET::LoginSet < 1} {
      _EX_TelnetLog "Waiting for login"
      _EX_Telnet:WaitFor "login:"
      #Oh well, lets try and login anyways.
   }
   _EX_TelnetLog "Logging in as $user"
   puts $SCTELNET::Session $user
   
   #Wait for password
   _EX_TelnetLog "Waiting for password"   
   if {![_EX_Telnet:WaitFor $passPrompt]} {return}
   _EX_TelnetLog "Setting password to $pass"   
   puts $SCTELNET::Session $pass

   #Wait for prompt
   _EX_TelnetLog "Waiting for prompt"  
   if {![_EX_Telnet:WaitFor $SCTELNET::Prompt 1]} {return}

   #If we made it this far we're open for business
   set SCTELNET::IsOpen 1

   #Send a command to suppress echo
   _EX_TelnetLog "Attempting to turn off echo"
   puts -nonewline $SCTELNET::Session [binary format ccc 255 254 1]
   _EX_TelnetLog "Attempting to flush buffer"
   puts $SCTELNET::Session ""   
   _EX_Telnet:WaitFor $SCTELNET::Prompt 1
   

}


###############################################
# Send Command
###############################################
proc EX_TelnetSendCommand {command args} {
   if {$SCTELNET::IsOpen < 1} {
      _EX_TelnetLog "Session not open"
      return
   }

   #Clear Buffer
   set SCTELNET::ReturnBuffer ""

   #Get last prompt for aesthetics
   set prompt $SCTELNET::Prompt

   #Parse args
   for {set i 0} {$i < [llength $args]} {incr i} {
      #Prompt
      set f [lsearch -exact $args "-prompt"]
      if {$f > -1} {set SCTELNET::Prompt [lindex $args [expr $f + 1]] }
   }
   
   #ESC
   if {$command == "ESC"} {
      _EX_TelnetLog "ESC"
      set command [binary format c 27]
      _EX_TelnetLog "$prompt ESC" ;#Just to make it look real
      puts -nonewline $SCTELNET::Session $command
      _EX_Telnet:WaitFor $SCTELNET::Prompt 1
   } else {
      #Process command
      _EX_TelnetLog "$prompt $command" ;#Just to make it look real
      puts $SCTELNET::Session $command      
      _EX_Telnet:WaitFor $SCTELNET::Prompt 1
   }

   return $SCTELNET::ReturnBuffer
}


###############################################
# Send File
###############################################
proc EX_TelnetSendFile {file} {

   if [file exists $file] {
      set f [open $file r]
      set buf [read $f]
      set lines [split $buf "\n"]
      for {set i 0} {$i < [llength $lines]} {incr i} {
         EX_TelnetSendCommand [lindex $lines $i]
      }

      close $f
   } else {
      _EX_TelnetLog "ERROR: Could not find Telnet file: $file"
   }

}

###############################################
# Send ttl File
###############################################
proc EX_TelnetSendTTLFile {file} {

   if [file exists $file] {
      set f [open $file r]
      set buf [read $f]
      close $f
      set lines [split $buf "\n"]
      set incrval 1
      
      for {set i 0} {$i < [llength $lines]} {incr i $incrval} {
         set incrval 1
         set line [lindex $lines $i]
         if {[string index [string trim $line] 0] != "\;"} {
            if [regexp "sendln" $line] {
               #Get command to send
               set cmdlist [split $line "\'"]
               set cmd [lindex $cmdlist 1]

               #Get next line as prompt
               set waitline [lindex $lines [expr $i +1]]
               set waitlist [split $waitline "\'"]
               set waitprompt [lindex $waitlist 1]

               #Send command
               EX_TelnetSendCommand $cmd -prompt $waitprompt
               
               #increment appropriately
               set incrval 2
            }
         }
      }
     
   } else {
      _EX_TelnetLog "ERROR: Could not find Telnet file: $file"
   }

}


###############################################
# Close Session
###############################################
proc EX_TelnetClose {} {
   catch {puts $SCTELNET::Session "exit"} iErr
   catch {close $SCTELNET::Session} iErr
   set SCTELNET::IsOpen 0
   catch {close $SCTELNET::LogFile}
}



###############################################
# LOG Header
###############################################
proc _EX_TelnetWriteLogHeader {server} {
   set now [clock seconds]
   set strDate [clock format $now -format {%m-%d-%y %H:%M %p}]

   catch {     
      puts $SCTELNET::LogFile "\n#####################################################################"
      puts $SCTELNET::LogFile "# Telnet session to $server"
      puts $SCTELNET::LogFile "# Started: $strDate"
      puts $SCTELNET::LogFile "#####################################################################"
   }
}


###############################################
# LOG
###############################################
proc _EX_TelnetLog {str} {   
   puts $str
   catch {
      puts $SCTELNET::LogFile $str
      flush $SCTELNET::LogFile
   }
}




} ;#If defined
