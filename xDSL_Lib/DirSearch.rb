#$LOAD_PATH.unshift File.join(File.dirname(__FILE__), '.') if $0 == __FILE__
#-------------------------------------------------------------------------------------------------------------#
# class Show_Outcomes
#                                                                              
# Purpose: Output execute results to screen    
#------------------------------------------------------------------------------------------------------------#
class Dump_File
  def initialize(str_outfile,strContent=nil)
    @str_outfile=str_outfile
    @strContent=strContent
    #@strtemp=String.new
    @hFile=File.new(@str_outfile,'w')
  end
  
  def dump_results()
    @hFile.write(@strContent)
    @hFile.close
  end
  
  def dump_append(arr_content)
    hFile=File.new(@str_outfile,'w+')
    
    arr_content.each {|x|
      hFile.puts(x.to_s)
      hFile.flush
    }
    hFile.close
  end

  def NoiseFile_csv(str_infname)
    arr_filecnt = IO.readlines(str_infname) 
    arr_pathfile= arr_filecnt.collect {|x| x if x =~ /^F\.\//}

    arr_pathbasename=[]
    str_pathbasename=""

    arr_pathfile.each{|x|
      if !x.to_s.empty?
        #puts "#{x}"
        z=File.split(x.gsub(/F\./, "") )
        z.each{|y|
          t=","+y;#for csv file purpose
          str_pathbasename << t.to_s
          #arr_pathbasename.push(y.to_s.gsub("\n", "") << ",")
        }
      end
    }

    Dump_File.new(@str_outfile,str_pathbasename).dump_results
  end
  
end
#-------------------------------------------------------------------------------------------------------------#
# class Dump_Dir_File
#                                                                              
# Purpose: Dump each file under specific directory
#------------------------------------------------------------------------------------------------------------#
class Dump_Dir_File
  require 'fileutils'
  require 'find'
  
  def Dir_filename(str_targetdir)
    arr_dircnt=[]

    FileUtils::Verbose.cd(str_targetdir) do
      Find.find(".") do |f|
        Find.prune if f =~ /temp|system*|\$+/i
        type = case 
          when File.file?(f): "F" 
          when File.directory?(f): "D" 
          else "?" 
          end
        #puts "#{type}: #{f}"
        #strpathfname=str_targetdir+f.to_s+"\n";  #p "#{strpathfname}"
        strpathfname=type.to_s+f.to_s+"\n"
        arr_dircnt.push(strpathfname)
      end
    end
    
    return arr_dircnt
  end

  def Dirname(str_targetdir)
    arr_dircnt=[]

    FileUtils::Verbose.cd(str_targetdir) do
      Find.find(".") do |f|
        Find.prune if f =~ /temp|system*|\$+/i
        type = case 
          when File.directory?(f): "D"
          else next 
          end
        strpathfname=type.to_s+f.to_s+"\n"
        arr_dircnt.push(strpathfname)
      end
    end
    
    return arr_dircnt
  end

  def Filename(str_targetdir)
    arr_dircnt=[]

    FileUtils::Verbose.cd(str_targetdir) do
      Find.find(".") do |f|
        Find.prune if f =~ /temp|system*|\$+/i
        type = case 
          when File.file?(f): "F"
          else next 
          end
        strpathfname=type.to_s+f.to_s+"\n"
        arr_dircnt.push(strpathfname)
      end
    end
    
    return arr_dircnt  
  end
  
  def doDumpDir_File(str_dir,logfname=nil)
    arr_dircnt=[]
    arr_dircnt=Dir_filename(str_dir)
    #arr_dircnt.sort.each {|x| p "#{x}"}
    arr_dircnt.push("currentpath="+str_dir) 
    str_dircnt=arr_dircnt.sort.to_s
    
    Dump_File.new(logfname,str_dircnt).dump_results
    return str_dircnt
  end
  
  def doDumpDir(str_dir,logfname=nil)
    arr_dircnt=[]
    arr_dircnt=Dirname(str_dir)
    #arr_dircnt.sort.each {|x| p "#{x}"}
    arr_dircnt.push("currentpath="+str_dir) 
    str_dircnt=arr_dircnt.sort.to_s
    
    Dump_File.new(logfname,str_dircnt).dump_results
    return arr_dircnt
  end
  
  def doDumpFile(str_dir,logfname=nil)
    arr_filecnt=[]
    arr_filecnt=Filename(str_dir)
    #arr_dircnt.sort.each {|x| p "#{x}"}
    arr_filecnt.push("currentpath="+str_dir) 
    str_filecnt=arr_filecnt.sort.to_s
    
    Dump_File.new(logfname,str_filecnt).dump_results
    return arr_filecnt  
  end
  
end
#----------------------------------------------------------------
#Start Here
#----------------------------------------------------------------
str_targetdir=File.join("C:","Program Files","Spirent Communications",\
                      "DLS 5204","NoiseFiles")

#str_targetdir=File.join("C:","Program Files", "Common Files")
strfname="Noise5500.txt"
strfname_Dir="Noise5500_Dir.txt"
strfname_File="Noise5500_File.txt"
strfname_Noise="noisefname_5500.csv"

Dump_Dir_File.new.doDumpDir_File(str_targetdir, strfname)
Dump_Dir_File.new.doDumpDir(str_targetdir, strfname_Dir)
Dump_Dir_File.new.doDumpFile(str_targetdir, strfname_File)

Dump_File.new("noisefname.csv").NoiseFile_csv(strfname)

#Dump_File.new("noisefname.txt").dump_append(arr_pathbasename)
#Dump_File.new("noisefname-uniq.txt").dump_append(arr_pathbasename.uniq)